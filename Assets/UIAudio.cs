﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIAudio : MonoBehaviour
{    
    public Slider MasterVolSlider;    
    public Slider MusicVolSlider;   
    public Slider SFXVolSlider;

    private AudioManager _audioManager;

    void Awake()
    {
        _audioManager = AudioManager.Instance;
        MasterVolSlider.onValueChanged.AddListener((value) => { _audioManager.OnMasterVolumeChanged(value); });
        MusicVolSlider.onValueChanged.AddListener((value) => { _audioManager.OnMusicVolumeChanged(value); });        
        SFXVolSlider.onValueChanged.AddListener((value) => { _audioManager.OnSFXVolumeChanged(value); });
    }

    //public void MasterVolumeChanged()
    //{
    //    _audioManager.MasterVolume = MasterVolSlider.value;        
    //}

    //public void MusicVolumeChanged()
    //{
    //    _audioManager.MusicVolume = MusicVolSlider.value;
    //}

    //public void SFXVolumeChanged()
    //{
    //    _audioManager.SFXVolume = SFXVolSlider.value;
    //}
}
