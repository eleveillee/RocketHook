using UnityEngine;

using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

public class DataShop : ScriptableObject
{
    [SerializeField]
    AnimationCurve _defaultPrices;
    public AnimationCurve DefaultPrices
    {
        get { return _defaultPrices; }
    }

    [SerializeField]
    AnimationCurve _defaultValues;
    public AnimationCurve DefaultValues
    {
        get { return _defaultValues; }
    }

    Dictionary<DataGadget, List<DataGadgetUpgrade>> _dataGadgetUpgrades = new Dictionary<DataGadget, List<DataGadgetUpgrade>>();

    [SerializeField] //Should remove the ducplicate from GameManager
    private AssetGameDefinition _gameDefinition;

    void OnEnable()
    {
        //if(_dataGadgetUpgrades == null)
            BuildGadgetDatabase();
    }

    public void BuildGadgetDatabase()
    {
        _dataGadgetUpgrades.Clear();
       // Debug.Log("[DataShop] BuildingGadgetDatabase");
        foreach (DataGadget dataGadget in _gameDefinition.Gadgets)
        {
            //Debug.Log("[DataShop] DataGadget : " + dataGadget.name);
            Type obj = dataGadget.GetType();
            FieldInfo[] objFields = obj.GetFields();
            List<DataGadgetUpgrade> dataUpgrades = new List<DataGadgetUpgrade>();
            foreach (FieldInfo field in objFields) // Find all Upgrades
            {
                if (field.GetValue(dataGadget) == null || field.GetValue(dataGadget).GetType().BaseType != typeof(DataGadgetUpgrade))
                    continue;

                //Debug.Log("GetType :" + (field.GetValue(dataGadget)).GetType());
                DataGadgetUpgrade upgrade = (DataGadgetUpgrade)field.GetValue(dataGadget);
                if (upgrade == null)
                    continue;

                DataGadgetUpgrade dataUpgrade = (DataGadgetUpgrade)field.GetValue(dataGadget);
                dataUpgrades.Add(dataUpgrade);
            }
            _dataGadgetUpgrades.Add(dataGadget, dataUpgrades);
        }
    }

    public List<DataGadget> GetLockedGadgets()
    {
        return GameManager.Instance.GameDefinition.Gadgets.FindAll(x => !GameManager.Instance.PlayerProfile.Inventory.Gadgets.Any(y => y.Id == x.Id)); // Find all the DataGadgets in GameDefinition that aren't in player inventory
    }
    

    public Dictionary<DataGadget, List<DataGadgetUpgrade>> DataGadgetUpgrades
    {
        get { return _dataGadgetUpgrades; }
    }
}