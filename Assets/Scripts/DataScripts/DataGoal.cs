﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DataGoal : Data
{
    [SerializeField]
    private GoalType _type;

    [SerializeField]
    private GoalDuration _duration;

    [SerializeField]
    private GoalSource _source;

    [SerializeField]
    private Currency _rewardCurrency;

    [SerializeField]
    private List<string> _values = new List<string>();

    public DataGoal() : base()
    {

    }

    public DataGoal(GoalType type, GoalDuration duration, GoalSource source, Currency rewardCurrency, List<String> values)
    {
        _type = type;
        _duration = duration;
        _source = source;
        _values = values;
        _rewardCurrency = rewardCurrency;
    }

    public GoalType Type { get { return _type; }}
    public GoalDuration Duration { get { return _duration; } }
    public GoalSource Source { get { return _source; } }
    public Currency RewardCurrency { get { return _rewardCurrency; } }
    public List<string> Values { get { return _values; }}
}

public enum GoalSource
{
    Main,
    Random
}

public enum GoalType
{
    Height,
    Speed,
    Gold,
    Time,
    Score
}

public enum GoalDuration
{
    Game,
    Cumul,
    Lifetime
}