﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

using Random = UnityEngine.Random;

public class DataEncounter : DataScriptable
{
    public EncounterType EncounterType;
    public DataLevelParameters LevelParameters;
    public StoryInstance StoryInstance;
    public List<SpawnRules> SpawnRules;
}

[Serializable]
public enum EncounterType
{
    Story,
    Challenge,
    Bonus,
    Tutorial
}