using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Lean;


public class DataPlayer : ScriptableObject
{
    [Header("Stats")]
    [SerializeField]
    private float _baseHP; // HP and dmg need to come from updates like gadget upgrades
    public float BaseHP
    {
        get { return _baseHP; }
    }
    
    [SerializeField]
    private float _baseDamage;
    public float BaseDamage
    {
        get { return _baseDamage; }
    }

    [SerializeField]
    private float _criticalSpeed;//Speed where it shreds through monsters
    public float CriticalSpeed
    {
        get { return _criticalSpeed; }
    }

    [SerializeField]
    private Ressources _baseRessource;
    public Ressources BaseRessources
    {
        get { return _baseRessource; }
    }

    [Header("Visual")]
    [SerializeField]
    private Sprite _playerSprite;
    public Sprite PlayerSprite
    {
        get { return _playerSprite; }
    }

    [SerializeField]
    private RuntimeAnimatorController _animatorController;
    public RuntimeAnimatorController AnimatorController
    {
        get { return _animatorController; }
        set { _animatorController = value; }
    }

    public float GetRessourceValue(RessourceType type)
    {
        float value = 1;
        if (type == RessourceType.Fuel)
            value = 20f;
        else if (type == RessourceType.Hook)
            value = 1f;

        return _baseRessource.Get(type) + GameManager.Instance.PlayerProfile.Stats.GetRessourceLevel(type) * value;
    }
}
