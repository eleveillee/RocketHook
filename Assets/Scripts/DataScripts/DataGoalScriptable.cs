﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DataGoalScriptable : ScriptableObject
{
    [SerializeField]
    private DataGoal _data;
    public DataGoal Data { get { return _data; }}
}