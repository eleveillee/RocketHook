﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

using Random = UnityEngine.Random;

public class DataLevelParameters : ScriptableObject
{
    [Header("Chunks")]
    [SerializeField] private int _size = 500;
    [SerializeField] private Vector2 _nbrInitialChunk = new Vector2(1, 1);

    [Header("Platforms")]
    [SerializeField] private Vector2 _nbrPlatform = new Vector2(25, 50);
    [SerializeField] private Vector2 _platformSizeX = new Vector2(100f, 200f);
    [SerializeField] private Vector2 _platformSizeY = new Vector2(0.75f, 1.25f);
    [SerializeField] private bool _isPlatformRotating = false;

    [Header("Spawnables")]
    [SerializeField] private Vector2 _nbrPowerUp = new Vector2(20, 50);
    [SerializeField] private Vector2 _nbrBumper = new Vector2(50, 100);
    [SerializeField] private Vector2 _nbrGoo = new Vector2(10, 25);
    [SerializeField] private Vector2 _nbrMonster = new Vector2(20, 60);
    
    [Header("Config")]
    [SerializeField] private bool _isSafeZone = true;
    public bool HasGround = false;
    public float Gravity = -10;
    public float HeightPerGravityStep = 500;

    public int Size
    {
        get { return _size; }
    }

    public Vector2 NbrPowerUp
    {
        get { return _nbrPowerUp; }
    }

    public Vector2 NbrInitialChunk
    {
        get { return _nbrInitialChunk; }
    }

    public Vector2 NbrBumper
    {
        get { return _nbrBumper; }
    }

    public Vector2 NbrGoo
    {
        get { return _nbrGoo; }
    }

    public Vector2 NbrMonster
    {
        get { return _nbrMonster; }
    }

    public bool IsPlatformRotating
    {
        get { return _isPlatformRotating; }
    }

    public Vector2 NbrPlatform
    {
        get { return _nbrPlatform; }
    }

    public Vector2 PlatformSizeX
    {
        get { return _platformSizeX; }
    }

    public Vector2 PlatformSizeY
    {
        get { return _platformSizeY; }
    }

    public bool IsSafeZone
    {
        get { return _isSafeZone; }
    }
}