﻿using System;
using UnityEngine;

public abstract class DataScriptable : ScriptableObject
{
    private bool _isDebugVerbose = false;

    [SerializeField]
    private string _id;
    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    protected string _name;
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }
    public DataScriptable()
    {
        string prevId = _id;
        _id = Guid.NewGuid().ToString();
        _name = "ConfigNoName";
        if (string.IsNullOrEmpty(prevId) && _isDebugVerbose)
            Debug.Log("Generating Initial Data Id for " + name + " = " + _id);
        else if(_isDebugVerbose)
            Debug.Log("Regenerating Data Id for " + name + " = (" + prevId + " => " + _id);
    }
}
