﻿using UnityEngine;
using System;
using System.Collections;

public class DataGadgetMagnet : DataGadget
{

    public override GadgetType Type { get { return GadgetType.Accessory; } }

    [Header("Upgrades")]
    [SerializeField] public UpgradeMagnetPower UpgradeMagnetPower = new UpgradeMagnetPower();
    [SerializeField] public UpgradeMagnetRadius UpgradeMagnetRadius = new UpgradeMagnetRadius();
}


[Serializable] public class UpgradeMagnetPower : DataGadgetUpgrade {}
[Serializable] public class UpgradeMagnetRadius : DataGadgetUpgrade {}
