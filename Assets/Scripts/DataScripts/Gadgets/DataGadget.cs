﻿using UnityEngine;
using System;
using System.Collections.Generic;

public abstract class DataGadget : DataScriptable
{
    public abstract GadgetType Type { get; }

    [SerializeField]
    private GameObject _prefab;
    public GameObject Prefab { get { return _prefab; } }

    [SerializeField]
    private Sprite _sprite;
    public Sprite Sprite { get { return _sprite; } }

    [SerializeField]
    private List<GadgetInput> _inputs;
    public List<GadgetInput> Inputs { get { return _inputs; } }

    [SerializeField]
    private List<RessourceType> _ressources;
    public List<RessourceType> Ressources { get { return _ressources; } }

    //public List<DataGadgetUpgrade> Upgrades;
    public Currency Price;
}

public enum GadgetType
{
    Launcher,
    Controller,
    Weapon,
    Armor,
    Accessory
}

[Serializable]
public enum GadgetInput
{
    Tap,
    Hold,
    Drag
}

[Serializable]
public enum GadgetSlot
{
    Null,
    Accessory,
    Weapon,
    Armor,
    Controller1,
    Controller2,
    Launcher
}

[Serializable]
public abstract class DataGadgetUpgrade : Data
{
    [SerializeField]
    bool isUsingDefaultPrice = false;

    public virtual bool IsUpgradable(GadgetInstance gadgetInstance)
    {
        //Debug.Log("Upgrade.IsUpgradable  : " + (GameManager.Instance.PlayerProfile.Inventory.Currency.Coin >= (int)GetPrice(gadgetInstance)));
        return GameManager.Instance.PlayerProfile.Inventory.Currency.Coin >= (int)GetPrice(gadgetInstance);
    }

    public virtual float GetPrice(GadgetInstance gadgetInstance)
    {
        AnimationCurve curve = isUsingDefaultPrice ? GameManager.Instance.GameDefinition.DataShop.DefaultPrices : Price;
        UpgradeData upgradeData = gadgetInstance.Upgrades.Find(x => x.Id == _id);
        if (upgradeData == null)
            return 0.0f;

        return curve.Evaluate((float)upgradeData.Level);
    }

    public virtual float GetValue(GadgetInstance gadgetInstance)
    {
        AnimationCurve curve = Value != null ? Value : GameManager.Instance.GameDefinition.DataShop.DefaultValues;
        UpgradeData upgradeData = gadgetInstance.Upgrades.Find(x => x.Id == _id);
        if (upgradeData == null)
            return 0.0f;

        return curve.Evaluate((float)upgradeData.Level);
    }

    public AnimationCurve Price;
    public AnimationCurve Value;
}