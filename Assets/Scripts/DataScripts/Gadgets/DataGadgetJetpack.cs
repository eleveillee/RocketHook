﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class DataGadgetJetpack : DataGadget
{
    public override GadgetType Type { get { return GadgetType.Controller; } }

    [Header("Upgrades")]
    [SerializeField]
    public UpgradeJetpackPower UpgradeJetpackPower = new UpgradeJetpackPower();
    
    [SerializeField]
    public UpgradeJetpackFuelCost UpgradeJetpackFuelCost = new UpgradeJetpackFuelCost();
}


[Serializable]
public class UpgradeJetpackPower : DataGadgetUpgrade
{
}

[Serializable]
public class UpgradeJetpackFuelCost : DataGadgetUpgrade
{
}