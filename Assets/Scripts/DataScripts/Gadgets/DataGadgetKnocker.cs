﻿using UnityEngine;
using System;
using System.Collections;

public class DataGadgetKnocker : DataGadget
{
    public override GadgetType Type { get { return GadgetType.Controller; } }

    [Header("Upgrades")]
    [SerializeField]
    public UpgradeKnockerPower UpgradeKnockerPower = new UpgradeKnockerPower();

    [SerializeField]
    public UpgradeKnockerFuelCost UpgradeKnockerFuelCost = new UpgradeKnockerFuelCost();
}


[Serializable] public class UpgradeKnockerPower : DataGadgetUpgrade {}
[Serializable] public class UpgradeKnockerFuelCost : DataGadgetUpgrade {}
