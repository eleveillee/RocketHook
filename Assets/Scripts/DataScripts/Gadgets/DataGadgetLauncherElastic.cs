﻿using UnityEngine;
using System;
using System.Collections;

public class DataGadgetLauncherElastic : DataGadget
{
    public override GadgetType Type { get { return GadgetType.Launcher; } }

    [Header("Upgrades")]
    [SerializeField]
    public UpgradeLauncherElastic_Power UpgradeLauncherElastic_Power = new UpgradeLauncherElastic_Power();
}


[Serializable] public class UpgradeLauncherElastic_Power : DataGadgetUpgrade {}
