﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class DataGadgetRoll : DataGadget
{
    public override GadgetType Type { get { return GadgetType.Controller; } }

    [Header("Upgrades")]
    [SerializeField]
    public UpgradeRollPower UpgradeRollPower = new UpgradeRollPower();
    
    [SerializeField]
    public UpgradeRollFuelCost UpgradeRollFuelCost = new UpgradeRollFuelCost();
}


[Serializable]
public class UpgradeRollPower : DataGadgetUpgrade
{
}

[Serializable]
public class UpgradeRollFuelCost : DataGadgetUpgrade
{
}