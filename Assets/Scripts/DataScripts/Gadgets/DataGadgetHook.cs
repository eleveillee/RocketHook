﻿using UnityEngine;
using System;
using System.Collections;

public class DataGadgetHook : DataGadget
{
    public override GadgetType Type {get { return GadgetType.Controller; }}
    
    [Header("Upgrades")]
    [SerializeField] public UpgradeHookPullStrength UpgradeHookPullStrength = new UpgradeHookPullStrength();
    [SerializeField] public UpgradeHookRange UpgradeHookRange = new UpgradeHookRange();
    [SerializeField] public UpgradeHookDamage UpgradeHookDamage = new UpgradeHookDamage();
}

[Serializable] public class UpgradeHookPullStrength : DataGadgetUpgrade{}
[Serializable] public class UpgradeHookRange : DataGadgetUpgrade{}
[Serializable] public class UpgradeHookDamage : DataGadgetUpgrade { }