﻿using UnityEngine;
using System;
using System.Collections;

public class DataGadgetLauncherBooster : DataGadget
{
    public override GadgetType Type { get { return GadgetType.Launcher; } }

    [Header("Upgrades")]
    [SerializeField]
    public UpgradeLauncherBooster_Power UpgradeLauncherBooster_Power = new UpgradeLauncherBooster_Power();

    [SerializeField]
    public UpgradeLauncherBooster_Duration UpgradeLauncherBooster_Duration = new UpgradeLauncherBooster_Duration();

    [SerializeField]
    public UpgradeLauncherBooster_Control UpgradeLauncherBooster_Control = new UpgradeLauncherBooster_Control();
}


[Serializable] public class UpgradeLauncherBooster_Power : DataGadgetUpgrade {}
[Serializable] public class UpgradeLauncherBooster_Duration : DataGadgetUpgrade {}
[Serializable] public class UpgradeLauncherBooster_Control : DataGadgetUpgrade {}
