﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class DataGadgetJump : DataGadget
{
    public override GadgetType Type { get { return GadgetType.Controller; } }
    
    [Header("Upgrades")]
    [SerializeField]
    public UpgradeJumpPower UpgradeJumpPower = new UpgradeJumpPower();
    
    [SerializeField]
    public UpgradeJumpFuelCost UpgradeJumpFuelCost = new UpgradeJumpFuelCost();
    
}


[Serializable]
public class UpgradeJumpPower : DataGadgetUpgrade
{
}

[Serializable]
public class UpgradeJumpFuelCost : DataGadgetUpgrade
{
}