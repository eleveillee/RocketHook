﻿using System;
using UnityEngine;

public abstract class DataSpawnable : DataScriptable
{
    public abstract SpawnableType SpawnableType { get; }

    public abstract T GetClass<T>() where T : DataScriptable;
    
    //public DataSpawnable(){}
}

public enum SpawnableType
{
    Platform,
    Monster,
    PowerUp,
    ObjectGroup,
    Ground
}