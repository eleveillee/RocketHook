﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;

public class AssetGameDefinition : ScriptableObject
{
    [Header("Data instances")]
    [SerializeField]
    private List<DataGoalScriptable> _goals;
    public List<DataGoalScriptable> Goals { get { return _goals; }}

    [SerializeField]
    private List<DataGadget> _gadgets;
    public List<DataGadget> Gadgets { get { return _gadgets; } }

    [SerializeField]
    private List<DataEncounter> _encounters;
    public List<DataEncounter> Encounters { get { return _encounters; } }

    [Header("Data Spawnables")]
    [SerializeField]
    private List<DataMonster> _monsters;
    public List<DataMonster> Monsters { get { return _monsters; } }

    [SerializeField]
    private List<DataPowerUp> _powerUps;
    public List<DataPowerUp> PowerUps { get { return _powerUps; } }
    
    [SerializeField]
    private List<DataPlatform> _platforms;
    public List<DataPlatform> Platforms { get { return _platforms; } }

    [SerializeField]
    private List<DataObjectGroup> _objectGroups;
    public List<DataObjectGroup> ObjectGroups { get { return _objectGroups; } }

    [Header("Data paramaters")]
    [SerializeField]
    private DataShop _dataShop;
    public DataShop DataShop { get { return _dataShop; } }
    
    [Header("Databases")]
    [SerializeField]
    private DatabaseSpawnablePrefabs _databaseSpawnablePrefabs;
    public DatabaseSpawnablePrefabs DatabaseSpawnablePrefabs { get { return _databaseSpawnablePrefabs; } }

    [SerializeField]
    private DatabaseAudio _databaseAudio;
    public DatabaseAudio DatabaseAudio { get { return _databaseAudio; } }


    public void UpdateDatas(List<DataGoalScriptable> goals, List<DataGadget> gadgets, List<DataEncounter> encounters, List<DataMonster> monsters, List<DataPowerUp> powerUps, List<DataPlatform> platforms, List<DataObjectGroup> objectGroups)
    {
        _goals = goals;
        _gadgets = gadgets;
        _encounters = encounters;
        _monsters = monsters;
        _platforms = platforms;
        _powerUps = powerUps;
        _objectGroups = objectGroups;
    }

    public List<DataEncounter> GetEncounters(EncounterType type)
    {
        return _encounters.FindAll(x => x.EncounterType == type);
    }

    public DataSpawnable GetSpawnableById(string id)
    {
        DataSpawnable data;
        data = _monsters.Find(x => x.Id == id);
        if (data != null)
            return data;
        
        data = _powerUps.Find(x => x.Id == id);
        if (data != null)
            return data;
        
        data = _platforms.Find(x => x.Id == id);
        if (data != null)
            return data;

        return null;
    }
}

