﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class DatabaseGadgetPrefabs : ScriptableObject
{
    [SerializeField]
    private List<GadgetPrefab> _gadgetPrefabs = new List<GadgetPrefab>();

    public GameObject GetGadgetPrefab(GadgetType _type)
    {
        return _gadgetPrefabs.Find(x => x.Type == _type).Prefab;
    }

}

[Serializable]
public class GadgetPrefab
{
    [SerializeField]
    private GadgetType _type;

    [SerializeField]
    private GameObject _prefab;

    public GadgetType Type
    {
        get { return _type; }
        set { _type = value; }
    }

    public GameObject Prefab
    {
        get { return _prefab; }
        set { _prefab = value; }
    }
}