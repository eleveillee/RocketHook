﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class DatabaseSprites : ScriptableObject
{
    [SerializeField]
    private List<RessourceSprite> _ressourceSprites = new List<RessourceSprite>();

    public Sprite GetRessourceSprite(RessourceType _type)
    {
        return _ressourceSprites.Find(x => x.Type == _type).Sprite;
    }

}

[Serializable]
public class RessourceSprite
{
    [SerializeField]
    private RessourceType _type;

    [SerializeField]
    private Sprite _sprite;

    public RessourceType Type
    {
        get { return _type; }
        set { _type = value; }
    }

    public Sprite Sprite
    {
        get { return _sprite; }
        set { _sprite = value; }
    }
}