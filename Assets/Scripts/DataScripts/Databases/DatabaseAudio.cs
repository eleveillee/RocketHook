﻿using UnityEngine;
using System.Collections.Generic;

public class DatabaseAudio : ScriptableObject
{
    [Header("Audio")]
    [SerializeField]
    private AudioClip _gameBeat;
    public AudioClip GameBeat { get { return _gameBeat; } }
    //private int _gameBeatAudioID;
    //public int GameBeatAudioID { get { return _gameBeatAudioID; } }

    [SerializeField]
    private List<AudioClip> _hurtAudio;
    public List<AudioClip> HurtAudio { get { return _hurtAudio; } }
    //private List<int> _hurtAudioID;
    //public List<int> HurtAudioID { get { return _hurtAudioID; } }

    [SerializeField]
    private List<AudioClip> _slashAudio;
    public List<AudioClip> SlashAudio { get { return _slashAudio; } }
    //private List<int> _slashAudioID;
    //public List<int> SlashAudioID { get { return _slashAudioID; } }

    [SerializeField]
    private AudioClip _powerUpAudio;
    public AudioClip PowerUpAudio { get { return _powerUpAudio; } }
    //private List<int> _powerUpAudioID;
    //public List<int> PowerUpAudioID { get { return _powerUpAudioID; } }

    //void Awake()
    //{         
    //    _gameBeatAudioID = SoundManager.GetMusicAudio(_gameBeat).audioID;

    //    _hurtAudioID = ListAudioIDs(_hurtAudio);
    //    _slashAudioID = ListAudioIDs(_slashAudio);
    //    _powerUpAudioID = ListAudioIDs(_powerUpAudio);       
      
    //}

    //List<int> ListAudioIDs(List<AudioClip> clipList)
    //{
    //    List<int> audioIDList = new List<int>();

    //    foreach (AudioClip ac in clipList)        
    //        audioIDList.Add(SoundManager.GetSoundAudio(ac).audioID);
        
    //    return audioIDList;
    //}
}



