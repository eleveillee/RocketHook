﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class DatabaseSpawnablePrefabs : ScriptableObject
{
    [SerializeField]
    private List<SpawnablePrefab> _spawnablePrefabs = new List<SpawnablePrefab>();

    public GameObject Get(SpawnableType _type)
    {
        return _spawnablePrefabs.Find(x => x.Type == _type).Prefab;
    }

}

[Serializable]
public class SpawnablePrefab
{
    [SerializeField]
    private SpawnableType _type;
    public SpawnableType Type
    {
        get { return _type; }
        set { _type = value; }
    }

    [SerializeField]
    private GameObject _prefab;
    public GameObject Prefab
    {
        get { return _prefab; }
        set { _prefab = value; }
    }
}