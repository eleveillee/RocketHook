﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class DataMonster : DataSpawnable
{
    public override SpawnableType SpawnableType { get { return SpawnableType.Monster; } }

    public override T GetClass<T>()
    {
        return (T) Convert.ChangeType(this, GetType());
    }

    [SerializeField]
    private float _maxHP;
    public float MaxHp
    {
        get { return _maxHP; }
    }

    [SerializeField]
    private float _damage;
    public float Damage
    {
        get { return _damage; }
    }

    [SerializeField]
    private int _killPoints;
    public int KillPoints
    {
        get { return _killPoints; }
    }

    [SerializeField]
    private float _aggroRange;
    public float AggroRange
    {
        get { return _aggroRange; }
        set { _aggroRange = value; }
    }

    [SerializeField]
    private float _speed;
    public float Speed
    {
        get { return _speed; }
        set { _speed = value; }
    }

    [SerializeField]
    private float _pushForce;
    public float PushForce
    {
        get { return _pushForce; }
        set { _pushForce = value; }
    }

    [SerializeField]
    private float _scale;
    public float Scale
    {
        get { return _scale; }
        set { _scale = value; }
    }

    [SerializeField]
    private Vector2 _colliderSize;
    public Vector2 ColliderSize
    {
        get { return _colliderSize; }
        set { _colliderSize = value; }
    }

    [SerializeField]
    private RuntimeAnimatorController _animatorController;
    public RuntimeAnimatorController AnimatorController
    {
        get { return _animatorController; }
        set { _animatorController = value; }
    }

    [SerializeField]
    private Sprite _sprite;
    public Sprite Sprite
    {
        get { return _sprite; }
        set { _sprite = value; }
    }

 
    [SerializeField]
    private BehaviourType _behaviour;
    public BehaviourType Behaviour
    {
        get { return _behaviour; }
        set { _behaviour = value; }
    }        
}

public enum BehaviourType
{
    Fixed,
    Patrol,
    SeekTarget,
    Special
}



