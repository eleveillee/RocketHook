﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Lean;
using System;

public class DataPowerUp : DataSpawnable
{
    public override SpawnableType SpawnableType { get { return SpawnableType.PowerUp; } }

    public override T GetClass<T>()
    {
        return (T) Convert.ChangeType(this, GetType());
    }

    [SerializeField]
    private Sprite _sprite;

    [SerializeField]
    private PowerUpType _type;
    
    [SerializeField]
    private int _hookValue = 1;

    [SerializeField]
    private float _fuelValue = 25f;

    [SerializeField]
    private float _hpValue = 2f;

    [SerializeField]
    private Currency _coinCurrency = new Currency(1, 0);

    public Sprite Sprite
    {
        get { return _sprite; }
    }

    public int HookValue
    {
        get { return _hookValue; }
    }

    public float FuelValue
    {
        get { return _fuelValue; }
    }

    public float HpValue
    {
        get { return _hpValue; }
    }

    public Currency CoinCurrency
    {
        get { return _coinCurrency; }
    }

    public PowerUpType Type
    {
        get { return _type; }
    }
}

public enum PowerUpType
{
    Hook,
    Fuel,
    Hp,
    GoldCoin
}


