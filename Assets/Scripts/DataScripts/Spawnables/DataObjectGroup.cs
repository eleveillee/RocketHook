﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Lean;

public class DataObjectGroup : DataSpawnable
{
    public override SpawnableType SpawnableType { get { return SpawnableType.ObjectGroup; } }

    public override T GetClass<T>()
    {
        return (T) Convert.ChangeType(this, GetType());
    }

    [SerializeField]
    private List<ObjectPosition> _objectPositions;
    public List<ObjectPosition> ObjectPositions
    {
        get { return _objectPositions; }
    }
}

[Serializable]
public class ObjectPosition
{
    [SerializeField]
    private string _id;
    public string Id
    {
        get { return _id; }
    }

    [SerializeField]
    private Vector2 _position;
    public Vector2 Position
    {
        get { return _position; }
    }

}

