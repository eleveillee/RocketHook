﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Lean;
using System;

public class DataPlatform : DataSpawnable
{
    public override SpawnableType SpawnableType { get { return SpawnableType.Platform; } }

    public override T GetClass<T>()
    {
        return (T) Convert.ChangeType(this, GetType());
    }

    [SerializeField]
    private Vector2 _dataSize = new Vector2(25f,100f);
    public Vector2 Size
    {
        get { return _dataSize; }
    }

    //Rotation, rotation chance,
    // Sliding, damage, ect
}


