﻿using UnityEngine;
using System.Collections;

public interface IDamageable
{

    void SetHp(float value);
    
    void AddHp(float value);

    //void Die();

}
