﻿using UnityEngine;
using System.Collections;

public interface ISpawnable
{
    void Initialize(DataSpawnable data, DataLevelParameters dataLevelParamaters);
    bool IsPositionGood();
}
