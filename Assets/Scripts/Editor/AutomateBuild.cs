﻿using System;
using System.Linq;
using System.Collections;
using UnityEditor;
//using UnityEngine.Windows;

public static class AutomateBuild
{
    
    // This is called by the editor menu.
    [MenuItem("Automation/Build Windows Player")]
    public static void BuildWindowsPlayerInEditor()
    {
        //DoBuildWindowsPlayer(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
        DoBuildWindowsPlayer("C:/Users/Eric/Documents/Unity/RocketHook/Builds/RocketHook.exe");

    }

    // this is called by the CI server
    public static void BuildWindowsPlayerInCI()
    {
        UnityEngine.Debug.Log("[AutomateBuild] BuildWindowsPlayerInCI.Start");
        var outputDir = GetArg("-outputDir"); // read the "-outputDir" command line argument
        DoBuildWindowsPlayer(outputDir);
        UnityEngine.Debug.Log("[AutomateBuild] BuildWindowsPlayerInCI.Finish");
        EditorApplication.Exit(0);
    }

    // This actually builds a windows player.
    private static void DoBuildWindowsPlayer(string outputDir)
    {
        if (outputDir == null)
            throw new ArgumentException("No output folder specified.");

        //if (!UnityEngine.Windows.Directory.Exists(outputDir))
        //    UnityEngine.Windows.Directory.CreateDirectory(outputDir);
        
        var scenes = EditorBuildSettings.scenes.Select(scene => scene.path).ToArray(); // get the scenes from the settings dialog

        BuildPipeline.BuildPlayer(scenes, outputDir + "/RocketHook.exe",
                                  BuildTarget.StandaloneWindows,
                                  BuildOptions.None
        );

    }


    // Helper function for getting the command line arguments
    private static string GetArg(string name)
    {
        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == name && args.Length > i + 1)
            {
                return args[i + 1];
            }
        }
        return null;
    }

}

// https://effectiveunity.com/articles/making-most-of-unitys-command-line.html
// C:\Program Files\Unity\Unity.exe ^
//    -outputDir "c:\temp\output" ...

// read the "-outputDir" command line argument
//var outputDir = GetArg("-outputDir");

/*

    */
