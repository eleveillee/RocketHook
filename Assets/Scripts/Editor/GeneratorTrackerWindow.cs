﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

public class GeneratorTrackerWindow : EditorWindow
{
    [SerializeField]
    GeneratorTracker _generatorTracker;
    bool wasReady;

    GTMenuEnum CurrentMenu;

    // Chunk Variables
    Vector2 scrollPos;
    bool _isRawChunkShown = true;
    bool _isProcChunkShown = true;
    RawChunkFilterEnum _chunkFilterEnum = RawChunkFilterEnum.Time;
    RawChunkFilterEnum _chunkFilterEnum_prev;
    List<GenerationInstance> _orderedChunkDatas;


    // Add menu named "My Window" to the Window menu
    [MenuItem("RocketHook/Generator Tracker")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        ((GeneratorTrackerWindow)EditorWindow.GetWindow(typeof(GeneratorTrackerWindow))).Show();
    }

    void OnEnable()
    {
        _generatorTracker = FindObjectOfType<GeneratorTracker>();
        wasReady = false;
    }

    void Update()
    {
        if (_generatorTracker != null && _generatorTracker.IsReady && !wasReady)
        {
            Repaint();
            wasReady = false;
        }
    }

    //void OnEnable()
    void OnGUI()
    {
        if (_generatorTracker == null)
            _generatorTracker = FindObjectOfType<GeneratorTracker>();
        _generatorTracker = (GeneratorTracker)EditorGUILayout.ObjectField(_generatorTracker, typeof(GeneratorTracker));
        

        if (_generatorTracker == null)
            return;
        
        DrawState();

        if (!_generatorTracker.IsReady)
            return;

        DrawWindow();
    }
    
    void DrawMain()
    {
        GUILayout.Label("MAIN", EditorStyles.boldLabel);

        float generationTime = _generatorTracker.LevelStateDatas[EnumGeneratorState.LevelReady.ToString()] - _generatorTracker.LevelStateDatas[EnumGeneratorState.SpawnNewWorld.ToString()];
        int nbrChunk = (_generatorTracker.LevelStateDatas.Count - 3)/2;
        GUILayout.Label("Generation time : " + generationTime.ToString("0.00"));
        GUILayout.Label("Time Per Chunk : " + (generationTime/(float)nbrChunk).ToString("0.00"));
        GUILayout.Label("Chunks : " + nbrChunk);

    }

    void DrawLevel()
    {
        GUILayout.Label("LEVEL", EditorStyles.boldLabel);

        GUILayout.Label("LevelGenerator : " + _generatorTracker.LevelStateDatas.Count);
        foreach (KeyValuePair<string, float> pair in _generatorTracker.LevelStateDatas)
        {
            GUILayout.Label(pair.Key + " : " + pair.Value);
        }
    }

    void DrawChunk()
    {
        GUILayout.Label("CHUNK", EditorStyles.boldLabel);


        /*//Processed Datas
        _isProcChunkShown = GUILayout.Toggle(_isProcChunkShown, "Show Raw Datas");
        if (_isProcChunkShown)
        {

        }*/

        //Raw Datas
        _isRawChunkShown = GUILayout.Toggle(_isRawChunkShown, "Show Raw Datas");
        _chunkFilterEnum = (RawChunkFilterEnum)EditorGUILayout.EnumPopup("Filter" , _chunkFilterEnum);
        if (_orderedChunkDatas == null)
            _orderedChunkDatas = _generatorTracker.ChunkDatas;

        if (_chunkFilterEnum != _chunkFilterEnum_prev)
        {
            if (_chunkFilterEnum == RawChunkFilterEnum.SpawnOrder)
                _orderedChunkDatas = _generatorTracker.ChunkDatas;
            else if (_chunkFilterEnum == RawChunkFilterEnum.Time)
                _orderedChunkDatas = _generatorTracker.ChunkDatas.OrderByDescending(x => x.Time).ToList();
            else if (_chunkFilterEnum == RawChunkFilterEnum.NumberTry)
                _orderedChunkDatas = _generatorTracker.ChunkDatas.OrderByDescending(x => x.NumberTry).ToList();

            _chunkFilterEnum_prev = _chunkFilterEnum;
        }

        
        if (_isRawChunkShown)
        {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos); // , GUILayout.Width(100), GUILayout.Height(100)
            // GUILayout.Label("ChunkGeneration : " + _generatorTracker.ChunkDatas.Count);
            foreach (GenerationInstance gi in _orderedChunkDatas)
            {
                string timeShown = gi.Time < 0.0001 ? "0" : gi.Time.ToString();

                EditorGUILayout.BeginHorizontal();
                GUILayout.Label(gi.DataSpawnable.Name + "(" + gi.Chunk.x + "," + gi.Chunk.y +  ")", GUILayout.Width(200));
                GUILayout.Label(string.Format("{0:0000}", timeShown), GUILayout.Width(50));
                if (gi.NumberTry > 1)
                    GUILayout.Label(" ( " + gi.NumberTry + ")");

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();
        }
    }

    void DrawButtons()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Main", GUILayout.MaxWidth(200)))
            ChangeMenu(GTMenuEnum.Main);

        if (GUILayout.Button("Level", GUILayout.MaxWidth(200)))
            ChangeMenu(GTMenuEnum.Level);

        if (GUILayout.Button("Chunk", GUILayout.MaxWidth(200)))
            ChangeMenu(GTMenuEnum.Chunk);

        GUILayout.EndHorizontal();
    }
    
    void DrawWindow()
    {
        DrawButtons();
        switch (CurrentMenu)
        {
            case GTMenuEnum.Main:
                DrawMain();
                break;

            case GTMenuEnum.Level:
                DrawLevel();
                break;

            case GTMenuEnum.Chunk:
                DrawChunk();
                break;

            default:
                Debug.LogWarning("[GDV]WrongEnum");
                break;
        }
    }

    void DrawState()
    {
        if (Application.isPlaying)
        {
            if (_generatorTracker.IsReady)
                GUILayout.Label("STATE: Display results.");
            else
                GUILayout.Label("STATE: Waiting results.");
        }
        else
        {
            if (!_generatorTracker.IsReady)
            {
                GUILayout.Label("STATE: Start game and generate level for datas");
                return;
            }
            else
            {
                GUILayout.Label("STATE: Displaying results from last runtime");
            }
        }
    }

    void ChangeMenu(GTMenuEnum menu)
    {
        CurrentMenu = menu;
    }

    // Enums


    enum RawChunkFilterEnum
    {
        SpawnOrder,
        Time,
        NumberTry
    }

    enum GTMenuEnum
    {
        Main,
        Level,
        Chunk
    }
}