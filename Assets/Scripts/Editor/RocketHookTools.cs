﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class RocketHookTools
{
    static AssetGameDefinition _gameDefinition;
    // This is called by the editor menu.
    [MenuItem("RocketHook/Update GameDefinition")]
    public static void UpdateGameDefinition()
    {
        _gameDefinition = Resources.Load("Data/GameDefinition", typeof(AssetGameDefinition)) as AssetGameDefinition;
        //_gameDefinition.Goals = FindAssetsByType<DataGoalScriptable>();
        _gameDefinition.UpdateDatas(FindAssetOfType<DataGoalScriptable>(), FindAssetOfType<DataGadget>(), FindAssetOfType<DataEncounter>(), FindAssetOfType<DataMonster>(), FindAssetOfType<DataPowerUp>(), FindAssetOfType<DataPlatform>(), FindAssetOfType<DataObjectGroup>());
        EditorUtility.SetDirty(_gameDefinition);
    }

    public static List<T> FindAssetOfType<T>() where T : UnityEngine.Object
    {
        List<T> assets = new List<T>();
        //string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T).ToString().Replace("UnityEngine.", "")));

        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (asset != null)
            {
                assets.Add(asset);
            }
        }
        return assets;
    }


}