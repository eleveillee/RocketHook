﻿using UnityEngine;
using System.Text;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(StoryManager))]
public class StoryManagerInspector : Editor
{
    StoryManager _storyManager;

    public override void OnInspectorGUI()
    {
        _storyManager = (StoryManager)target;
        GUIStyle guiStyle = new GUIStyle();

        if (!TestManager())
            return;

        EditorGUILayout.LabelField("[STATE] Running Story(" + _storyManager.CurrentStoryInstance.Level + ") : " + _storyManager.CurrentStoryInstance.Name, EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Step : " + _storyManager.CurrentStoryInstance.StepHolders.Count);
        for (int i = 0; i < _storyManager.CurrentStoryInstance.StepHolders.Count; i++)
        {
            bool _isCurrentStep = i == _storyManager.CurrentStoryInstance.CurrentStep;
            guiStyle.fontStyle = _isCurrentStep ? FontStyle.Bold : FontStyle.Normal;

            guiStyle.normal.textColor = Color.black;
            GameObject go = _storyManager.CurrentStoryInstance.StepHolders[i];
            StepCompleteCondition stepCompleteCondition = go.GetComponent<StepCompleteCondition>();
            EditorGUILayout.LabelField("o " + go.name + "[" + stepCompleteCondition.Operator + "]", guiStyle);

            foreach (CompleteCondition completeCondition in stepCompleteCondition.CompleteConditions)
            {
                if(_isCurrentStep)
                    guiStyle.normal.textColor = completeCondition.IsComplete ? Color.green : Color.red;
                    

                string conditionString = completeCondition.Type.ToString();
                foreach (string s in completeCondition.Values)
                {
                    conditionString += s;
                }

                EditorGUILayout.LabelField("  + " + conditionString + " : " + completeCondition.IsComplete , guiStyle);
            }

            guiStyle.normal.textColor = Color.black;

            foreach (StoryStep storyStep in go.GetComponents<StoryStep>())
            {
                EditorGUILayout.LabelField("    -> " + storyStep.GetType(), guiStyle);
            }
        }

        Repaint();
    }
    
    bool TestManager()
    {
        string nullFound = "";

        if (_storyManager == null)
            nullFound = "[STATE] Error: Can't find StoryManager";
        else if (_storyManager.CurrentStoryInstance == null)
            nullFound = "[STATE] No story Instance";
        else if (_storyManager.CurrentStoryInstance.StepHolders == null || _storyManager.CurrentStoryInstance.StepHolders.Count == 0)
            nullFound = "[STATE] No StorySteps";
        EditorGUILayout.LabelField(nullFound, EditorStyles.boldLabel);
        return nullFound == "";
    }
}