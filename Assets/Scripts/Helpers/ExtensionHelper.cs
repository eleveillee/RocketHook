﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

using Random = UnityEngine.Random;

public static class ExtensionHelper {

    // Action
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
            action.Invoke();
    }

    // Vector2
    public static float GetRandomValue(this Vector2 vector)
    {
        return Random.Range(vector.x, vector.y);
    }

    // Text
    public static void SetValue(this UnityEngine.UI.Text text, float value, float duration = 0.0f)
    {
        text.text = String.Format("{0:0}", value);
    }

    // List
    public static T GetRandomValue<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }
         
    public static void MoveItemAtIndexToFront<T>(this List<T> list, int index)
    {
        T item = list[index];
        for (int i = index; i > 0; i--)
            list[i] = list[i - 1];
        list[0] = item;
    }

    // Bool
    public static bool IsSpendable(this Currency currency)
    {
        return GameManager.Instance.PlayerProfile.Inventory.Currency.Has(currency);
    }
    
    // Transform
    public static void SetY(this Transform transform, float value)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, value, transform.localPosition.z);
    }

    // GameObject
    public static void ToggleActive(this GameObject go)
    {
        go.SetActive(!go.activeSelf);
    }

}
