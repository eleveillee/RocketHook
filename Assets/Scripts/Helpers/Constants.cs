﻿using System;
using System.Reflection;

using Random = UnityEngine.Random;

public static class Constants {

    // Add Basic Gadgets
    public const string GADGET_MAGNET = "d113d643-9b40-45de-9f32-03866a61c75c"; // Magnet

    public const string GADGET_JETPACK = "717ee925-b4fc-4e7c-84ae-c8de9c12f9a0"; // Jetpack
    public const string GADGET_HOOK = "87eb8dbe-3e24-4d34-90ff-aed320411596"; // Hook
    public const string GADGET_KNOCKER = "caf6ba56-50f2-4b90-8f1e-1071d2049a74"; // Knocker
    public const string GADGET_ROLL = "8dee32af-bb30-476a-b614-310caf870660"; // Roll
    public const string GADGET_JUMP = "2f1af087-48f3-4ba1-840e-f278a02625a4"; // Jump
   
    public const string GADGET_LAUNCHERBOOSTER = "7ea0aa90-e64a-4997-b610-59f0f37be2f6"; // Launcher Booster
    public const string GADGET_LAUNCHERELASTIC = "978cfeb5-044e-467a-99b0-c0c59ffd7cea"; // Launcher Elastic

    public const string ENCOUNTER_STORY1 = "ee1574ce-ac2f-4b9c-8cc8-a72f7ed3be46"; // Encounter Story1
    public const string ENCOUNTER_TUTORIAL1 = "ff2674de-ac2f-4b9c-8cc8-a72f7ed3be46"; // Encounter Tutorial1

    public const string STORY_TUTO1 = "5e960e39-82b4-41ca-bb78-043bbecf8a7d"; // Encounter Story1

    public const int LAYER_PLAYER= 8;
    public const int LAYER_PLAYERONEWAY = 9;

    public const int LAYER_GROUND = 12;
    public const int LAYER_GROUNDONEWAY = 13;

}
