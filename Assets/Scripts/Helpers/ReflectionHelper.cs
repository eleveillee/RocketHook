﻿using System;
using System.Reflection;

using Random = UnityEngine.Random;

public static class ReflectionHelper {
    
    public static T GetReference<T>(object inObj, string fieldName) where T : class
    {
        return GetField(inObj, fieldName) as T;
    }


    public static T GetValue<T>(object inObj, string fieldName) where T : struct
    {
        return (T)GetField(inObj, fieldName);
    }


    public static void SetField(object inObj, string fieldName, object newValue)
    {
        FieldInfo info = inObj.GetType().GetField(fieldName);
        if (info != null)
            info.SetValue(inObj, newValue);
    }


    private static object GetField(object inObj, string fieldName)
    {
        object ret = null;
        FieldInfo info = inObj.GetType().GetField(fieldName);
        if (info != null)
            ret = info.GetValue(inObj);
        return ret;
    }
}
