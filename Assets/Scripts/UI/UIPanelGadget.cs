﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIPanelGadget : UIPanel {
    
    [SerializeField]
    private GameObject _cellTransactionPrefab;

    [SerializeField]
    GameObject _CellsGadgetUpgradeRoot;

    [SerializeField]
    private Text _gadgetNameText;

    List<UICellGadgetUpgrade> _cellGadgetUpgrades = new List<UICellGadgetUpgrade>();

    private int _currentGadgetIndex;

    protected override void OnOpen()
    {
        ChangeGadget(_currentGadgetIndex);
    }

    public void ChangeGadget(int direction)
    {
        ClearCellUpgrades();
        int gadgetListSize = GameManager.Instance.PlayerProfile.Inventory.Gadgets.Count; // Enum.GetValues(typeof(GadgetType)).Length;
        int newIndex = _currentGadgetIndex += direction;
        if (newIndex < 0)
            _currentGadgetIndex = gadgetListSize - 1;
        else if (newIndex >= gadgetListSize)
            _currentGadgetIndex = 0;
        else
            _currentGadgetIndex = newIndex;
        UpdateDatas();
    }

    public void UpdateDatas()
    {
        GadgetInstance currentGadget = GameManager.Instance.PlayerProfile.Inventory.Gadgets[_currentGadgetIndex];
        _gadgetNameText.text = currentGadget.Data.Name.ToString();

        List<DataGadgetUpgrade> dataUpgrades = GameManager.Instance.GameDefinition.DataShop.DataGadgetUpgrades[currentGadget.Data];
        foreach(DataGadgetUpgrade dataGadgetUpgrade in dataUpgrades)
        {
            SpawnCell(currentGadget, dataGadgetUpgrade);
        }
    }

    void SpawnCell(GadgetInstance gadgetInstance, DataGadgetUpgrade dataGadgetUpgrade)
    {
        UICellGadgetUpgrade cellGadgetUpgrade = Instantiate(_cellTransactionPrefab).GetComponent<UICellGadgetUpgrade>();
        cellGadgetUpgrade.transform.SetParent(_CellsGadgetUpgradeRoot.transform);
        cellGadgetUpgrade.transform.localScale = Vector3.one;
        cellGadgetUpgrade.transform.localPosition = new Vector3(0, -125f * _cellGadgetUpgrades.Count);
        cellGadgetUpgrade.Initialize(gadgetInstance, dataGadgetUpgrade);
        _cellGadgetUpgrades.Add(cellGadgetUpgrade);
    }

    void ClearCellUpgrades()
    {
        foreach (UICellGadgetUpgrade cell in _cellGadgetUpgrades)
        {
            Destroy(cell.gameObject);
        }
        _cellGadgetUpgrades.Clear();
    }
}
