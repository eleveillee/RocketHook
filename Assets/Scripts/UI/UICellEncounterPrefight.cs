﻿using System;
using System.Reflection;

using UnityEngine;
using UnityEngine.UI;

public class UICellEncounterPrefight : MonoBehaviour
{
    [SerializeField] Text _gadgetName;
    [SerializeField] Button _button;
    [SerializeField] Image _imageSelected;

    public Action<DataEncounter> OnClick;
    DataEncounter _dataEncounter;
    EncounterState _encounterState;

    public void Initialize(DataEncounter dataEncounter)
    {
        _dataEncounter = dataEncounter;
        _gadgetName.text = dataEncounter.Name;
        _encounterState = GameManager.Instance.PlayerProfile.EncounterStates.Find(x => x.EncounterId == _dataEncounter.Id);
        UpdateCell();
    }

    //Triggered by Unity UI
    public void OnClick_Button()
    {
        if (OnClick != null)
            OnClick(_dataEncounter);
    }

    public void ToggleCurrentEncounter(bool isSelected)
    {
        _imageSelected.gameObject.SetActive(isSelected);
    }

    public DataEncounter DataEncounter { get {  return _dataEncounter;} }

    void UpdateCell()
    {
        _button.interactable = _encounterState != null;
        //_buttonBackground.color = _encounterState == null ? Color.red : Color.white;
    }
}