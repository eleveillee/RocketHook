﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Lean;

public class UIPanelLoadingScreen : UIPanel
{
    [SerializeField] HorizontalLayoutGroup _gridRoot;
    [SerializeField] UICellLoadingControl _cellPrefab;

    [SerializeField] UnityEngine.UI.Text _waitingText;

    List<UICellLoadingControl> _cells = new List<UICellLoadingControl>();

    private bool _isLevelReady;

    protected override void OnOpen()
    {
        LeanTouch.OnFingerDown += OnFingerDown;
        LevelManager.Instance.Generator.OnLevelReady += OnLevelReady;
        _isLevelReady = false;
        UpdateDatas();
    }

    void OnFingerDown(LeanFinger fingers)
    {
        if (!_isLevelReady)
            return;

        _isLevelReady = false;
        Close();
        LevelManager.Instance.SkipLoadingScreen();
    }
        
    void OnDisable()
    {
        LeanTouch.OnFingerDown -= OnFingerDown;
        LevelManager.Instance.Generator.OnLevelReady -= OnLevelReady;
        //_waitingTextIndex = 0;
    }

    void OnLevelReady()
    {
        _isLevelReady = true;
        _waitingTextIndex = 0;
        //LevelManager.Instance.SkipLoadingScreen();
    }

    void UpdateDatas()
    {
        ClearCells();
        foreach(GadgetInstance gadgetInstance in GameManager.Instance.PlayerProfile.Inventory.Gadgets)
        {
            if (!gadgetInstance.IsEquipped)
                continue;

            foreach(GadgetInput input in gadgetInstance.Data.Inputs)
            {
                UICellLoadingControl cell = ((UICellLoadingControl)Instantiate(_cellPrefab, _gridRoot.transform, false));
                cell.Initialize(gadgetInstance.Data.Name, input);
                _cells.Add(cell);
            }
        }
    }

    void ClearCells()
    {
        foreach (UICellLoadingControl cell in _cells)
        {
            Destroy(cell.gameObject);
        }
        _cells.Clear();
    }

    //  WaitingVisual Timer
    private int _waitingTextIndex;
    private float _lastStep;
    private float _delayStep = 0.75f;
    private List<string> _texts = new List<string>() {"Generating Level.", "Generating Level..", "Generating Level...", "Inventing Physic.", "Inventing Physic..", "Inventing Physic...", "Cloning Monsters.", "Cloning Monsters..", "Cloning Monsters...", "Bribing Apple.", "Bribing Apple..", "Bribing Apple..." };
    void Update()
    {
        if(_isLevelReady)
        {
            _waitingText.text = "Ready!";
        }
        else if (Time.realtimeSinceStartup > _lastStep + _delayStep)
        {
            if (_waitingTextIndex == _texts.Count - 1)
                _waitingTextIndex = 0;

            _waitingText.text = _texts[_waitingTextIndex];
            _waitingTextIndex++;
            _lastStep = Time.realtimeSinceStartup;
        }
    }
}