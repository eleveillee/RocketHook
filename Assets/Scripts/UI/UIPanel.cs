﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class UIPanel : MonoBehaviour
{
    [SerializeField]
    protected GameObject _root;

    protected bool _isOpen;

    public void Close()
    {
        OnClose();
        _isOpen = false;
        _root.SetActive(false);
    }

    public void Open()
    {
        OnOpen();
        _isOpen = true;
        _root.SetActive(true);
    }

    protected virtual void OnOpen(){}
    protected virtual void OnClose(){}
}
