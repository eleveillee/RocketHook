﻿using UnityEngine;
using System.Collections;

public class UIPanelOptions : UIPanel {

    protected override void OnOpen() { }

    public void DeleteSave_Button()
    {
        GameManager.Instance.ResetGame();
    }

    public void CheatMoney_Button()
    {
        GameManager.Instance.CheatGetRich();
    }

    public void GodMode_Button()
    {
        GameManager.Instance.CheatGodMode();
    }
}
