﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UICellRessourceUpgrade : MonoBehaviour
{
    [SerializeField] Text _ressourceLevelText;
    [SerializeField] Text _ressourceLevelValue;
    [SerializeField] SpriteRenderer _ressourceSprite;
    
    [SerializeField] Button _buttonBuy;
    [SerializeField] Text _buttonPrice;

    RessourceLevel _ressourceLevel;
    Action<RessourceLevel> _onRessourceBought;
    
    public void Initialize(RessourceLevel ressourceLevel, Action<RessourceLevel> onRessourceBought)
    {
        _ressourceLevel = ressourceLevel;
        _onRessourceBought = onRessourceBought;
        _ressourceSprite.sprite = GameManager.Instance.DatabaseSprite.GetRessourceSprite(ressourceLevel.Type);
        UpdateValues();
        
    }

    public void UpdateValues()
    {
        _ressourceLevelText.SetValue(_ressourceLevel.Level);
        _ressourceLevelValue.SetValue(LevelManager.Instance.PlayerPrefab.GetComponent<Player>().Data.GetRessourceValue(_ressourceLevel.Type));//LevelManager.Instance.Player.Ressources.Get(_ressourceLevel.Type));
        _buttonPrice.SetValue(_ressourceLevel.GetPrice().Coin);
        _buttonBuy.interactable = _ressourceLevel.GetPrice().IsSpendable();
    }

    public void OnBuy()
    {
        if (!GameManager.Instance.PlayerProfile.Inventory.Currency.TrySpend(_ressourceLevel.GetPrice())) // Spend the currency if true
            return;

        _onRessourceBought(_ressourceLevel);
        UpdateValues();
    }
}