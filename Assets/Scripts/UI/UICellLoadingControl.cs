﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UICellLoadingControl : MonoBehaviour
{
    
   [SerializeField] Text _textGadgetName;
   [SerializeField] Text _textAction;
   [SerializeField] Image _imageActionVisual;
    
    public void Initialize(string gadgetName, GadgetInput input)
    {
        _textGadgetName.text = gadgetName.ToUpper();
        _textAction.text = input.ToString().ToUpper();
    }
}
