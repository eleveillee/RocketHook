﻿using System;
using System.Reflection;

using UnityEngine;
using UnityEngine.UI;

public class UICellGadgetEquip : MonoBehaviour
{ 
    [SerializeField] Text _equippedGadgetText;
    [SerializeField] SpriteRenderer _buttonBackground;
    [SerializeField] RawImage _buttonBorder;
    [SerializeField] GadgetSlot _gadgetSlot;
    [SerializeField] GadgetType _gadgetType;

    public Action<UICellGadgetEquip> OnClick;
    private GadgetInstance _gadget;
    public void Initialize(GadgetInstance gadget) 
    {
        _gadget = gadget;
        //UpdateCell();
    }

    public void OnMouseDown()
    {
        OnClick(this);
    }

    public void Equip(GadgetInstance gadget)
    {
        Debug.Log("Equip null : " + (_gadget == gadget));
        if(_gadget == gadget)
            _gadget = null;
        else
            _gadget = gadget;
        UpdateDatas();
        Debug.Log("CellEquip : " + _gadget);
    }

    public void Toggle(bool isTrue)
    {
        _buttonBorder.color = isTrue ? Color.white : Color.black;
    }

    public GadgetInstance GadgetInstance
    {
        get { return _gadget; }
    }
    
    public GadgetType GadgetType
    {
        get { return _gadgetType; }
    }

    public GadgetSlot GadgetSlot
    {
        get { return _gadgetSlot; }
    }

    public void UpdateDatas()
    {
        _gadget = GameManager.Instance.PlayerProfile.Inventory.GetGadgetSlot(_gadgetSlot);
        _buttonBackground.gameObject.SetActive(_gadget == null);
        _equippedGadgetText.gameObject.SetActive(_gadget != null);
        _equippedGadgetText.text = _gadget != null ? _gadget.Data.Name : "";
    }
}