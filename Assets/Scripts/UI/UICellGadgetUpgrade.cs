﻿using System;

using UnityEngine;
using UnityEngine.UI;

public class UICellGadgetUpgrade : MonoBehaviour
{
    [SerializeField] Text _textUpgradeName;
    [SerializeField] Text _textUpgradeLevel;
    [SerializeField] Text _textUpgradePrice;
    [SerializeField] Button _buttonBuy;

    GadgetInstance _gadgetInstance;
    DataGadgetUpgrade _dataGadgetUpgrade;
    UpgradeData _upgradeData;

    public void Initialize(GadgetInstance gadgetInstance, DataGadgetUpgrade dataGadgetUpgrade) 
    {
        if (dataGadgetUpgrade == null)
            return;

        _gadgetInstance = gadgetInstance;
        _dataGadgetUpgrade = dataGadgetUpgrade;
        _upgradeData = _gadgetInstance.Upgrades.Find(x => x.Id == _dataGadgetUpgrade.Id);

        UpdateDatas();
    }

    public void OnBuy()
    {
        Currency price = new Currency((int)_dataGadgetUpgrade.GetPrice(_gadgetInstance), 0);
        if (!GameManager.Instance.PlayerProfile.Inventory.Currency.TrySpend(price)) // Spend the ressource if available, otherwise return
            return;

        _upgradeData.Level++;
        UpdateDatas();
        SaveSystem.Save();
        //Debug.Log("Level " + _dataGadgetUpgrade.Name + " :" + _upgradeData.Level);
    }

    void UpdateDatas()
    {
        _textUpgradeName.text = _dataGadgetUpgrade.Name;
        _textUpgradeLevel.SetValue(_upgradeData.Level);
        _textUpgradePrice.SetValue(_dataGadgetUpgrade.GetPrice(_gadgetInstance));
        _buttonBuy.interactable = GameManager.Instance.PlayerProfile.Inventory.Currency.Coin >= (int)_dataGadgetUpgrade.GetPrice(_gadgetInstance);
    }
}