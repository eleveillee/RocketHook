﻿using System;

using UnityEngine;
using UnityEngine.UI;

public class UICellGadgetShop : MonoBehaviour
{
    [SerializeField] Text _textGadgetName;
    [SerializeField] Text _textGadgetPrice;
    [SerializeField] Button _buttonBuy;
    public Action<UICellGadgetShop> OnBuy;
    private DataGadget _dataGadget;

    public void Initialize(DataGadget dataGadget)
    {
        if (dataGadget == null)
            return;

        _dataGadget = dataGadget;
        UpdateDatas();
    }
    
    void UpdateDatas()
    { 
        _textGadgetName.text = _dataGadget.Name;
        _textGadgetPrice.SetValue(_dataGadget.Price.Coin);
        _buttonBuy.interactable = GameManager.Instance.PlayerProfile.Inventory.Currency.Coin >= (int)_dataGadget.Price.Coin;
    }

    // Triggered by Unity UI
    public void OnBuy_Button()
    {
        if (!GameManager.Instance.PlayerProfile.Inventory.Currency.TrySpend(_dataGadget.Price)) // Spend the ressource if available, otherwise return
            return;
        
        GameManager.Instance.PlayerProfile.Inventory.AddGadget(_dataGadget.Id);
        UpdateDatas();
        OnBuy(this);
        UIManager.Instance.ToggleNotifiers(); // System should be robust enough to update notifier itself
    }
}