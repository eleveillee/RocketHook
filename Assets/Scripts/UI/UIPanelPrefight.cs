﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class UIPanelPrefight :UIPanel
{
    [SerializeField] UISectionGoals _uiSectionGoals;
    [SerializeField] UIPanelPrefightEncounterSection _uiPanelPrefightEncounterSection;
    
    [SerializeField] GameObject _gridGadget;
    [SerializeField] GameObject _cellGadgetPrefightPrefab;

    List<UICellGadgetPrefight> _cellGadgetPrefight = new List<UICellGadgetPrefight>();

    protected override void OnOpen()
    {
        UpdateDatas();
    }

    void UpdateDatas()
    {
        _uiSectionGoals.UpdateDatas();
        _uiPanelPrefightEncounterSection.OnOpen();
    }
        
    // Triggered by Unity UI
    public void OnEquipGear_Button()
    {
        UIManager.Instance.ShowPanel(PanelType.EquipGear);
    }

    // Triggered by Unity UI
    public void OnPlay_Button()
    {
        StateManager.Instance.ChangeState(GameState.Play);
    }
}