﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class UIPanelEquipGear : UIPanel
{
    
    [SerializeField] GameObject _equippableGadgetGrid;
    [SerializeField] UICellGadgetPrefight _cellPrefab;
    List<UICellGadgetPrefight> _cellsGadgetPrefight = new List<UICellGadgetPrefight>();

    List<UICellGadgetEquip> _cellsGadgetEquip = new List<UICellGadgetEquip>();
    UICellGadgetEquip _currentCellToggled;

    void Awake()
    {
        _cellsGadgetEquip = GetComponentsInChildren<UICellGadgetEquip>(true).ToList();

        foreach(UICellGadgetEquip cell in _cellsGadgetEquip)
        {
            cell.OnClick += OnCellClick;
        }
    }

    protected override void OnOpen()
    {
        _currentCellToggled = _cellsGadgetEquip.Find(x => x.GadgetSlot == GadgetSlot.Controller1);
        UpdateDatas();
    }

    void UpdateDatas()
    {
        foreach (UICellGadgetEquip cell in _cellsGadgetEquip)
        {
            cell.UpdateDatas();
        }
        UpdateEquippableGrid();
        ToggleSelectedGadget();
    }
    
    void OnCellClick(UICellGadgetEquip cell)
    {
        _currentCellToggled = cell;
        UpdateDatas();
    }

    void ToggleSelectedGadget()
    {
        foreach (UICellGadgetEquip cell in _cellsGadgetEquip)
        {
            cell.Toggle(cell == _currentCellToggled);
        }
    }

    public void UpdateEquippableGrid()
    {
        _equippableGadgetGrid.gameObject.SetActive(false);
        ClearCells();
        foreach (GadgetInstance gadget in GameManager.Instance.PlayerProfile.Inventory.Gadgets.FindAll(x => x.Data.Type == _currentCellToggled.GadgetType))
        {
            SpawnCell(gadget);
        }
        _equippableGadgetGrid.gameObject.SetActive(true);
    }

    void SpawnCell(GadgetInstance gadgetInstance)
    {
        UICellGadgetPrefight cell = Instantiate(_cellPrefab).GetComponent<UICellGadgetPrefight>();
        cell.transform.SetParent(_equippableGadgetGrid.transform);
        cell.transform.localScale = Vector3.one;
        cell.transform.localPosition = new Vector3(-25, -75f - 125f * _cellsGadgetPrefight.Count, 0f);
        cell.Initialize(gadgetInstance);
        cell.OnEquip += OnEquip;
        _cellsGadgetPrefight.Add(cell);

    }

    public void OnEquip(GadgetInstance gadgetInstance)
    {
        Debug.Log("Equip : " + gadgetInstance.Data.Name + " in " + _currentCellToggled.GadgetSlot);
        gadgetInstance.Equip(_currentCellToggled.GadgetSlot, true);
        UpdateDatas();
    }


    void ClearCells()
    {
        foreach (UICellGadgetPrefight cell in _cellsGadgetPrefight)
        {
            Destroy(cell.gameObject);
        }
        _cellsGadgetPrefight.Clear();
    }

    // Triggered by Unity UI
    public void OnBackButton()
    {
        UIManager.Instance.ShowPanel(PanelType.Prefight);
    }
}