﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIOverlay : MonoBehaviour
{
    [SerializeField] GameObject _root;
    [SerializeField] GameObject _overlayRoot;

    [SerializeField] List<GameObject> _ressourcesObjects;

    [SerializeField] Text _textPosition;
    [SerializeField] Text _textSpeed;
    [SerializeField] Text _textHP;
    [SerializeField] Text _textGold;
    [SerializeField] Text _textKillPoints;        
    [SerializeField] Text _textHook;
    [SerializeField] Text _textFuel;

    private bool _isLevelRunning;
    private LevelGenerator _levelGenerator;


    // Use this for initialization
    void Start()
    {
        _levelGenerator = LevelManager.Instance.Generator;

        // Connect to HP/Ressources callbacks
        LevelManager.Instance.OnPlayerSpawned += (player) =>
        {
            player.Ressources.OnRessourceUpdate += OnRessourceUpdate;
            player.OnHpUpdated += OnHpUpdate;
        };

        LevelManager.Instance.OnEncounterStart += () =>
        {
            _overlayRoot.SetActive(true);
            _isLevelRunning = true;

        };
        
        LevelManager.Instance.OnEncounterEnd += () =>
        {
            _isLevelRunning = false;
        };

        LevelManager.Instance.Tracker.OnCurrencyUpdate += OnCurrencyUpdate;
        LevelManager.Instance.Tracker.OnKillUpdate += OnKillUpdate;
    }

    void Update()
    {
        if (!_isLevelRunning)
            return;

        _textPosition.text = string.Format("({0:0},{1:0})", (int) (LevelManager.Instance.Player.transform.position.x/100f),
            (int) (LevelManager.Instance.Player.transform.position.y/100f));
        _textSpeed.text = string.Format("{0:0}", LevelManager.Instance.Player.Controller.Rigidbody2D.velocity.magnitude/10f);
    }

    void OnKillUpdate(float total)
    {
        _textKillPoints.text = total.ToString();
    }

    void OnHpUpdate(float value)
    {
        _textHP.text = value.ToString();
    }

    void OnCurrencyUpdate(Currency currency)
    {
        _textGold.text = currency.Coin.ToString();
    }

    void OnRessourceUpdate(RessourceType type, float value)
    {
        if(type == RessourceType.Fuel)
            _textFuel.SetValue(value);
        else if(type == RessourceType.Hook)
            _textHook.SetValue(value);

    }

    public void Show(bool isVisible)
    {
        _root.SetActive(isVisible);
        _textGold.text = 0.ToString();
        _ressourcesObjects[0].SetActive(GameManager.Instance.PlayerProfile.Inventory.Gadgets.Any(x => x.IsEquipped && x.Data.Ressources.Any(y => y == RessourceType.Fuel)));
        _ressourcesObjects[1].SetActive(GameManager.Instance.PlayerProfile.Inventory.Gadgets.Any(x => x.IsEquipped && x.Data.Ressources.Any(y => y == RessourceType.Hook)));
    }

    // Triggered by UI
    public void OnEndGame_Button()
    {
        LevelManager.Instance.EndEncounter();
    }
  
}