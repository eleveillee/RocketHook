﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UICellGoal : MonoBehaviour
{
   [SerializeField] Text _goalType;
   [SerializeField] Text _goalValue;
   [SerializeField] Text _goalSource;
   [SerializeField] Image _mainQuestBackground;

   [SerializeField] Button _claimButton;

    private Goal _goal;

    public void SetGoal(Goal goal)
    {
        _goal = goal;
        _goalType.text = goal.GetDescriptionText();//_goal.Data.Type.ToString();

        float goalValue = goal.GetValue();
        _goalValue.text = goal.GetValue()  + "/" + goal.Data.Values[0];
        _goalSource.text = goal.Data.Source.ToString();

        if (goal.Data.Source == GoalSource.Main)
            _goalSource.color = Color.green;
        else if(goal.Data.Source == GoalSource.Random)
            _goalSource.color = Color.white;
        _mainQuestBackground.gameObject.SetActive(goal.Data.Source == GoalSource.Main);

        _goalValue.color = goal.IsCompleted ? Color.red : Color.white;
        _claimButton.interactable = goal.IsCompleted;
    }

    public void OnClaim()
    {
        GoalSystem.CompleteGoal(_goal);

        LeanTween.scale(gameObject, UnityEngine.Vector3.one * 1.08f, 0.1f).setEase(LeanTweenType.easeOutCubic);
        LeanTween.scale(gameObject, UnityEngine.Vector3.one, 0.05f).setEase(LeanTweenType.easeInCubic).setDelay(0.1f); //.setOnComplete(animCompleted);

        //LeanTween.rotate(gameObject, transform.rotation.eulerAngles + 10f*Vector3.forward, 0.1f).setEase(LeanTweenType.easeOutCubic).setDelay(numberTweenTime);
        //LeanTween.rotate(gameObject, transform.rotation.eulerAngles, 0.1f).setEase(LeanTweenType.easeInCubic).setDelay(numberTweenTime + 0.1f); //.setOnComplete(animCompleted);
    }

    //public Goal Goal { get { return _goal; } }
}
