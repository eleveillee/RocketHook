using System;
using UnityEngine;
using UnityEngine.UI;

public class UITextTweener : MonoBehaviour
{
	[SerializeField]
	private Text _text;
	public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponent<Text>();
            return _text;
        }
    }			

	[SerializeField]
	private float _numberTweenTime = 0.4f;
	[SerializeField]
	private float _numberScaleTime = 0.2f;
	[SerializeField]
	private float _numberScaleUpRatio = 1.5f;
	[SerializeField]
	private float _numberScaleDownRatio = 0.75f;

	private float _currentValue;
	private string _format;
        
    // Set the value directly without tweening
	public void SetValue(float value, string format = "")
	{
		LeanTween.cancel(Text.gameObject);

        Text.text = value.ToString(format);

		_currentValue = value;
	}
		
    // Tween the value
	public void UpdateValue(float value, bool isReset = false, string format = "", Action animCompleted = null)
	{
        if (isReset)
            _currentValue = 0f;

		if (_currentValue == value)
		{
			if (animCompleted != null)
			{
				animCompleted.Invoke();
			}
			return;
		}

		_format = format;

		float numberTweenTime = GetCurrencyNumberTweenTime(Mathf.Abs(value - _currentValue));
		float halfScaleTime = _numberScaleTime / 2.0f;
		float scaleRatio = _currentValue < value ? _numberScaleUpRatio : _numberScaleDownRatio;

		LeanTween.value(Text.gameObject, UpdateTextValue, _currentValue, value, numberTweenTime).setEase(LeanTweenType.linear);
		LeanTween.scale(Text.gameObject, UnityEngine.Vector3.one * scaleRatio, halfScaleTime).setEase(LeanTweenType.easeOutCubic).setDelay(numberTweenTime);
		LeanTween.scale(Text.gameObject, UnityEngine.Vector3.one, halfScaleTime).setEase(LeanTweenType.easeInCubic).setDelay(numberTweenTime + halfScaleTime).setOnComplete(animCompleted);			

		_currentValue = value;
	}
		
	private float GetCurrencyNumberTweenTime(float value)
	{
		if (value > 100)
		{
			return _numberTweenTime;
		}
			
		return value / 100.0f * _numberTweenTime;
	}
		
	private void UpdateTextValue(float v)
	{
        Text.text = Mathf.RoundToInt(v).ToString(_format);
	}
}