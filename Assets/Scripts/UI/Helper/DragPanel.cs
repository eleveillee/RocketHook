﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler {
	
	private Vector2 originalLocalPointerPosition;
	private Vector3 originalPanelLocalPosition;
	private RectTransform panelRectTransform;
	//private RectTransform parentRectTransform;
	
	void Awake () {
        Debug.Log("Awake");
		panelRectTransform = transform.parent as RectTransform;
		//parentRectTransform = panelRectTransform.parent as RectTransform;
	}
	
	public void OnPointerDown (PointerEventData data) {
        Debug.Log("OnPointerDown");
		originalPanelLocalPosition = panelRectTransform.localPosition;
        originalLocalPointerPosition = data.position;
        //RectTransformUtility.ScreenPointToLocalPointInRectangle (parentRectTransform, data.position, data.pressEventCamera, out originalLocalPointerPosition);
    }
	
	public void OnDrag (PointerEventData data) {
        Debug.Log("OnDrag");

		if (panelRectTransform == null)// || parentRectTransform == null)
			return;

        Vector3 offsetToOriginal = data.position - originalLocalPointerPosition;
        panelRectTransform.SetY(((originalPanelLocalPosition + offsetToOriginal)).y);

        Debug.Log(offsetToOriginal + " = " + panelRectTransform.localPosition + " - " + (Vector3)originalLocalPointerPosition);
        Debug.Log("OnDrag2");
        /*Vector2 localPointerPosition;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (parentRectTransform, data.position, data.pressEventCamera, out localPointerPosition)) {
			Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;
			panelRectTransform.localPosition = originalPanelLocalPosition + offsetToOriginal;
		}*/
		
		//ClampToWindow ();
	}
	
	// Clamp panel to area of parent
	/*void ClampToWindow () {
		Vector3 pos = panelRectTransform.localPosition;
		
		Vector3 minPosition = parentRectTransform.rect.min - panelRectTransform.rect.min;
		Vector3 maxPosition = parentRectTransform.rect.max - panelRectTransform.rect.max;
		
		pos.x = Mathf.Clamp (panelRectTransform.localPosition.x, minPosition.x, maxPosition.x);
		pos.y = Mathf.Clamp (panelRectTransform.localPosition.y, minPosition.y, maxPosition.y);
		
		panelRectTransform.localPosition = pos;
	}*/
}
