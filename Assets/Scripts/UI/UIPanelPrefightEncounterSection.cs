﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIPanelPrefightEncounterSection : MonoBehaviour
{
    [SerializeField] Text _encounterTypeText;
    [SerializeField] UICellEncounterPrefight _cellPrefab;
    [SerializeField] GameObject _cellsRoot;
    List<UICellEncounterPrefight> _cells = new List<UICellEncounterPrefight>();

    int _currentEncounterTypeIndex;

    /*
    void Start()
    {
        UpdateDatas();
    }
    */

    // Called from Unity UI
    public void ChangeEncounterType(int offset)
    {
        _currentEncounterTypeIndex += offset;

        if (_currentEncounterTypeIndex < 0)
            _currentEncounterTypeIndex = Enum.GetValues(typeof(EncounterType)).Length - 1;
        else if(_currentEncounterTypeIndex >= Enum.GetValues(typeof(EncounterType)).Length)
            _currentEncounterTypeIndex = 0;

        DataEncounter _newDataEncounter = GameManager.Instance.GameDefinition.GetEncounters((EncounterType)_currentEncounterTypeIndex)[0];
        
        if (GameManager.Instance.PlayerProfile.EncounterStates.Find(x => x.EncounterId == _newDataEncounter.Id) != null)
            GameManager.Instance.PlayerProfile.CurrentDataEncounter = _newDataEncounter;

        UpdateDatas();
    }

    public void OnOpen()
    {
        _currentEncounterTypeIndex = (int)GameManager.Instance.PlayerProfile.CurrentDataEncounter.EncounterType;
        UpdateDatas();
    }

    public void UpdateDatas()
    {
        _cellsRoot.gameObject.SetActive(false);
        _encounterTypeText.text = ((EncounterType)_currentEncounterTypeIndex).ToString();
        ClearCells();
        foreach (DataEncounter dataEncounter in GameManager.Instance.GameDefinition.GetEncounters((EncounterType)_currentEncounterTypeIndex))
        {
            SpawnCell(dataEncounter);
        }
        ToggleCells();
        _cellsRoot.gameObject.SetActive(true);
    }

    void SpawnCell(DataEncounter dataEncounter)
    {
        UICellEncounterPrefight cell = Instantiate(_cellPrefab).GetComponent<UICellEncounterPrefight>();
        cell.transform.SetParent(_cellsRoot.transform);
        cell.transform.localScale = Vector3.one;
        cell.transform.localPosition = new Vector3(-25 ,-75f - 125f * _cells.Count, 0f); // z = 1 should probably handled by the prefabfor simplicity
        cell.Initialize(dataEncounter);
        cell.OnClick += OnEncounterClick;
        cell.ToggleCurrentEncounter(cell.DataEncounter.Id == GameManager.Instance.PlayerProfile.CurrentDataEncounter.Id);
        _cells.Add(cell);

    }

    void ClearCells()
    {
        foreach (UICellEncounterPrefight cell in _cells)
        {
            Destroy(cell.gameObject);
        }
        _cells.Clear();
    }

    void OnEncounterClick(DataEncounter dataEncounter)
    {
        GameManager.Instance.PlayerProfile.CurrentDataEncounter = dataEncounter;
        ToggleCells();
    }

    void ToggleCells()
    {
        DataEncounter currentDataEncounter = GameManager.Instance.PlayerProfile.CurrentDataEncounter;
        foreach (UICellEncounterPrefight cell in _cells)
        {
            cell.ToggleCurrentEncounter(cell.DataEncounter.Id == currentDataEncounter.Id);
        }
    }
}