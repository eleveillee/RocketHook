﻿using System;
using System.Reflection;

using UnityEngine;
using UnityEngine.UI;

public class UICellGadgetPrefight : MonoBehaviour
{
    [SerializeField] Text _gadgetName;
    [SerializeField] Image _buttonBackground;

    public Action<GadgetInstance> OnEquip;
    private GadgetInstance _gadgetInstance;
    public void Initialize(GadgetInstance gadget) 
    {
        _gadgetInstance = gadget;
        UpdateCell();
    }

    public void OnClick()
    {
        if (!_gadgetInstance.IsEquippable)
            return;

        //_gadgetInstance.Equip();
        if (OnEquip != null)
            OnEquip(_gadgetInstance);
        UpdateCell();
    }

    void UpdateCell()
    {
        _gadgetName.text = _gadgetInstance.Data.Name;
        _buttonBackground.color = _gadgetInstance.IsEquipped ? Color.red : Color.white;
    }

    public GadgetInstance GadgetInstance
    {
        get { return _gadgetInstance; }
    }
}