﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [Header("UI Panels and overlays")]
    [SerializeField] UIOverlay _uiOverlay;
    [SerializeField] UISidebar _uiSidebar;
    [SerializeField] UIPanelEndGame _uiPanelEndGame;
    [SerializeField] UIPanelChar _uiPanelChar;
    [SerializeField] UIPanelGadget _uiPanelGadget;
    [SerializeField] UIPanelShop _uiPanelShop;
    [SerializeField] UIPanelPrefight _uiPanelPrefight;
    [SerializeField] UIPanelOptions _uiPanelOptions;
    [SerializeField] UIPanelEquipGear _uiPanelEquipGear;
    [SerializeField] UIPanelLoadingScreen _uiPanelLoadingScreen;
    [SerializeField] UIDialog _uiDialog;
    [SerializeField] UIAudio _uiAudio;   

    Dictionary<PanelType, UIPanel> _panels = new Dictionary<PanelType, UIPanel>();
    PanelType _currentMenuType;
    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        _uiDialog.gameObject.SetActive(false);

        _panels.Add(PanelType.EndGame, _uiPanelEndGame);
        _panels.Add(PanelType.Char, _uiPanelChar);
        _panels.Add(PanelType.Gadget, _uiPanelGadget);
        _panels.Add(PanelType.Shop, _uiPanelShop);
        _panels.Add(PanelType.Prefight, _uiPanelPrefight);
        _panels.Add(PanelType.Options, _uiPanelOptions);
        _panels.Add(PanelType.EquipGear, _uiPanelEquipGear);
        _panels.Add(PanelType.LoadingScreen, _uiPanelLoadingScreen);
        //_panels.Add(PanelType.Audio, _uiAudio);
    }

    // Panels
    public void CloseAllPanels()
    {
        foreach(KeyValuePair<PanelType, UIPanel> pair in _panels)
        {
            pair.Value.Close();
        }
    }

    public void ToggleNotifiers()
    {
        _uiSidebar.ToggleNotifiers();
    }

    public void ShowPanel(PanelType type,bool isVisible = true)
    {
        CloseAllPanels();
        if (isVisible)
        {
            _panels[type].Open();
            _currentMenuType = type;
        }
            
        else
        {
            _panels[type].Close();
            _currentMenuType = PanelType.Null;
        }
    }

    public void ShowEndGame(bool isVisible)
    {
        ShowPanel(PanelType.EndGame, isVisible);
    }
    
    public void ShowChar(bool isVisible)
    {
        ShowPanel(PanelType.Char, isVisible);
    }

    public void ShowGadgets(bool isVisible)
    {
        ShowPanel(PanelType.Gadget, isVisible);
    }

    public void ShowShop(bool isVisible)
    {
        ShowPanel(PanelType.Shop, isVisible);
    }

    public void ShowEquipGear(bool isVisible)
    {
        ShowPanel(PanelType.EquipGear, isVisible);
    }

    public void ToggleUIAudio()
    {
        if (!_uiAudio.isActiveAndEnabled)
            _uiAudio.gameObject.SetActive(true);
        else
            _uiAudio.gameObject.SetActive(false);
    }

    // Overlays
    public void ShowUIOVerlay(bool isVisible)
    {
        _uiOverlay.Show(isVisible);
    }
    
    public void ShowMenuSidebar(bool isVisible)
    {
        _uiSidebar.Show(isVisible);
    }

    public void ReloadMenu()
    {
        ShowPanel(_currentMenuType);
    }

    // UIDialog
    public UIDialog ShowDialog(bool isVisible = true)
    {
        _uiDialog.gameObject.SetActive(isVisible);
        return _uiDialog;
    }
}

public enum PanelType
{
    Null,
    EndGame,
    Shop,
    Char,
    Gadget,
    Prefight,
    Options,
    EquipGear,
    LoadingScreen,
    Audio
}