﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Lean;

public class UISectionGoals : MonoBehaviour
{
    [SerializeField] GameObject _uiCellGoal;
    [SerializeField] GameObject _rootGoals;
    
    private List<UICellGoal> _goalCells = new List<UICellGoal>();

    void Start()
    {
        GoalSystem.OnNewGoal += OnNewGoal;
    }

    void OnNewGoal()
    {
        UpdateDatas();
    }

    public void UpdateDatas()
    {
        for(int i = 0; i < GameManager.Instance.PlayerProfile.Goals.Count; i++)
        {
            if (_goalCells.Count < GameManager.Instance.PlayerProfile.Goals.Count)
            {
                GameObject go = GameObject.Instantiate(_uiCellGoal);
                go.transform.SetParent(_rootGoals.transform, false);
                go.transform.localPosition = new Vector3(0f, _goalCells.Count * -95f, 0.0f);
                _goalCells.Add(go.GetComponent<UICellGoal>());
            }

            _goalCells[i].SetGoal(GameManager.Instance.PlayerProfile.Goals[i]);
        }
    }
}