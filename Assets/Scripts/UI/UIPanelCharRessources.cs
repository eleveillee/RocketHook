﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class UIPanelCharRessources : MonoBehaviour
{
    [SerializeField]
    private UICellRessourceUpgrade _uiCellRessourceUpgradePrefab;

    [SerializeField]
    private GameObject _ressourcesRoot;
    
    private List<UICellRessourceUpgrade> _cellRessources = new List<UICellRessourceUpgrade>();

    public void UpdateRessources()
    {
        ClearCellsRessource();

        foreach (RessourceLevel level in GameManager.Instance.PlayerProfile.Stats.RessourcesLevel)
        {
            UICellRessourceUpgrade cell = SpawnCell(level);
            _cellRessources.Add(cell);
        }
    }

    UICellRessourceUpgrade SpawnCell(RessourceLevel level)
    {
        UICellRessourceUpgrade uiCell = Instantiate(_uiCellRessourceUpgradePrefab).GetComponent<UICellRessourceUpgrade>();
        uiCell.Initialize(level, OnRessourceBought);
        uiCell.transform.SetParent(_ressourcesRoot.transform, false);
        uiCell.transform.localPosition = new Vector3(0, _cellRessources.Count * -150f, 0);
        return uiCell;
    }

    void ClearCellsRessource()
    {
        foreach (UICellRessourceUpgrade cell in _cellRessources)
        {
            Destroy(cell.gameObject);
        }
        _cellRessources.Clear();
    }

    void OnRessourceBought(RessourceLevel ressourceLevel)
    {
        //GameManager.Instance.PlayerProfile.Inventory.Currency.AddCoin(-1);
        ressourceLevel.Level++;
        SaveSystem.Save();
    }
}
