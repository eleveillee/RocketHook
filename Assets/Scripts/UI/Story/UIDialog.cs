﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIDialog : MonoBehaviour
{
   [SerializeField] Image _avatarImage;
   [SerializeField] Text _nameText;
   [SerializeField] Text _dialogText;


    /*public void Initialize(string text)
    {
        _dialogText.text = text;
    }*/

    internal void UpdateText(StringBuilder _liveText)
    {
        _dialogText.text = _liveText.ToString();
    }
}
