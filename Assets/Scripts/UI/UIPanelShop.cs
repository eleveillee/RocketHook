﻿using UnityEngine;
using System.Collections.Generic;

using UnityEngine.UI;
using System.Linq;

public class UIPanelShop : UIPanel {

    [SerializeField]
    private UICellGadgetShop _cellGadgetShopPrefab;
    
    [SerializeField]
    GameObject _CellsGadgetShopRoot;

    List<UICellGadgetShop> _cellGadgetShops = new List<UICellGadgetShop>();

    private int _currentGadgetIndex;

    protected override void OnOpen()
    {
        UpdateDatas();
    }

    public void UpdateDatas()
    {
        ClearCellsShop();
        foreach (DataGadget dataGadget in GameManager.Instance.GameDefinition.DataShop.GetLockedGadgets())
        {
            SpawnCell(dataGadget);
        }
    }

    //void SpawnCell(GadgetInstance gadgetInstance, DataGadgetUpgrade dataGadgetUpgrade)
    void SpawnCell(DataGadget dataGadget)
    {
        UICellGadgetShop cellGadgetShop = Instantiate(_cellGadgetShopPrefab).GetComponent<UICellGadgetShop>();
        cellGadgetShop.transform.SetParent(_CellsGadgetShopRoot.transform);
        cellGadgetShop.transform.localScale = Vector3.one;
        cellGadgetShop.transform.localPosition = new Vector3(0, -125f * _cellGadgetShops.Count);
        cellGadgetShop.Initialize(dataGadget);
        cellGadgetShop.OnBuy += OnGadgetBuy;
        _cellGadgetShops.Add(cellGadgetShop);
        
    }

    void OnGadgetBuy(UICellGadgetShop uiCellGadgetShop)
    {
        Destroy(_cellGadgetShops.Find(x => x == uiCellGadgetShop).gameObject); // Destroy the cell
        _cellGadgetShops.Remove(uiCellGadgetShop); // Remove the cell from the list
    }

    void ClearCellsShop()
    {
        foreach (UICellGadgetShop cell in _cellGadgetShops)
        {
            Destroy(cell.gameObject);
        }
        _cellGadgetShops.Clear();
    }

}
