﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Lean;

public class UIPanelEndGame : UIPanel
{
    private EndGameState _endGameState;
    private bool _isWaitingOnClick;

    [SerializeField] UISectionGoals _uiSectionGoals;

    [SerializeField] UITextTweener _textHeight;
    [SerializeField] UITextTweener _textHeightBest;

    [SerializeField] UITextTweener _textSpeed;
    [SerializeField] UITextTweener _textSpeedBest;

    [SerializeField] UITextTweener _textGold;
    [SerializeField] UITextTweener _textGoldBest;

    [SerializeField] UITextTweener _textTime;
    [SerializeField] UITextTweener _textTimeBest;

    [SerializeField] UITextTweener _textScore;
    [SerializeField] UITextTweener _textScoreBest;
    
    void Awake()
    {
        Close();
        LeanTouch.OnFingerTap += OnFingerTap;
    }

    public void OnFingerTap(LeanFinger finger)
    {
        if (_isWaitingOnClick)
            NextState();
    }

    protected override void OnOpen()
    {
        _isWaitingOnClick = true;
        _endGameState = EndGameState.Display;
        UpdateDatas();
    }

    public void NextState()
    {
        if(_endGameState == EndGameState.Display)
        {
            _endGameState = EndGameState.Update;
        }
    }
    public void OnMenuClick()
    {
        StateManager.Instance.ChangeState(GameState.Menu);
        Close();
    }

    public void OnReplayClick()
    {
        StateManager.Instance.ChangeState(GameState.Play);
        Close();
    }

    void UpdateDatas()
    {
        Debug.Log("PanelUpdateDataS");
        _uiSectionGoals.UpdateDatas();
        TrackingData data = LevelManager.Instance.Tracker.Data;
        TrackingData persistentData = GameManager.Instance.PlayerProfile.PersistentTrackingData;

        if (data == null || persistentData == null)
        {
            Close();
            UIManager.Instance.ShowPanel(PanelType.Prefight);
            return;
        }
            

        _textHeight.UpdateValue(data.MaxHeight);
        _textHeightBest.UpdateValue(persistentData.MaxHeight, true);
        ColorNewBest(_textHeightBest, data.MaxHeight, persistentData.MaxHeight);

        _textSpeed.UpdateValue(data.MaxSpeed);
        _textSpeedBest.UpdateValue(persistentData.MaxSpeed, true);
        ColorNewBest(_textSpeedBest, data.MaxSpeed, persistentData.MaxSpeed);

        _textGold.UpdateValue(data.CurGold);
        _textGoldBest.UpdateValue(persistentData.CurGold, true);
        ColorNewBest(_textGoldBest, data.CurGold, persistentData.CurGold);

        _textTime.UpdateValue(data.TimeLength);
        _textTimeBest.UpdateValue(persistentData.TimeLength, true);
        ColorNewBest(_textTimeBest, data.TimeLength, persistentData.TimeLength);

        _textScore.UpdateValue(data.Score);
        _textScoreBest.UpdateValue(persistentData.Score, true);
        ColorNewBest(_textScoreBest, data.Score, persistentData.Score);
    }

    void ColorNewBest(UITextTweener tweener, float newValue, float maxValue)
    {
        if (Mathf.Abs(newValue - maxValue) < 0.01f && maxValue > 0.01f)
            tweener.Text.color = Color.red;
        else
            tweener.Text.color = Color.white;
    }
}

public enum EndGameState
{
    Display,
    Update
}