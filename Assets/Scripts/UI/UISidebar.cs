﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UISidebar : MonoBehaviour
{
    [SerializeField]
    GameObject _root;

    [SerializeField] UITextTweener _textTweenerCoin;

    [SerializeField] Notifier _charNotifier;
    [SerializeField] Notifier _gadgetNotifier;
    [SerializeField] Notifier _shopNotifier;
    [SerializeField] Notifier _playNotifier;

    private bool _isVisible;

    void Awake()
    {
        Show(_isVisible);
        GameManager.Instance.OnNewPlayerProfile += OnNewProfile;
    }

    void Start()
    {
        // Mock update call, system should be more robust and update itself accordingly
        OnProfileUpdated(GameManager.Instance.PlayerProfile);
        ToggleNotifiers();
    }

    void OnNewProfile(PlayerProfile playerProfile)
    {
        playerProfile.OnUpdate += OnProfileUpdated;
        OnProfileUpdated(playerProfile);
    }

    void OnProfileUpdated(PlayerProfile playerProfile)
    {
        //Debug.Log("Update : " + _textTweenerCoin.Text.text + " to " + playerProfile.Inventory.Currency.Coin);
        if (!_isVisible)
            return;

        ToggleNotifiers();
        _textTweenerCoin.UpdateValue(playerProfile.Inventory.Currency.Coin);
    }

    public void Show(bool isVisible)
    {
        _isVisible = isVisible;
        _root.SetActive(isVisible);
    }

    public void ToggleNotifiers()
    {
        _gadgetNotifier.Toggle(GameManager.Instance.PlayerProfile.IsGadgetUpgradable);
        _shopNotifier.Toggle(GameManager.Instance.PlayerProfile.IsGadgetBuyable);
        _charNotifier.Toggle(GameManager.Instance.PlayerProfile.IsCharUpgradable);
    }
    
    public void OnPlay()
    {
        UIManager.Instance.ShowPanel(PanelType.Prefight);
    }

    public void OnOptions()
    {
        UIManager.Instance.ShowPanel(PanelType.Options);
    }


}