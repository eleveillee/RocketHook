﻿using UnityEngine;
using System.Collections;

public class UIPanelChar : UIPanel
{
    [SerializeField]
    UIPanelCharRessources uiPanelCharRessources;

    protected override void OnOpen()
    {
        uiPanelCharRessources.UpdateRessources();
    }
}
