using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public class StoryInstance : MonoBehaviour
{
    [SerializeField]
    protected string _id;

    public string Name;
    public int Level;

    public List<GameObject> StepHolders = new List<GameObject>();
    public int CurrentStep;
    bool _isCompleted = false;

    public string Id
    {
        get { return _id; }
    }
    
    StoryInstance()
    {
        if (string.IsNullOrEmpty(_id))
        {
            _id = Guid.NewGuid().ToString();
            Debug.Log("Id : " + _id);
        }
        else
            Debug.Log("Null :  " + _id);
    }

    void Awake()
    {
        foreach (Transform t in transform)
        {
            StepHolders.Add(t.gameObject);
            t.gameObject.SetActive(false);
        }
    }

    public void Activate()
    {
        ActivateStep(0);
    }

    void ActivateStep(int stepIndex)
    {
        if (_isCompleted)
            return;

        CurrentStep = stepIndex;
        StepHolders[CurrentStep].SetActive(true);
        StepHolders[CurrentStep].GetComponent<StepCompleteCondition>().OnComplete += OnStepCompleted;
        foreach (StoryStep storyStep in StepHolders[CurrentStep].GetComponents<StoryStep>())
        {
            storyStep.Activate();
        }
    }
    
    void OnStepCompleted()
    {
        if (_isCompleted || StepHolders[CurrentStep] == null)
            return;

        // Deactivate all StorySteps
        foreach (StoryStep storyStep in StepHolders[CurrentStep].GetComponents<StoryStep>())
        {
            storyStep.OnDeactivate();
        }
        StepHolders[CurrentStep].SetActive(false);
        StepHolders[CurrentStep].GetComponent<StepCompleteCondition>().OnComplete -= OnStepCompleted;

        // Complete the story instance if it was the last step
        if (CurrentStep >= StepHolders.Count - 1)
        {
            Invoke("CompleteEncounter", 0.5f);
            return;
        }
        ActivateStep(CurrentStep + 1);
    }

    void CompleteEncounter()
    {
        LevelManager.Instance.EndEncounter();
        StoryManager.Instance.CompleteStoryInstance(this);
    }
}
