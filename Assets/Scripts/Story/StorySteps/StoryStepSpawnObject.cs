using UnityEngine;
using System.Collections.Generic;
using System;

public class StoryStepSpawnObject : StoryStep
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private SpawnTarget _spawnTarget;

    public override void Activate()
    {
        if (LevelManager.Instance.Player == null)
            return;

        GameObject go = Instantiate(_prefab) as GameObject;
        if (_spawnTarget == SpawnTarget.PlayerPosition)
        {
            go.transform.position = LevelManager.Instance.Player.transform.position;
        }
        else if (_spawnTarget == SpawnTarget.ChildToPlayer)
        {
            go.transform.SetParent(LevelManager.Instance.Player.transform, false);
            go.transform.localPosition = Vector3.zero;
        }
            
    }
    
}

public enum SpawnTarget
{
    PlayerPosition,
    ChildToPlayer
}
