using UnityEngine;
using System.Collections;
using System.Text;

public class StoryStepDialog : StoryStep
{
    public string Text = "Missing Text";
    private StringBuilder _liveText;
    int _currentIndex = -1;
    private UIDialog _uiDialog;

    public override void Activate()
    {
        Debug.Log("Activate : " + _currentIndex);
        _liveText = new StringBuilder();
        _uiDialog = UIManager.Instance.ShowDialog();
        StartCoroutine(WriteText());
    }

    IEnumerator WriteText()
    {
        while(_currentIndex < Text.Length - 1)
        {
            _liveText.Append(Text[++_currentIndex]);
            _uiDialog.UpdateText(_liveText);
            yield return new WaitForEndOfFrame();
            //yield return new WaitForSeconds(0.005f);
        }
    }
}
