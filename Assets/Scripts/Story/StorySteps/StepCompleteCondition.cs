using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Lean;
public class StepCompleteCondition : MonoBehaviour
{
    [SerializeField]
    EnumOperator _operator;

    public float Delay;

    [SerializeField]
    List<CompleteCondition> _completeConditions = new List<CompleteCondition>();
    List<CompleteCondition> _timedConditions = new List<CompleteCondition>();

    float _timeStart;

    public Action OnComplete;
    void OnEnable()
    {
        Invoke("Initialize", 1.0f);
    }

    void OnDisable()
    {
        LeanTouch.OnFingerTap -= OnFingerTap;
        LeanTouch.OnFingerHeldDown -= OnFingerHeldDown;
    }

    void Initialize()
    {
        if (GetConditions(EnumCompleteConditionType.Tap).Count > 0)
            LeanTouch.OnFingerTap += OnFingerTap;

        if (GetConditions(EnumCompleteConditionType.Hold).Count > 0)
            LeanTouch.OnFingerHeldDown += OnFingerHeldDown;

        if (GetConditions(EnumCompleteConditionType.EncounterStart).Count > 0)
            LevelManager.Instance.OnEncounterStart += OnEncounterStart;

        _timedConditions = GetConditions(EnumCompleteConditionType.Time);
        _timeStart = Time.time;
    }

    void Update()
    {
        foreach(CompleteCondition timedCondition in _timedConditions)
        {
            if (Time.time - _timeStart > float.Parse(timedCondition.Values[0])) // Test for duration
                timedCondition.IsComplete = true;
        }

        if(AreConditionsCompleted())
        {
            if(OnComplete != null)
                LeanTween.delayedCall(Delay, OnComplete);
        }
    }

    bool AreConditionsCompleted()
    {
        bool isCompleted = _operator == EnumOperator.And;
        foreach (CompleteCondition condition in _completeConditions)
        {
            if(_operator == EnumOperator.And)
                isCompleted &= condition.IsComplete;
            else
                isCompleted |= condition.IsComplete;
        }

        return isCompleted;
    }

    void OnFingerHeldDown(LeanFinger fingers)
    {
        CompleteConditionsOfType(EnumCompleteConditionType.Hold);
    }

    void OnFingerTap(LeanFinger fingers)
    {
        CompleteConditionsOfType(EnumCompleteConditionType.Tap);
    }

    void OnEncounterStart()
    {
        foreach (CompleteCondition condition in _completeConditions)
        {
            if (LevelManager.Instance.CurrentEncounter.Id ==  condition.Values[0])
                condition.IsComplete = true;
        }
    }

    void CompleteConditionsOfType(EnumCompleteConditionType type)
    {
        foreach(CompleteCondition condition in _completeConditions.FindAll(x => x.Type == type))
        {
            condition.IsComplete = true;
        }
    }

    List<CompleteCondition> GetConditions(EnumCompleteConditionType type)
    {
        return _completeConditions.FindAll(x => x.Type == type);
    }
    
    public List<CompleteCondition> CompleteConditions
    {
        get { return _completeConditions; }
    }

    public EnumOperator Operator
    {
        get { return _operator; }
    }
}
[Serializable]
public class CompleteCondition
{
    public EnumCompleteConditionType Type;
    public List<string> Values = new List<string>();

    private bool _isComplete;
    public bool IsComplete
    {
        get { return (_isComplete || Type == EnumCompleteConditionType.True) && Type != EnumCompleteConditionType.False; }
        set { _isComplete = value;  }
    }
}

public enum EnumCompleteConditionType
{
    Custom,
    Time,
    Tap,
    Hold,
    EncounterStart,
    True,
    False
}
