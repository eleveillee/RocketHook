using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class StoryStep : MonoBehaviour
{
    public abstract void Activate();
    public virtual void OnDeactivate() { }
}
