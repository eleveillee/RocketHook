using UnityEngine;
using System.Collections.Generic;
using System;

public class StoryStepHideScreen : StoryStep
{
    [SerializeField]
    public float _duration;
    
    [SerializeField]
    public float _opacity = 0.5f;

    public override void Activate()
    {
        HideScreen.Instance.ChangeOpacity(_opacity);
        HideScreen.Instance.gameObject.SetActive(true);

        if (_duration > 0f)
            Invoke("Hide", _duration);
    }

    void Hide()
    {
        HideScreen.Instance.gameObject.SetActive(false);
    }

    public override void OnDeactivate()
    {
        Hide();
    }
}