using UnityEngine;
using System.Collections.Generic;
using System;

public class StoryStepBlockInputs : StoryStep
{
    [SerializeField]
    private float _duration;

    public override void Activate()
    {
        if (_duration < 0f)
            return;
        
        LevelManager.Instance.Player.Controller.ActivateInputs(false);

        if (_duration > 0f)
            Invoke("ReactivateControls", _duration);
    }

    public override void OnDeactivate()
    {
        ReactivateControls();
    }

    void ReactivateControls()
    {
        LevelManager.Instance.Player.Controller.ActivateInputs(true);
    }
}