using UnityEngine;
using System.Collections.Generic;
using System;

public class StoryManager : MonoBehaviour
{
    public static StoryManager Instance;
    StoryInstance _currentStoryInstance;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    public void Activate(StoryInstance storyInstancePrefab)
    {
        _currentStoryInstance = (StoryInstance)Instantiate(storyInstancePrefab, transform);
        _currentStoryInstance.gameObject.SetActive(true);
        _currentStoryInstance.Activate();
    }

    public void CompleteStoryInstance(StoryInstance storyInstance)
    {
        GameManager.Instance.PlayerProfile.CompletedTutorials.Add(storyInstance.Id);
        if(storyInstance.Id == Constants.STORY_TUTO1)
        {
            // Add first Story Encounter
            GameManager.Instance.PlayerProfile.EncounterStates.Add(new EncounterState(Constants.ENCOUNTER_STORY1, 0));
            GameManager.Instance.PlayerProfile.CurrentDataEncounter = GameManager.Instance.GameDefinition.Encounters.Find(x => x.Id == Constants.ENCOUNTER_STORY1);
            
            // Clear All Gadgets
            GameManager.Instance.PlayerProfile.Inventory.Gadgets.Clear();

            // Add Roll Gadgets
            GameManager.Instance.PlayerProfile.Inventory.AddGadget(Constants.GADGET_ROLL, GadgetSlot.Controller1);
        }
    }

    public StoryInstance CurrentStoryInstance
    {
        get { return _currentStoryInstance; }
    }

    internal void StopCurrentStory()
    {
        if (_currentStoryInstance == null)
            return;

        Destroy(_currentStoryInstance.gameObject);
        _currentStoryInstance = null;
    }
}