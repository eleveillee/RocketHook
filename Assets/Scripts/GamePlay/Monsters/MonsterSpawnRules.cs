﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpawnRules
{
    [SerializeField]
    private DataSpawnable _spawnable;
    public DataSpawnable Spawnable
    {
        get { return _spawnable; }
    }
        
    [SerializeField]
    private int _heightIndex; //1 index == 500 units
    public int HeightIndex
    {
        get { return _heightIndex; }
    }
}
