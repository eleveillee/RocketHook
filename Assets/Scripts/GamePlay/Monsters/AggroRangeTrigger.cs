﻿using UnityEngine;
using System.Collections;
using System;

public class AggroRangeTrigger : MonoBehaviour
{
    public bool OnRangeTrigger = false;

    [SerializeField]
    public CircleCollider2D _trigger;

    public void Initialize(float range)
    {
        _trigger.isTrigger = true;
        _trigger.radius = range;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.GetComponentInParent<Player>())
            return;
        
        OnRangeTrigger = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!other.GetComponentInParent<Player>())
            return;

        OnRangeTrigger = false;
    }

}
