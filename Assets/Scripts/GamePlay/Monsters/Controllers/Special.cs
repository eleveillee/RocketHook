﻿using UnityEngine;
using System.Collections;
using System;

public class Special : BaseMonsterController
{
    public override BehaviourType Behaviour { get { return BehaviourType.Special; } }

    public Special(Monster monster) : base(monster)
    {
        Debug.Log("Special");
    }
}
