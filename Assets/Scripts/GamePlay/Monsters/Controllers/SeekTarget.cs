﻿using UnityEngine;
using System.Collections;

public class SeekTarget : BaseMonsterController
{
    public override BehaviourType Behaviour { get { return BehaviourType.SeekTarget; } }

    private AggroRangeTrigger _rangeTrigger;

    public SeekTarget(Monster monster) : base(monster)
    {
        GameObject go = Object.Instantiate(monster.TargetFinderPrefab, monster.transform, false) as GameObject;
                                    
        _rangeTrigger = go.GetComponent<AggroRangeTrigger>();
        _rangeTrigger.Initialize(monster.Data.AggroRange);
    }

    public override void FixedUpdate()
    {
        if (!_rangeTrigger.OnRangeTrigger)
            return;
        
        if ((LevelManager.Instance.Player.Controller.transform.position.x - _monster.transform.position.x) < 0)
            _monster.MonsterView.SpriteRenderer.flipX = true;
        else
            _monster.MonsterView.SpriteRenderer.flipX = false;

        Vector3 vectorToTarget = LevelManager.Instance.Player.Controller.transform.position - _monster.transform.position;
        vectorToTarget = _monster.MonsterView.SpriteRenderer.flipX ? -vectorToTarget : vectorToTarget;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

        _monster.transform.rotation = Quaternion.Slerp(_monster.transform.rotation, q, Time.deltaTime * 5f);
        _monster.transform.position = Vector3.MoveTowards(_monster.transform.position, LevelManager.Instance.Player.Controller.transform.position, _monster.Data.Speed);
    }
}
