﻿using UnityEngine;
using System.Collections;
using System;

using Random = UnityEngine.Random;
public class Patrol : BaseMonsterController
{
    public override BehaviourType Behaviour { get { return BehaviourType.Patrol; } }

    Vector3 _targetPos;
    public override void FixedUpdate()
    {
        _monster.transform.position = Vector3.MoveTowards(_monster.transform.position, _targetPos, _monster.Data.Speed);
    }

    public Patrol(Monster monster) : base(monster)
    {
        _targetPos = (_monster.transform.position + Vector3.one * Random.Range(-500f, 500f));
    }
}
