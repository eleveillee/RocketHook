﻿using UnityEngine;
using System.Collections;

public class ImpactHandler : MonoBehaviour
{
    private Monster _monster;

    void Start()
    {
        _monster = GetComponentInParent<Monster>();
    } 
        
    void OnTriggerEnter2D(Collider2D other)
    {        
        CheckForValidImpact(other);
    }

    void KnockbackEffect(Vector2 targetPos)
    {
        _monster.transform.position = Vector2.MoveTowards(transform.position, targetPos, _monster.Data.Speed * 50f);
    }

    public void CheckForValidImpact(Collider2D other)
    {
        if (other.GetComponentInParent<Player>())
        {
            if (LevelManager.Instance.Player.Controller.Rigidbody2D.velocity.magnitude < LevelManager.Instance.Player.Data.CriticalSpeed)
            {
                other.gameObject.layer = 9;

                _monster.AddHp(-LevelManager.Instance.Player.Data.BaseDamage);

                if (_monster == null)
                    return;

                LevelManager.Instance.Player.AddHp(-_monster.Data.Damage);

                if (LevelManager.Instance.Player == null)
                    return;

                Vector2 targetPos = (transform.position - LevelManager.Instance.Player.Controller.transform.position) + transform.position;
                KnockbackEffect(targetPos);

                LevelManager.Instance.Player.Controller.Rigidbody2D.AddForce(
                    (LevelManager.Instance.Player.Controller.transform.position - transform.position) * _monster.Data.PushForce, ForceMode2D.Impulse);
            }
            else
            {
                LevelManager.Instance.Player.View.SlashEffect();
                LevelManager.Instance.Audio.PlaySlashAudio();
                _monster.Die();
                
            }
        }
    }
}
