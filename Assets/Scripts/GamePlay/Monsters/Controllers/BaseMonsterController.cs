﻿using UnityEngine;
using System.Collections;

public abstract class BaseMonsterController
{
    public abstract BehaviourType Behaviour { get; }
    protected Monster _monster;
    
    
    public BaseMonsterController(Monster monster)
    {
        _monster = monster;
    }
        
    public virtual void FixedUpdate() { }
    
}
