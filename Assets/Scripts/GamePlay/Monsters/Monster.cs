using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Monster : MonoBehaviour, IDamageable, ISpawnable
{
    private BaseMonsterController _controller;

    private MonsterView _monsterView;
    public MonsterView MonsterView
    {
        get { return _monsterView; }
        set { _monsterView = value; }
    }

    private DataMonster _data;
    public DataMonster Data
    {
        get { return _data; }
        set { _data = value; }
    }

    [SerializeField]
    private GameObject _targetFinderPrefab;
    public GameObject TargetFinderPrefab
    {
        get { return _targetFinderPrefab; }
    }

    private float _curHp;
    public float CurHp
    {
        get { return _curHp; }
    }

    public Action<float> OnDamageTaken;
    
    public void Initialize(DataSpawnable data, DataLevelParameters dataLevelParamaters)
    {
        _data = (DataMonster)data;                
        _controller = GetController(_data.Behaviour);
        SetHp(_data.MaxHp);
        _monsterView = GetComponentInChildren<MonsterView>();
        _monsterView.InitializeView(_data);

    }


    void FixedUpdate()
    {
        if (_controller != null)
            _controller.FixedUpdate();
    }

    public bool IsPositionGood()
    {
        int colliderNumber = _data.Behaviour != BehaviourType.SeekTarget ? 1 : 2;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, _data.ColliderSize.x * 3f);
        return colliders.Length <= colliderNumber;
    }

    public BaseMonsterController GetController(BehaviourType behaviour)
    {
        switch (_data.Behaviour)
        {
            case BehaviourType.Fixed:
                return null;
            case BehaviourType.Patrol:
                return new Patrol(this);
            case BehaviourType.SeekTarget:
                return new SeekTarget(this);
            case BehaviourType.Special:
                return new Special(this);
            default:
                return null;
        }
    }

    public void AddHp(float value)
    {
        SetHp(_curHp + value);
        if (value < 0)
            OnDamageTaken(value);
    }

    public void SetHp(float value)
    {
        _curHp = Mathf.Clamp(value, 0.0f, _data.MaxHp);

        if (_curHp < Mathf.Epsilon)
            Die();
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        LevelTracker.Instance.AddKill(_data);
    }
}


