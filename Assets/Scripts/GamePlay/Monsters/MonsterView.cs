﻿using UnityEngine;
using System.Collections;

public class MonsterView : MonoBehaviour
{
    private Monster _monster;

    private Animator _animator;
    public Animator Animator
    {
        get { return _animator; }
        set { _animator = value; }
    }

    private SpriteRenderer _spriteRenderer;
    public SpriteRenderer SpriteRenderer
    {
        get { return _spriteRenderer; }
        set { _spriteRenderer = value; }
    }

    public void InitializeView(DataMonster data)
    {
        _monster = GetComponentInParent<Monster>();
        if (_monster != null)
            _monster.OnDamageTaken += GotHitAnimation;
        else
            Debug.LogWarning("wtf, this should never happen.");

        _animator = GetComponent<Animator>();
        _animator.runtimeAnimatorController = data.AnimatorController;

        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = data.Sprite;

        GetComponentInChildren<Transform>().localScale = Vector3.one * data.Scale;
        GetComponentInChildren<BoxCollider2D>().size = data.ColliderSize;
    }

    void OnDestroy()
    {
        if(_monster != null)
            _monster.OnDamageTaken -= GotHitAnimation;
    }

    void GotHitAnimation(float damage)
    {
        _animator.SetTrigger("gothit");
        //_animator.SetFloat("Damage", damage); // show damage done to monsters?
    }

}
