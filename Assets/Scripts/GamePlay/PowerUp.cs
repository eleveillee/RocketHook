﻿using UnityEngine;
using System.Collections;
using Lean;
using System;

public class PowerUp : MonoBehaviour, ISpawnable
{
    //MyScriptableObject obj = ScriptableObject.CreateInstance<MyScriptableObject>().init(5, "Gorlock", 15, owningMap, someOtherParameter);

    [SerializeField]
    DataPowerUp _data;
    
    [SerializeField]
    SpriteRenderer _spriteRenderer;
    
    public void Initialize (DataSpawnable data, DataLevelParameters dataLevelParamaters)
    {
        _data = (DataPowerUp)data;
        _spriteRenderer.sprite = _data.Sprite;
    }

    public bool IsPositionGood()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 10f);
        return colliders.Length <= 1;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.GetComponent<PowerupPicker>()) // Added a new script to detect the player's PowerUpPicker. Script is on the same level as body Collider. This need a better solution/standardization of scripts/collider/body positionning.
            return;
        
        PickUpPowerUp();
    }

    void PickUpPowerUp()  
    {
        switch (_data.Type)
        {
            case PowerUpType.Hook:
                LevelTracker.Instance.Player.Ressources.Add(RessourceType.Hook, _data.HookValue);
                break;

            case PowerUpType.Fuel:
                LevelTracker.Instance.Player.Ressources.Add(RessourceType.Fuel, _data.FuelValue);
                break;

            case PowerUpType.Hp:
                LevelTracker.Instance.Player.AddHp(_data.HpValue);
                break;

            case PowerUpType.GoldCoin:
                LevelManager.Instance.Tracker.AddCurrency(_data.CoinCurrency);
                break;

            default:
                Debug.LogWarning("Trying to use a powerup that doesn't exist");
                break;
        }

        LevelManager.Instance.Audio.PlayPowerUpAudio();
        Destroy(gameObject);
    }
}
