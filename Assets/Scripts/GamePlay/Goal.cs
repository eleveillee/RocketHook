﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class Goal
{
    [SerializeField]
    private DataGoal _data;

    private bool _isInitialized; //Some goals(Duration=Game) need to be initialized at the start of a new game to avoid doing them over and over from panelscree.

    public Goal(DataGoal data)
    {
        _data = data;
        if (_data.Duration != GoalDuration.Game)
            Initialize();
    }

    public void Initialize()
    {
        _isInitialized = true;
    }

    public DataGoal Data
    {
        get { return _data; }
    }

    public string GetDescriptionText()
    {
        string text = "";
        switch (_data.Duration)
        {
            case GoalDuration.Game:
                text = "Get {1:0} {0} in a single game";
                break;
            case GoalDuration.Cumul:
                text = "Cumulate {1:0} {0}";
                break;
            case GoalDuration.Lifetime:
                text = "Get a lifetime best {0} of {1:0}";
                break;
            default:
                Debug.LogWarning("[Goal] Missing text");
                return "Missing Text";
        }

        return string.Format(text, _data.Type, _data.Values[0]);
    }

    public float GetValue()
    {
        if (!_isInitialized)
            return 0f;

        TrackingData data;
        switch (_data.Duration)
        {
            case GoalDuration.Game:
                if (LevelManager.Instance == null || LevelManager.Instance.Tracker == null || LevelManager.Instance.Tracker.Data == null)
                    return 0f;
                data = LevelManager.Instance.Tracker.Data;
                break;
            case GoalDuration.Cumul:
                data = GameManager.Instance.PlayerProfile.CumulativeTrackingData;
                break;
            case GoalDuration.Lifetime:
                data = GameManager.Instance.PlayerProfile.PersistentTrackingData;
                break;
            default:
                Debug.LogWarning("[Goal] Unknown goal duration");
                return float.NaN;
        }

        switch (_data.Type)
        {
            case GoalType.Height:
                return data.MaxHeight;
            case GoalType.Gold:
                return data.CurGold;
            case GoalType.Speed:
                return data.MaxSpeed;
            case GoalType.Time:
                return data.TimeLength;
            case GoalType.Score:
                return data.Score;
            default:
                Debug.LogWarning("[Goal] Unknown goal type");
                return float.NaN;
        }
    }

    public bool IsCompleted
    {
        get { return GetValue() >= float.Parse(Data.Values[0]); }
    }
}

