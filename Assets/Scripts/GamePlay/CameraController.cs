﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    [SerializeField]
	GameObject _target;

    [SerializeField]
    Vector3 _offset;

    float _dampingParam = 5.0f;
    //float deadZone = 0.15f;
    //float speed = 2.5f;
    
    void Start()
    {
        LevelManager.Instance.OnPlayerSpawned += (player => _target = player.gameObject);
    }
    void FixedUpdate()
	{
	    if (_target == null)
	        return;

        FollowTarget();
        ZoomWithSpeed();

    }

    void FollowTarget()
    {
        Vector2 newPosition = Vector2.Lerp(transform.position, _target.transform.position + _offset, _dampingParam * Time.deltaTime);
        transform.position = new Vector3(newPosition.x, newPosition.y, _offset.z);
        //transform.position = Vector3.Lerp(transform.position, v3, speed * Time.deltaTime);
    }
    public void TeleportFaceTarget()
    {
        transform.position = _target.transform.position + _offset;
    }

    void ZoomWithSpeed()
    {
        //int cameraSizeGoal = 50 + (int)_targetRigidbody2D.velocity.magnitude/10;
        //Camera.main.orthographicSize += (cameraSizeGoal - Camera.main.orthographicSize) / 10;
    }
}
