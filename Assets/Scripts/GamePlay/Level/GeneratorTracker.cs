﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using System.Linq;

[Serializable]
public class GeneratorTracker : MonoBehaviour
{
    public static bool IsRunning = true;

    LevelGenerator _levelGenerator;
    ChunkGenerator _chunkGenerator;

    int currentChunkId;

    public Dictionary<string, float> LevelStateDatas = new Dictionary<string, float>();
    public List<GenerationInstance> ChunkDatas = new List<GenerationInstance>();
    public bool IsReady;


    #region Logistic

    void Awake()
    {
        _levelGenerator = GetComponentInParent<LevelGenerator>();
        _chunkGenerator = GetComponentInParent<ChunkGenerator>();
        _levelGenerator.OnStateUpdate += GeneratorStateUpdate;
        _chunkGenerator.OnGeneration += OnGeneration;
    }

    void Reset()
    {
        LevelStateDatas.Clear();
        ChunkDatas.Clear();
    }
    #endregion

    #region Processing

    #endregion

    #region Callbacks
    void OnGeneration(GenerationInstance generationInstance)
    {
        ChunkDatas.Add(generationInstance);
    }

    void GeneratorStateUpdate(EnumGeneratorState state)
    {
        if (state == EnumGeneratorState.SpawnNewWorld)
            Reset();

        string key = state.ToString();
        if (state == EnumGeneratorState.StartChunk || state == EnumGeneratorState.FinishChunk)
            key += currentChunkId;

        if (state == EnumGeneratorState.LevelReady)
            IsReady = true;
        else if (state == EnumGeneratorState.SpawnNewWorld)
            IsReady = false;
        
        LevelStateDatas.Add(key, Time.realtimeSinceStartup);
        currentChunkId++;
    }
    #endregion

}

public class GenerationInstance
{
    public DataSpawnable DataSpawnable;
    public Vector2 Chunk;
    public float Time;
    public int NumberTry;

    public GenerationInstance(DataSpawnable data, Vector2 chunk, float time, int number)
    {
        DataSpawnable = data;
        Chunk = chunk;
        Time = time;
        NumberTry = number;
    }
}