﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using System.Linq;

public class ChunkGenerator : MonoBehaviour
{
    bool _isDebugVerbose = false;

    [SerializeField]
    private GameObject _bumperPrefab;

    [SerializeField]
    private GameObject _gooPrefab;

    private bool _isSpawning;
    private int _instantiationPerFrame = 25;
    private int _instantiationPerFrameOnIni = 150;
    DataLevelParameters _dataLevelParameters;
    LevelGenerator _levelGenerator;
    AssetGameDefinition _gameDefinition;
    private float _timeStartGeneration;
    private float _timeObjectGeneration;
    private const int MAX_REROLL = 25;
    private float _frameTime;
    private Vector2 _currentChunk;

    public Action<GenerationInstance> OnGeneration;

    public void Start()
    {
        _levelGenerator = GetComponent<LevelGenerator>();
        _gameDefinition = GameManager.Instance.GameDefinition;
    }

    void Update()
    {
    }

    public void SpawnChunk(DataLevelParameters dataLevelParameters, Vector2 chunkToSpawn, GameObject chunkRoot, Action<Chunk> OnSpawnCompleted)
    {
        if (_isDebugVerbose) Debug.Log("[ChunkGenerator]SpawnChunk : " + chunkToSpawn);
        _dataLevelParameters = dataLevelParameters;
        _currentChunk = chunkToSpawn;
        StartCoroutine(SpawnChunkCoroutine((int)chunkToSpawn.x, (int)chunkToSpawn.y, chunkRoot, OnSpawnCompleted));
    }

    IEnumerator SpawnChunkCoroutine(int i, int j, GameObject chunkRoot, Action<Chunk> OnSpawnCompleted)
    {
        _timeStartGeneration = Time.realtimeSinceStartup;
        // Initialize chunk values
        int curInstantiatePerFrame = _levelGenerator.IsLevelReady ? _instantiationPerFrame : _instantiationPerFrameOnIni;
        Vector2 chunkVector = new Vector2(i, j);
        List<GameObject> platforms = new List<GameObject>();
        Chunk chunk = new GameObject().AddComponent<Chunk>();
        chunk.transform.SetParent(chunkRoot.transform);
        chunk.Index = chunkVector;
        chunk.transform.position = new Vector3(i * _dataLevelParameters.Size, j * _dataLevelParameters.Size, 0.0f);
        chunk.name = "Chunk(" + i + "," + j + ")";
        
        /////////////////////////
        // Spawn Generic Objects
        /////////////////////////
        // Spawn Platforms
        StartCoroutine(SpawnObjects(GameManager.Instance.GameDefinition.DatabaseSpawnablePrefabs.Get(SpawnableType.Platform), chunk.gameObject, _gameDefinition.Platforms.Cast<DataSpawnable>().ToList(), (int)_dataLevelParameters.NbrPlatform.GetRandomValue(), "Platform"));
        while(_isSpawning)
            yield return null;

        // Spawn Goos
        StartCoroutine(SpawnObjects(_gooPrefab, chunk.gameObject, null, (int)_dataLevelParameters.NbrGoo.GetRandomValue(), "Goo"));
        while (_isSpawning)
            yield return null;

        // Spawn Monsters
        if (!_dataLevelParameters.IsSafeZone || _levelGenerator.IsLevelReady)// Spawn monsters if there are no safe zones, or the level is ready. No monsters in initial chunk.gameObjects.
        {
            List<DataSpawnable> MonsterList = _gameDefinition.Monsters.Cast<DataSpawnable>().ToList();
            foreach (SpawnRules sr in LevelManager.Instance.CurrentEncounter.SpawnRules)
            {
                if (MonsterList.Count <= 1)
                    break;

                if (sr.HeightIndex > _currentChunk[1])
                    MonsterList.Remove(sr.Spawnable);
            }            
            StartCoroutine(SpawnObjects(GameManager.Instance.GameDefinition.DatabaseSpawnablePrefabs.Get(SpawnableType.Monster), chunk.gameObject, MonsterList, (int)_dataLevelParameters.NbrMonster.GetRandomValue(), "Monster"));

            while (_isSpawning)
                yield return null;
        }

        // Spawn Powerups
        StartCoroutine(SpawnObjects(GameManager.Instance.GameDefinition.DatabaseSpawnablePrefabs.Get(SpawnableType.PowerUp), chunk.gameObject, _gameDefinition.PowerUps.Cast<DataSpawnable>().ToList(), (int)_dataLevelParameters.NbrPowerUp.GetRandomValue(), "PowerUp"));
        while (_isSpawning)
            yield return null;
        
        // Spawn ObjectsGroups
        StartCoroutine(SpawnObjects(GameManager.Instance.GameDefinition.DatabaseSpawnablePrefabs.Get(SpawnableType.ObjectGroup), chunk.gameObject, _gameDefinition.ObjectGroups.Cast<DataSpawnable>().ToList(), 12, "ObjectGroup"));
        while (_isSpawning)
            yield return null;
            
        /*
        //Spawn Bumper
        for (int k = 0; k < _dataLevelParameters.NbrBumper.GetRandomValue(); k++)
        {
            GameObject randomPlatform = platforms.GetRandomValue();
            Vector2 localPosition = randomPlatform.transform.localPosition + new Vector3(Random.Range(
                -randomPlatform.transform.localScale.x / 2 + _bumperPrefab.transform.localScale.x,
                randomPlatform.transform.localScale.x / 2 - _bumperPrefab.transform.localScale.x),
                randomPlatform.transform.localScale.y / 2 + _bumperPrefab.transform.localScale.y / 2);

            SpawnObject(_bumperPrefab, localPosition, Quaternion.identity, chunk.gameObject, "Bumper(" + k + ")");

            if (k % curInstantiatePerFrame == 0)
                yield return null;
        }
    */

        chunk.gameObject.SetActive(true);
        OnSpawnCompleted(chunk);
        if (_isDebugVerbose) Debug.Log("[ChunkGenerator]ChunkGeneration time : " + (Time.realtimeSinceStartup - _timeStartGeneration));
    }

    IEnumerator SpawnObjects(GameObject prefab, GameObject root, List<DataSpawnable> datas, int number, string name)
    {
        _isSpawning = true;
        int curInstantiatePerFrame = _levelGenerator.IsLevelReady ? _instantiationPerFrame : _instantiationPerFrameOnIni;
        if (_isDebugVerbose) Debug.Log("[ChunkGenerator] SPAWNED " + number + " - " + name + " in " + root.name);
        for (int k = 0; k < number; k++)
        {
            Vector2 localPosition = new Vector3(Random.Range(0, _dataLevelParameters.Size), Random.Range(0, _dataLevelParameters.Size), 0.0f);
            GameObject objectGo = SpawnObject(prefab, (datas != null ? datas.GetRandomValue() : null), root.transform, localPosition, Quaternion.identity, name + "(" + k + ")");

            if (k % curInstantiatePerFrame == 0) // Should include some part of the counter in it
                yield return null;
        }
        _isSpawning = false;
    }

    public GameObject SpawnObject(GameObject prefab, DataSpawnable data, Transform parent, Vector2 localPosition, Quaternion rotation, string objectName, bool isCheckPosition = true, DataLevelParameters dataLevelParameters = null)
    {
        if (dataLevelParameters == null)
            dataLevelParameters = _dataLevelParameters;
        _timeObjectGeneration = Time.realtimeSinceStartup;
        GameObject go = Instantiate(prefab);
        go.transform.parent = parent;
        go.transform.localPosition = localPosition;
        go.transform.rotation = rotation;
        go.name = objectName;

        // Initialize Spawnable
        ISpawnable iSpawnable = go.GetComponent<ISpawnable>();
        if (data == null)
            return go;

        iSpawnable.Initialize(data, dataLevelParameters);

        // Replace Object
        int counter = 0;
        while (isCheckPosition && !iSpawnable.IsPositionGood())
        {
            go.transform.localPosition = new Vector2(Random.Range(0, dataLevelParameters.Size), Random.Range(0, dataLevelParameters.Size));
            counter++;
            if (counter > MAX_REROLL)
                break;
        }
        if (counter > 0 && _isDebugVerbose)
            Debug.Log("[ChunkGenerator] Rerolled " + objectName + " : " + counter);

        if (GeneratorTracker.IsRunning && OnGeneration != null) OnGeneration(new GenerationInstance(data, _currentChunk, Time.realtimeSinceStartup - _timeObjectGeneration, counter));
        return go;
    }
    
}