﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using System.Linq;

public class LevelGenerator : MonoBehaviour
{
    private bool _isSpawningChunk;

    private const float DISTANCE_THRESHOLD = 2000f;

    [SerializeField]
    private GameObject _environmentRoot;
    private GameObject _chunkRoot;

    private ChunkGenerator _chunkGenerator;
    private AssetGameDefinition _gameDefinition;


    private int _instantiationPerFrame = 25;
    private int _instantiationPerFrameOnIni = 150;

    private bool _isLevelReady;
    private bool _isActive;
    private List<Vector2> _chunksToSpawn = new List<Vector2>();
    private List<Chunk> _chunks = new List<Chunk>();
    private Vector2 _playerCachedChunkIndex;
    private float _timeStartGeneration;

    private DataLevelParameters _dataLevelParameters;
    private DataEncounter _dataEncounter;

    public Action OnLevelReady;
    public Action<EnumGeneratorState> OnStateUpdate;

    void Start()
    {
        _chunkGenerator = GetComponent<ChunkGenerator>();
        _gameDefinition = GameManager.Instance.GameDefinition;
    }

    public void SpawnNewWorld(DataEncounter dataEncounter)
    {
        // Initialize new level
        _timeStartGeneration = Time.realtimeSinceStartup;
        if(GeneratorTracker.IsRunning && OnStateUpdate != null) OnStateUpdate(EnumGeneratorState.SpawnNewWorld);

        _dataEncounter = dataEncounter;
        _dataLevelParameters = _dataEncounter.LevelParameters;
        _isLevelReady = false;
        _isSpawningChunk = false;

        // Clear previous level
        ClearLastLevel();
        if(GeneratorTracker.IsRunning && OnStateUpdate != null) OnStateUpdate(EnumGeneratorState.ClearedLastLevel);

        //if (_dataEncounter.StoryInstance != null)
         //   StoryManager.Instance.Activate(_dataEncounter.StoryInstance);

        if (_dataLevelParameters.HasGround)
            Instantiate(GameManager.Instance.GameDefinition.DatabaseSpawnablePrefabs.Get(SpawnableType.Ground), Vector3.down * 25f, Quaternion.identity, _chunkRoot.transform);
        else
            _chunkGenerator.SpawnObject(GameManager.Instance.GameDefinition.DatabaseSpawnablePrefabs.Get(SpawnableType.Platform), GameManager.Instance.GameDefinition.Platforms[0], _chunkRoot.transform, Vector2.zero, Quaternion.identity, "Base Platform", false, _dataEncounter.LevelParameters);

        _chunkRoot.SetActive(true);
        QueueChunksAroundPlayer();
        _isActive = true;
    }

    void ClearLastLevel() // This should be done gradually, and at the end of the previous level.
    {
        _chunksToSpawn.Clear();
        _chunks.Clear();
        if (_chunkRoot == null)
        {
            _chunkRoot = new GameObject();
            _chunkRoot.transform.SetParent(_environmentRoot.transform);
            _chunkRoot.name = "ChunksRoot";
        }
        else
        {
            foreach (Transform t in _chunkRoot.transform)
            {
                Destroy(t.gameObject);
            }
        }
    }

    void Update()
    {
        if (!_isActive || _isSpawningChunk)
            return; 
        
        if(_chunksToSpawn.Count > 0)
            SpawnChunk(_chunksToSpawn[0]);

        Vector2 playerChunkIndex = LevelManager.Instance.GetPlayerChunk();
        if (playerChunkIndex == _playerCachedChunkIndex)
            return;

        _playerCachedChunkIndex = playerChunkIndex;
        DeactivateDistantChunks();
        QueueChunksAroundPlayer();
    }

    // Test for all chunks farther than DISTANCE_THRESHOLD and deactivate them
    void DeactivateDistantChunks()
    {
        if (_isLevelReady && LevelManager.Instance.Player != null)
        {
            foreach (Chunk chunk in _chunks.FindAll(x => x.gameObject.activeSelf && Vector3.Distance(x.transform.position, LevelManager.Instance.Player.transform.position) > DISTANCE_THRESHOLD))
            {
                chunk.gameObject.SetActive(false);
            }
        }
    }

    void TriggerLevelReady()
    {
        _isLevelReady = true;
        OnLevelReady();
        _chunkRoot.SetActive(true);
        if(GeneratorTracker.IsRunning && OnStateUpdate != null) OnStateUpdate(EnumGeneratorState.LevelReady);
    }

    // Find all chunks around player and add them to the queue
    void QueueChunksAroundPlayer()
    { 
        //TODO: Incoroprate player speed to prioritize importants chunk
        for (int j = (int)_playerCachedChunkIndex.y - 1; j <= (int)_playerCachedChunkIndex.y + 1; j++)
        {
            for (int i = (int)_playerCachedChunkIndex.x - 1; i <= (int)_playerCachedChunkIndex.x + 1; i++)
            {
                // Skip the current and chunk above player as they will be put in priority
                if ((i == (int) _playerCachedChunkIndex.x && j == (int) _playerCachedChunkIndex.y) || (i ==(int)_playerCachedChunkIndex.x && j == (int)_playerCachedChunkIndex.y + 1)) 
                    continue;
                AddChunkToQueue(i, j, true);
            }
        }

        AddChunkToQueue((int)_playerCachedChunkIndex.x, (int)_playerCachedChunkIndex.y +1 , true); // Add chunk above player to priority
        AddChunkToQueue((int)_playerCachedChunkIndex.x, (int)_playerCachedChunkIndex.y, true); // Add player chunk to priority
    }

    // Add a chunk to the queue or activate it if it already exist
    void AddChunkToQueue(int i, int j, bool isPriority)
    {
        // Active the chunk if it already exist
        Vector2 chunkToSpawn = new Vector2(i, j);
        Chunk existingChunk = _chunks.Find(x => x.Index == chunkToSpawn); //If the chunk is already spawned and deactivated
        if (existingChunk != null)
        {
            if (existingChunk.gameObject.activeSelf)
                return;
            
            existingChunk.gameObject.SetActive(true);
            return;
        }

        // If chunk is already to be spawned, move it at  of the queue list
        int chunkToSpawnIndex = _chunksToSpawn.FindIndex(x => x == chunkToSpawn);
        if (chunkToSpawnIndex != -1)
        {
            if (isPriority)
                _chunksToSpawn.MoveItemAtIndexToFront(chunkToSpawnIndex);
            return;
        }

        // Otherwise, add the chunk to the list to be spawned
        if (isPriority)
            _chunksToSpawn.Insert(0, chunkToSpawn);
        else
            _chunksToSpawn.Add(chunkToSpawn);
    }

    void SpawnChunk(Vector2 chunkIndex)
    {

        if(GeneratorTracker.IsRunning && OnStateUpdate != null) OnStateUpdate(EnumGeneratorState.StartChunk);
        _chunksToSpawn.Remove(chunkIndex);
        if (_isLevelReady && LevelManager.Instance.Player != null && Vector3.Distance(chunkIndex * _dataLevelParameters.Size, LevelManager.Instance.Player.transform.position) > DISTANCE_THRESHOLD)
            return;

        _chunkGenerator.SpawnChunk(_dataLevelParameters, chunkIndex, _chunkRoot, OnSpawnCompleted);
          _isSpawningChunk = true;
        if(GeneratorTracker.IsRunning && OnStateUpdate != null) OnStateUpdate(EnumGeneratorState.FinishChunk);
    }

    // Called by the ChunkGenerator when everything as been instantiated(over many frames)
    void OnSpawnCompleted(Chunk chunk)
    {
        _chunks.Add(chunk);
        _isSpawningChunk = false;

        if (!_isLevelReady && _chunksToSpawn.Count <= 0)
                TriggerLevelReady();
    }

    public bool IsLevelReady
    {
        get { return _isLevelReady; }
    }

    public ChunkGenerator ChunkGenerator
    {
        get { return _chunkGenerator; }
    }

    public GameObject ChunkRoot
    {
        get { return _chunkRoot; }
    }
}

public enum EnumGeneratorState
{
    SpawnNewWorld,
    ClearedLastLevel,
    StartChunk,
    FinishChunk,
    LevelReady
}