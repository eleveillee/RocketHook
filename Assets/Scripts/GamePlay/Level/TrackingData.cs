﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;
using Random = UnityEngine.Random;

[Serializable]
public class TrackingData
{
    // Here I use straight up public field because it's only data and make it more readable/simple to maintain for now
    public int MaxHeight;
    public int CurGold;
    //public int KillPoints;
    public float TimeLength;
    public float MaxSpeed;
    public float Score;
    public Currency Currency = new Currency();
    public Bestiary Bestiary = new Bestiary();


    // Update max values of current TackingData with fed one. This is used to compare persistenTrackingData with the finished level one
    // -- IMPORTANT --  : Add values here when adding new members
    public void UpdateMaxValues(TrackingData newData)
    {
        MaxHeight = Math.Max(MaxHeight, newData.MaxHeight);
        MaxSpeed = Math.Max(MaxSpeed, newData.MaxSpeed);
        CurGold = Math.Max(Currency.Coin, newData.Currency.Coin);
        TimeLength = Math.Max(TimeLength, newData.TimeLength);
        //KillPoints = Math.Max(KillPoints, newData.KillPoints);
        Score = Math.Max(Score, newData.Score);        
    }

    public void UpdateCumulativeValues(TrackingData newData)
    {
        MaxHeight += newData.MaxHeight;
        MaxSpeed += newData.MaxSpeed;
        CurGold +=  newData.Currency.Coin;
        TimeLength += newData.TimeLength;
        //KillPoints += newData.KillPoints;
        Score += newData.Score;
    }

    public void CalculateScore()
    {
        Score = (int)(MaxHeight + MaxSpeed + TimeLength + CurGold * 10 + Bestiary.TotalKillPoints());
    }
}