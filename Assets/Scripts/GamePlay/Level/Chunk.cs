﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using System.Linq;

public class Chunk : MonoBehaviour
{
    private Vector2 _index;
    public Vector2 Index
    {
        get { return _index; }
        set { _index = value; }
    }
}