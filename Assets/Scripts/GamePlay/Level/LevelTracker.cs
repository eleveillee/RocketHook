﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class LevelTracker : MonoBehaviour
{
    public static LevelTracker Instance;

    private bool _isActive;
    private Player _player;
    private TrackingData _data;
    public Action<Currency> OnCurrencyUpdate;
    public Action<float> OnKillUpdate;

    //private TrackingData _persistentData;
    private float _startTime;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        
    }

    void Start()
    {
        LevelManager.Instance.OnEncounterStart += OnLevelStart;
        LevelManager.Instance.OnEncounterEnd += OnLevelEnd;
    }

    public void Initialize(Player player)
    {
        _player = player;
    }

    void OnLevelStart()
    {
        _data = new TrackingData();
        _isActive = true;
        _startTime = Time.realtimeSinceStartup;
        _data.Bestiary.Populate();
    }

    void OnLevelEnd()
    {
        if (_data == null)
            return;

        _isActive = false;
        _data.TimeLength = Time.realtimeSinceStartup - _startTime;
        _data.CalculateScore();
        GameManager.Instance.PlayerProfile.PersistentTrackingData.UpdateMaxValues(_data);
        GameManager.Instance.PlayerProfile.CumulativeTrackingData.UpdateCumulativeValues(_data);
    }
    

    public void AddCurrency(Currency addCurrency)
    {
        _data.Currency.Add(addCurrency);
        OnCurrencyUpdate(_data.Currency);
    }

    public void AddKill(DataMonster monster)
    {
        int index = _data.Bestiary.monsterId.IndexOf(monster.Id);
        _data.Bestiary.killNumber[index]++;
        OnKillUpdate(_data.Bestiary.TotalKillPoints());
    }

    void Update()
    {
        if (!_isActive)
            return;
        
        _data.MaxHeight = Math.Max(_data.MaxHeight, (int)(_player.transform.position.y/100.0f));
        _data.MaxSpeed = Math.Max(_data.MaxSpeed, LevelManager.Instance.Player.Controller.Rigidbody2D.velocity.magnitude/10.0f);
    }

    #region Serialization

    #endregion

    public TrackingData Data
    {
        get { return _data; }
    }

    public Player Player
    {
        get { return _player; }
    }

    /*
    public TrackingData PersistentData
    {
        get { return _persistentData; }
    }
    */
}