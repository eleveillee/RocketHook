﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Bestiary
{   
    public List<string> monsterId = new List<string>();
    public List<float> killPoints = new List<float>();
    public List<int> killNumber = new List<int>();

    public void Populate()
    {
        foreach (DataMonster dm in GameManager.Instance.GameDefinition.Monsters)        
            {
                monsterId.Add(dm.Id);
                killPoints.Add(dm.KillPoints);
                killNumber.Add(0);
            }
    }

    public float TotalKillPoints()
    {
        float total = 0f;

        for (int i = 0; i < monsterId.Count; i++)
              total = total + killPoints[i] * killNumber[i];
        
        return total;
    }  
}
