using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory
{
    public Action OnUpdate;
    public Inventory()
    {
        _currency.OnUpdate += OnCurrencyUpdated;
    }
    
    ~Inventory()
    {
        _currency.OnUpdate -= OnCurrencyUpdated;
    }

    void OnCurrencyUpdated(Currency currency)
    {
        OnUpdate.SafeInvoke();
    }

    [SerializeField]
    private Currency _currency = new Currency();
    public Currency Currency
    {
        get { return _currency; }
    }

    [SerializeField]
    private List<GadgetInstance> _gadgets = new List<GadgetInstance>();
    public List<GadgetInstance> Gadgets
    {
        get { return _gadgets; }
    }
    
    public GadgetInstance GetGadgetSlot(GadgetSlot slot)
    {
        return _gadgets.Find(x => x.EquippedSlot == slot);
    }

    public void AddGadget(string id, GadgetSlot slot = GadgetSlot.Null)
    {
        // Return if there is already a gadget with the same id
        if (_gadgets.Any(x => x.Data.Id == id))
            return;
        
        GadgetInstance gadgetInstance = new GadgetInstance(id);
        _gadgets.Add(gadgetInstance);
        
        if (slot != GadgetSlot.Null)
            gadgetInstance.Equip(slot);
    }

    public GadgetInstance GetGadget(string id)
    {
        return Gadgets.Find(x => x.Id == id);
    }



}