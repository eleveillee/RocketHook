using UnityEngine;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Lean;
using System;
using System.Collections;
using System.Linq;

public class Player : MonoBehaviour, IDamageable
{
    [SerializeField]
    private DataPlayer _data;
    
    private Ressources _ressources; // All the ressources the player can use to play(Hook, Fuel..)
    private PlayerController _controller;

    private PlayerView _playerView;
    public PlayerView View
    {
        get { return _playerView; }
        set { _playerView = value; }
    }



    private float _gracePeriodDuration = 0.5f;
    private float _endGameSpeedTheshold = 0.5f;

    
    private bool _isAboutToEndGame;

    private float _curHP;

    public Action<float> OnHpUpdated;
    public Action<float> OnDamageTaken;
    
    void Awake()
    {
        _ressources = new Ressources();
        gameObject.SetActive(false);
        _controller = GetComponent<PlayerController>();
        _playerView = GetComponentInChildren<PlayerView>();
        
    }

    public void Initialize()
    {
        _controller.SpawnGadgets();
        InitializeDatas();
    }

    void InitializeDatas()
    {
        Ressources.Set(RessourceType.Hook, (int)_data.GetRessourceValue(RessourceType.Hook));
        Ressources.Set(RessourceType.Fuel, _data.GetRessourceValue(RessourceType.Fuel));
        SetHp(_data.BaseHP);
    }
    
    void Update()
    {
        if (StateManager.Instance.GameState != GameState.Play || _isAboutToEndGame)
            return;

        if (Ressources.Get(RessourceType.Hook) <= 0 && Ressources.Get(RessourceType.Fuel) <= 0)
        {
            if (IsReadyToEndGame())
            {
                StartCoroutine(WaitAndEndGameIfStillReady(_gracePeriodDuration));
            }
        }
    }

    IEnumerator WaitAndEndGameIfStillReady(float timeWait)
    {
        _isAboutToEndGame = true;

        yield return new WaitForSeconds(timeWait);

        if (IsReadyToEndGame())
            ForceEndGame();

        _isAboutToEndGame = false;
    }

    private bool IsReadyToEndGame()
    {
        bool isStopped = _controller.Rigidbody2D.velocity.magnitude <= _endGameSpeedTheshold;
        return isStopped && !_controller.Gadgets.Any(x => x.IsCurrentlyInUse);
    }


    void ForceEndGame()
    {
        LevelManager.Instance.EndEncounter();
    }

    //Hp
    public void AddHp(float value)
    {
        SetHp(_curHP + value);
        if (value < 0)
            OnDamageTaken(value);
    }

    public void SetHp(float value)
    {
        _curHP = Mathf.Clamp(value, 0.0f, _data.BaseHP);
        if (OnHpUpdated != null)
            OnHpUpdated(_curHP);

        if (_curHP < Mathf.Epsilon)
            ForceEndGame();
    }
    
    public float HP
    {
        get { return _curHP; }
    }

    public PlayerController Controller
    {
        get { return _controller; }
    }

    public DataPlayer Data
    {
        get { return _data; }
    }

    public Ressources Ressources
    {
        get { return _ressources; }
    }
}