using UnityEngine;
using System.Collections.Generic;
using Lean;
using System;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Collider2D _bodyCollider2D;

    private List<Gadget> _gadgets = new List<Gadget>(); // The currently spawned gadgets

    private bool _isInput = false;
    private Rigidbody2D _rigidbody2D;
         
    void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();        
    }

    public void ActivateInputs(bool isInput)
    {
        _isInput = isInput;
    }

    #region Gadgets
    public void SpawnGadgets()
    {
        foreach (GadgetInstance gadgetInstance in GameManager.Instance.PlayerProfile.Inventory.Gadgets.FindAll(x => x.IsEquipped))
        {
            if (_gadgets.Find(x => x.Instance.Data.Id == gadgetInstance.Data.Id)) // Don't spawn two instance of the same Gadget
                continue;

            GameObject go = Instantiate(gadgetInstance.Data.Prefab);
            go.transform.parent = transform;
            go.transform.localPosition = Vector3.zero;
            go.transform.rotation = Quaternion.identity;

            Gadget gadget = go.GetComponent<Gadget>();
            if (gadget == null)
                continue;

            _gadgets.Add(gadget);
            gadget.Initialize(gadgetInstance.Data, LevelManager.Instance.Player);
            Invoke("ActivateInitialGadgets", 0.3f); //Delay isn't really clean especially with method as a string, might need a more robust solution
        }
    }



    public void ActivateInitialGadgets()
    {
        bool isThereLauncher = _gadgets.Find(x => x.Instance.Data.Type == GadgetType.Launcher) != null;
        foreach (Gadget gadget in _gadgets)
        {
            gadget.Activate(!isThereLauncher || (gadget.Instance.Data.Type == GadgetType.Launcher));
        }
    }
    
    public void ActivateNormalGadgets()
    {
        foreach (Gadget gadget in _gadgets)
        {
            gadget.Activate(gadget.Instance.Data.Type != GadgetType.Launcher);
        }
    }
    
    #endregion
    
    void FixedUpdate()
    {
        // Change layer to oneway when ySpeed is positive.
        _bodyCollider2D.gameObject.layer = (_rigidbody2D.velocity.y > 0.5f) ? Constants.LAYER_PLAYERONEWAY : Constants.LAYER_PLAYER;
    }
    
    public void Reset()
    {
        _rigidbody2D.velocity = Vector2.zero;
    }

    public bool IsInput
    {
        get { return _isInput; }
    }

    public Rigidbody2D Rigidbody2D
    {
        get { if (_rigidbody2D == null) _rigidbody2D = GetComponent<Rigidbody2D>(); return _rigidbody2D; }
        set { _rigidbody2D = value; }
    }

    public List<Gadget> Gadgets
    {
        get { return _gadgets; }
    }
}
