using UnityEngine;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Lean;
using System;
using System.Collections;
using System.Linq;

//TODO Player Profile(hookLevel, FuelLevel...)
//TODO Inventory(Gold, gadget??)

[Serializable]
public class PlayerProfile
{
    public static bool IsTutorial = true; // Hack To trigger tutorial on new profile
    public static bool IsAlwaysNewProfile = true; // Hack to always create new profile

    [SerializeField]
    private float _version;
    public float Version { get { return _version; } }
    
    [SerializeField]
    private PlayerStats _stats = new PlayerStats();
    public PlayerStats Stats { get { return _stats; } }

    [SerializeField]
    private TrackingData _cumulativeTrackingData = new TrackingData();
    public TrackingData CumulativeTrackingData { get { return _cumulativeTrackingData; } }

    [SerializeField]
    private TrackingData _persistentTrackingData = new TrackingData();
    public TrackingData PersistentTrackingData { get { return _persistentTrackingData; } }

    [SerializeField]
    private Inventory _inventory = new Inventory();
    public Inventory Inventory { get { return _inventory; } }

    [SerializeField]
    private List<Goal> _goals = new List<Goal>();
    public List<Goal> Goals { get { return _goals; } }

    [SerializeField]
    private List<EncounterState> _encounterStates = new List<EncounterState>();
    public List<EncounterState> EncounterStates { get { return _encounterStates; } }

    [SerializeField]
    private List<string> _completedTutorials = new List<string>();
    public List<string> CompletedTutorials { get { return _completedTutorials; } }

    [SerializeField]
    private string _currentDataEncounterId;
    public DataEncounter CurrentDataEncounter // Each get this property is a List.Find to only serialize the id instead of full Data. Need to be taken into account to avoid making too many
    {
        get
        {
            DataEncounter data = GameManager.Instance.GameDefinition.Encounters.Find(x => x.Id == _currentDataEncounterId);
            return data != null ? data : GameManager.Instance.GameDefinition.Encounters.Find(x => x.Id == Constants.ENCOUNTER_STORY1);
        }
        set { _currentDataEncounterId = value.Id; }
    }

    public Action<PlayerProfile> OnUpdate;
    public void Initialize(bool isNewProfile = true)
    {
        if(isNewProfile)
            CreateNewProfile();

        _inventory.OnUpdate += OnUpdateCall;
    }

    ~PlayerProfile()
    {
        _inventory.OnUpdate -= OnUpdateCall;
    }

    void OnUpdateCall()
    {
        if(StateManager.Instance.GameState != GameState.Play)
            SaveSystem.Save();
                
        if(OnUpdate != null)
            OnUpdate(this);
    }

    public void CreateNewProfile()
    {
        //Debug.Log("[PlayerProfile] Creating new profile. Done automatically if versions doesn't match");
        // Save Version
        _version = SaveSystem.CURRENT_VERSION;

        // Get the first main encounter
        _currentDataEncounterId = GameManager.Instance.GameDefinition.Encounters[0].Id;

        // Ressources Levels
        Array enums = Enum.GetValues(typeof(RessourceType));
        foreach (RessourceType ressourceType in enums)
        {
            _stats.RessourcesLevel.Add(new RessourceLevel(ressourceType));
        }
        //Base Currency
        _inventory.Currency.Add(new Currency(10, 0));

        //Base gadgets
        _inventory.Gadgets.Add(new GadgetInstance(Constants.GADGET_JETPACK)); // Jetpack
        _inventory.Gadgets.Add(new GadgetInstance(Constants.GADGET_HOOK)); // Hook
        
        _inventory.GetGadget(Constants.GADGET_JETPACK).Equip(GadgetSlot.Controller1);
        _inventory.GetGadget(Constants.GADGET_HOOK).Equip(GadgetSlot.Controller2);

        // Add Goals
        AddGoal(GoalSystem.GetNextMainGoal());
        AddGoal(GoalSystem.GetRandomGoal());
        AddGoal(GoalSystem.GetRandomGoal());
        AddGoal(GoalSystem.GetRandomGoal());

        if (IsTutorial)
            SetupTutorial();
        else
            SetupNoTuto();

        OnUpdateCall();
    }

    void SetupTutorial()
    {
        _currentDataEncounterId = Constants.ENCOUNTER_TUTORIAL1;
        UpdateEncounter(_currentDataEncounterId, 0); // Tutorial1


        _inventory.AddGadget(Constants.GADGET_LAUNCHERBOOSTER, GadgetSlot.Launcher);
        //_inventory.Gadgets.Add(new GadgetInstance(Constants.GADGET_KNOCKER)); // Knocker
        //_inventory.Gadgets.Add(new GadgetInstance(Constants.GADGET_MAGNET)); // Magnet
    }

    void SetupNoTuto()
    {
        // Initial Encounters
        _currentDataEncounterId = Constants.ENCOUNTER_STORY1;
        UpdateEncounter(Constants.ENCOUNTER_STORY1, 0); // Main1
        UpdateEncounter(Constants.ENCOUNTER_TUTORIAL1, 0); // Tutorial1
        UpdateEncounter("aa2574ce-ac2f-4b9c-8cc8-a72f7ed3be46", 0); // Boss1
        UpdateEncounter("ef3574ce-ac2f-4b9c-8cc8-a72f7ed3be46", 0); // WorldOfGoo
        UpdateEncounter("fg4574ce-ac2f-4b9c-8cc8-a72f7ed3be46", 0); // Gravity1
        UpdateEncounter("fa2574ce-ac2f-4b9c-8cc8-a72f7ed3be46", 0); // Gravity2
    }

    public void AddGoal(Goal goal)
    {
        _goals.Add(goal);
        _goals = _goals.OrderBy(x => x.Data.Source).ToList();
    }


    public void UpdateEncounter(string encounterId, int level)
    {
        EncounterState encounterState = _encounterStates.Find(x => x.EncounterId == encounterId);
        if (encounterState == null)
            _encounterStates.Add(new EncounterState(encounterId, level));
        else
            encounterState.Level = Math.Max(encounterState.Level, level);
    }

    public bool IsGadgetUpgradable
    {
        get { return GameManager.Instance.PlayerProfile.Inventory.Gadgets.Any(x => x.IsUpgradable); } //Debug.Log("Uppgradable : " + GameManager.Instance.GameDefinition.DataShop.GetLockedGadgets().Any(x => Inventory.Currency.Has(x.Price)));
    }

    public bool IsGadgetBuyable
    {
        get { return GameManager.Instance.GameDefinition.DataShop.GetLockedGadgets().Any(x => Inventory.Currency.Has(x.Price)); } //Debug.Log("Shoppable : " + GameManager.Instance.GameDefinition.DataShop.GetLockedGadgets().Any(x => Inventory.Currency.Has(x.Price))); 
    }

    public bool IsCharUpgradable
    {
        get { return _stats.RessourcesLevel.Any(x => x.GetPrice().IsSpendable()); } //Debug.Log("Shoppable : " + GameManager.Instance.GameDefinition.DataShop.GetLockedGadgets().Any(x => Inventory.Currency.Has(x.Price))); 
    }
}