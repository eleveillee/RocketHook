using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class PlayerStats
{
    public List<RessourceLevel> RessourcesLevel = new List<RessourceLevel>();
    
    public float GetRessourceLevel(RessourceType type)
    {
        return RessourcesLevel.Find(x => x.Type == type).Level;
    }
}


// Need to add other upgrades : 
// Hp, Damage, speed, coin boost, ect

[Serializable]
public class RessourceLevel // Might be replaced by more generic UpgradeData
{
    public RessourceLevel(RessourceType type)
    {
        Type = type;
    }

    public Currency GetPrice()
    {
        return new Currency(5 + (int)Mathf.Pow(Level, 1.75f) * 7);
    }

    public RessourceType Type;
    public int Level;
       
}