using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[Serializable]
public class Ressources
{
    //Dictionary<RessourceType, float> _quantities = new Dictionary<RessourceType, float>();
    [SerializeField]
    List<RessourceQuantity> _quantities = new List<RessourceQuantity>();

    public Action<RessourceType, float> OnRessourceUpdate;


    public Ressources()
    {
        foreach (RessourceType type in Enum.GetValues(typeof(RessourceType)))
        {
            _quantities.Add(new RessourceQuantity(type, 0));
        }
    }

    public void Add(RessourceType type, float value)
    {
        Set(type, _quantities.Find(x => x.Type == type).Quantity + value);
    }

    public void Set(RessourceType type, float value)
    {
        _quantities.Find(x => x.Type == type).Quantity = value;
        if (OnRessourceUpdate != null)
            OnRessourceUpdate(type, value);
    }

    public float Get(RessourceType type)
    {
        return _quantities.Find(x => x.Type == type).Quantity;
    }

    [Serializable]
    class RessourceQuantity
    {
        public RessourceQuantity(RessourceType type, float quantity)
        {
            Type = type;
            Quantity = quantity;
        }

        public RessourceType Type;
        public float Quantity;
    }
}

public enum RessourceType
{
    Fuel,
    Hook
}