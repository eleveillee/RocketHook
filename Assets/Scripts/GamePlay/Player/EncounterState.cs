using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class EncounterState
{
    public EncounterState(string id, int level)
    {
        _encounterId = id;
        _level = level;
    }

    [SerializeField]
    private string _encounterId;
    public string EncounterId { get { return _encounterId; }}

    [SerializeField]
    private int _level;
    public int Level { get { return _level; } set { _level = value; }}
}