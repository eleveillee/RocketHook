using UnityEngine;
using System.Collections;
using Lean;

public class PlayerView : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _spriteRenderer;
    private Player _player;
    private Vector3 _originalSize;
    private Animator _animator;

    void Start()
    {
        _player = GetComponent<Player>();
        _player.OnDamageTaken += Bleed;
        _originalSize = _spriteRenderer.transform.localScale;
        _animator = GetComponentInChildren<Animator>();
        _animator.runtimeAnimatorController = _player.Data.AnimatorController;
    }

    public void Bleed(float damage)
    {
        if (!_spriteRenderer.gameObject.activeInHierarchy)
            return;

        _animator.SetTrigger("Ouch");
    }

    public void SlashEffect()
    {
        if (!_spriteRenderer.gameObject.activeInHierarchy)
            return;

        _animator.SetTrigger("Slash");
    }
    
    
    void OnDestroy()
    {
        if(_player != null)
            _player.OnDamageTaken -= Bleed;
    }
}
