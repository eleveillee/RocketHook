﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    private PlayerProfile _playerProfile;

    [SerializeField]
    private GameState _initialGameState;

    [SerializeField]
    public PanelType InitialPanelMenu;

    [SerializeField]
    private DatabaseSprites _databaseSprites;

    [SerializeField]
    private AssetGameDefinition _gameDefinition;

    public Action<PlayerProfile> OnNewPlayerProfile;
    // Use this for initialization
    void Awake ()
	{
	    if (Instance == null)
	        Instance = this;
        else
            Destroy(gameObject);

        _gameDefinition.DataShop.BuildGadgetDatabase();
        LeanTween.init(600); // Hit the max number tween when it was default 400. Maybe I need to derefenrece tweens ??

        InitializeProfile(false);
    }

    // Only broadcast the new profile on a GameReset(F9). At start of game it need to be done during the Start().
    public void InitializeProfile(bool broadcastEvent = true)
    {
        // Load or create a new profile. Initialize it.
        _playerProfile = (SaveSystem.IsSaveExist && !PlayerProfile.IsAlwaysNewProfile) ? SaveSystem.Load() : new PlayerProfile();
        _playerProfile.Initialize(!SaveSystem.IsSaveExist || _playerProfile.Version != SaveSystem.CURRENT_VERSION);

        if (broadcastEvent && OnNewPlayerProfile != null)
            OnNewPlayerProfile(_playerProfile);
    }

    void Start()
    {
        if (OnNewPlayerProfile != null)
            OnNewPlayerProfile(_playerProfile);
        StateManager.Instance.ChangeState(_initialGameState);
        GoalSystem.StartInitialize();
    }

    public void ResetGame()
    {
        SaveSystem.AllowSave(false);
        SaveSystem.DeleteSave();
        InitializeProfile(true);

        if (StateManager.Instance.GameState == GameState.Play)
            StateManager.Instance.ChangeState(GameState.Play);
        else
            UIManager.Instance.ReloadMenu();

        UIManager.Instance.ToggleNotifiers();
        SaveSystem.AllowSave(true);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F2))
            CheatGetRich();
        else if (Input.GetKeyDown(KeyCode.F1))
            CheatGodMode();
        else if (Input.GetKeyDown(KeyCode.F5))
            SaveSystem.Save();
        else if (Input.GetKeyDown(KeyCode.F9))
            ResetGame();

        if (Input.GetKeyDown(KeyCode.Escape)) // Escape is the back button on mobile
        {
            if(StateManager.Instance.GameState == GameState.Play)
                LevelManager.Instance.EndEncounter();
            else if(StateManager.Instance.GameState == GameState.Menu)
                ResetGame();
        }
    }
    
    public void CheatGetRich()
    {
        PlayerProfile.Inventory.Currency.Add(new Currency(100000, 100000));
    }

    public void CheatGodMode()
    {

        // Get Money money
        CheatGetRich();

        // Unlock Encounters
        foreach (DataEncounter data in GameManager.Instance.GameDefinition.Encounters)
        {
            _playerProfile.UpdateEncounter(data.Id, 0);
        }


        // Level Ressources
        foreach (RessourceLevel ressourceLevel in _playerProfile.Stats.RessourcesLevel)
        {
            ressourceLevel.Level = 20;
        }

        // Give Gadgets
        foreach (DataGadget data in GameDefinition.Gadgets)
        {
            _playerProfile.Inventory.AddGadget(data.Id);
        }
        /*
       Constants.GADGET_JETPACK);
        _playerProfile.Inventory.AddGadget(Constants.GADGET_HOOK);
        _playerProfile.Inventory.AddGadget(Constants.GADGET_MAGNET);
        _playerProfile.Inventory.AddGadget(Constants.GADGET_KNOCKER);
        _playerProfile.Inventory.AddGadget(Constants.GADGET_LAUNCHERBOOSTER);
        _playerProfile.Inventory.AddGadget(Constants.GADGET_LAUNCHERELASTIC);
        */
        
        // Upgrade gadgets
        foreach (UpgradeData upgradeData in _playerProfile.Inventory.GetGadget(Constants.GADGET_JETPACK).Upgrades){upgradeData.Level = 10;}
        foreach (UpgradeData upgradeData in _playerProfile.Inventory.GetGadget(Constants.GADGET_HOOK).Upgrades) { upgradeData.Level = 10; }
        foreach (UpgradeData upgradeData in _playerProfile.Inventory.GetGadget(Constants.GADGET_MAGNET).Upgrades) { upgradeData.Level = 10; }
        foreach (UpgradeData upgradeData in _playerProfile.Inventory.GetGadget(Constants.GADGET_LAUNCHERBOOSTER).Upgrades) { upgradeData.Level = 10; }
        foreach (UpgradeData upgradeData in _playerProfile.Inventory.GetGadget(Constants.GADGET_KNOCKER).Upgrades) { upgradeData.Level = 15; }
   
        // Equip launcher and magnet
        _playerProfile.Inventory.GetGadget(Constants.GADGET_LAUNCHERBOOSTER).Equip(GadgetSlot.Launcher);
        _playerProfile.Inventory.GetGadget(Constants.GADGET_MAGNET).Equip(GadgetSlot.Accessory);

    }

    public PlayerProfile PlayerProfile
    {
        get { return _playerProfile; }
        //set { _playerProfile = value;  }
    }

    public DatabaseSprites DatabaseSprite
    {
        get { return _databaseSprites; }
    }

    public AssetGameDefinition GameDefinition
    {
        get { return _gameDefinition; }
    }
}
