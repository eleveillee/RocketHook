﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    // Singleton Instance
    public static LevelManager Instance;

    // Serialized Fields
    [SerializeField] private GameObject _worldRoot;
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private AudioManager _audioManager;

    // Cached values
    private Player _player;
    private LevelGenerator _levelGenerator;
    private LevelTracker _levelTracker;
    private DataEncounter _currentEncounter;
    
    //private List<spawnrules> _currentSpawnRules;

    //private bool _isLevelReady;

    // Callbacks
    public Action<Player> OnPlayerSpawned;
    public Action OnEncounterStart;
    public Action OnEncounterEnd;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        _levelGenerator = GetComponentInChildren<LevelGenerator>();
        _levelTracker = GetComponentInChildren<LevelTracker>();        
        _levelGenerator.OnLevelReady += OnLevelReady;
    }
    
    void FixedUpdate()
    {
        if (_player == null)
            return;

        UpdateGravity();
    }

    // First call to start a new encounter. Tell the generator to start creating it
    public void StartNewEncounter()
    {
        UIManager.Instance.ShowPanel(PanelType.LoadingScreen);
        //_isLevelReady = false;
        _worldRoot.SetActive(true);
        _currentEncounter = GameManager.Instance.PlayerProfile.CurrentDataEncounter;
        //_currentSpawnRules = GameManager.Instance.PlayerProfile.CurrentDataEncounter.SpawnRules;
        _levelGenerator.SpawnNewWorld(_currentEncounter);
    }

    // Spawn player and display world
    void InitializeEncounter()
    {
        if (_player == null)
        {
            GameObject playerGo = Instantiate(_playerPrefab);
            _player = playerGo.GetComponent<Player>();
            _player.transform.SetParent(_worldRoot.transform, true);

            if (OnPlayerSpawned != null)
                OnPlayerSpawned(_player);

            _levelTracker.Initialize(_player);
            _audioManager.Initialize(_player);
        }

        _player.transform.position = 10f * Vector2.up;
        _player.Initialize();
        _player.gameObject.SetActive(true);
        _cameraController.TeleportFaceTarget();
        _cameraController.gameObject.SetActive(true);

        if (_currentEncounter.StoryInstance != null)
            StoryManager.Instance.Activate(_currentEncounter.StoryInstance);

        OnEncounterStart.SafeInvoke();
        UIManager.Instance.ShowUIOVerlay(true);

        if (_currentEncounter.StoryInstance == null)
            _player.Controller.ActivateInputs(true);
        
    }
    
    // Function that is called from within an encounter to end it. Done automatically or trought button/hotkey.
    public void EndEncounter()
    {
        _worldRoot.SetActive(false);
        UIManager.Instance.ShowDialog(false);
        OnEncounterEnd();
        if(_player != null)

            Destroy(_player.gameObject);
        StateManager.Instance.ChangeState(GameState.EndGame);
        if(Tracker.Data != null)
            GameManager.Instance.PlayerProfile.Inventory.Currency.Add(Tracker.Data.Currency);

        StoryManager.Instance.StopCurrentStory();
        TestEncounterCompletion();
        
        SaveSystem.Save();
    }

    

    // Callback received from the Generator when the first chunk are generated and player is ready to play.
    void OnLevelReady()
    {
        //_isLevelReady = true;
    }
    
    // LoadingScreen inform levelManager it has been tapped
    public void SkipLoadingScreen()
    {
        InitializeEncounter();
    }

    void TestEncounterCompletion()
    {
        if (_levelTracker.Data == null || _levelTracker.Data.MaxHeight < 10)
            return;

        GameManager.Instance.PlayerProfile.UpdateEncounter(GameManager.Instance.PlayerProfile.CurrentDataEncounter.Id, 1);
        List<DataEncounter> encounters = GameManager.Instance.GameDefinition.GetEncounters(EncounterType.Story);
        int index = encounters.FindIndex(x => x.Id == GameManager.Instance.PlayerProfile.CurrentDataEncounter.Id);

        // Return if this is the last main encounter
        if (index >= encounters.Count - 1)
            return;

        // Unlock the next main encounter
        GameManager.Instance.PlayerProfile.UpdateEncounter(encounters[index + 1].Id, 0);

    }

    private void UpdateGravity()
    {
        float gravity = _currentEncounter.LevelParameters.Gravity - _player.transform.position.y / _currentEncounter.LevelParameters.HeightPerGravityStep;
        Physics2D.gravity = new Vector2(0f, gravity);
    }

    
    public Vector2 GetPlayerChunk()
    {
        if (_player == null || _currentEncounter == null)
            return Vector2.zero;
        
        Vector3 playerPosition = _player.transform.position;
        int posX = (int) (playerPosition.x/ _currentEncounter.LevelParameters.Size);
        int posY = (int) (playerPosition.y/ _currentEncounter.LevelParameters.Size);

        if (playerPosition.x < 0)
            posX = (int)(playerPosition.x / _currentEncounter.LevelParameters.Size) - 1;

        if (playerPosition.y < 0)
            posY = (int)(playerPosition.y / _currentEncounter.LevelParameters.Size) - 1;
        
        return new Vector2(posX, posY);
    }

    public LevelTracker Tracker
    {
        get { return _levelTracker; }
        set { _levelTracker = value; }
    }

    public Player Player
    {
        get { return _player; }
        set { _player = value; }
    }

    public GameObject PlayerPrefab
    {
        get { return _playerPrefab; }
    }

    public LevelGenerator Generator
    {
        get { return _levelGenerator; }
    }

    public AudioManager Audio
    {
        get { return _audioManager;  }
    }

    public DataEncounter CurrentEncounter
    {
        get { return _currentEncounter; }
    }
}