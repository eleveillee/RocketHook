﻿using UnityEngine;
using System.Collections;
using System;

public class StateManager : MonoBehaviour
{
    public static StateManager Instance;
    private GameState _gameState;
    private bool _isDebugVerbose = false;
    public Action<GameState> OnStateChanged;

    public void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    public void ChangeState(GameState newState)
    {
        /*if (newState == _gameState)
        {
            Debug.LogWarning("[StateManager] State was changed to the same state (" + _gameState + ")");
            return;
        }*/

        if (_isDebugVerbose) Debug.Log("[StateManager] ChangeState(" + _gameState + " -> " + newState + ")");

        _gameState = newState;
        if(OnStateChanged != null)
            OnStateChanged(newState);
        switch (newState)
        {
            case GameState.Menu:
                MenuState();
                break;
            case GameState.Play:
                PlayState();
                break;
            case GameState.EndGame:
                EndGameState();
                break;
            
        }
    }

    void PlayState()
    {
        UIManager.Instance.ShowMenuSidebar(false);
        UIManager.Instance.CloseAllPanels();
        LevelManager.Instance.StartNewEncounter();
    }

    void EndGameState()
    {
        UIManager.Instance.ShowUIOVerlay(false);

        UIManager.Instance.ShowEndGame(true);
        UIManager.Instance.ShowMenuSidebar(true);
        //Camera.main.gameObject.SetActive(false);
    }

    void MenuState()
    {
        UIManager.Instance.ShowUIOVerlay(false);
        UIManager.Instance.ShowEndGame(false);

        UIManager.Instance.ShowPanel(GameManager.Instance.InitialPanelMenu);
        UIManager.Instance.ShowMenuSidebar(true);
    }

    public GameState GameState
    {
        get { return _gameState; }
    }
}


public enum GameState
{
    Menu,
    Play,
    EndGame
}