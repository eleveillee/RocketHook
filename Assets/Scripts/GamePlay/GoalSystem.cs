﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public static class GoalSystem
{
    static bool _isDebugVerbose = false;
    static public Action OnNewGoal;


    public static void StartInitialize()
    {
        SaveSystem.AllowSave(true);
        LevelManager.Instance.OnEncounterStart += InitializeGoals;
    }

    // Find the current Main Goal, if null use the first one. Otherwise get the next one.
    public static Goal GetNextMainGoal()
    {
        Goal _currentMainGoal = GameManager.Instance.PlayerProfile.Goals.Find(x => x.Data.Source == GoalSource.Main);
        if (_currentMainGoal == null)
            return new Goal(GameManager.Instance.GameDefinition.Goals[0].Data);

        int index = GameManager.Instance.GameDefinition.Goals.FindIndex(x => x.Data.Id == _currentMainGoal.Data.Id);
        if (index >= GameManager.Instance.GameDefinition.Goals.Count - 1)
            return null;

        return new Goal(GameManager.Instance.GameDefinition.Goals[index + 1].Data);
    }

    public static Goal GetRandomGoal()
    {
        Array enumGoalTypes = Enum.GetValues(typeof(GoalType));
        GoalType type = ((GoalType)enumGoalTypes.GetValue(Random.Range(0, enumGoalTypes.Length)));

        Array enumGoalDuration = Enum.GetValues(typeof(GoalDuration));
        GoalDuration duration = ((GoalDuration)enumGoalDuration.GetValue(Random.Range(0, enumGoalDuration.Length)));

        // Speed can't be cumulative. Better system need to be in place, dictonnary of forbidden goal ?
        if(type == GoalType.Speed)
        {
            while(duration == GoalDuration.Cumul)
            {
                duration = ((GoalDuration)enumGoalDuration.GetValue(Random.Range(0, enumGoalDuration.Length)));
            }
        }

        List<string> values = new List<string>() { "10" }; // Objectif, to put parametrized values
        Currency rewardCurrency = new Currency(Random.Range(2, 10));
        DataGoal newDataGoal = new DataGoal(type, duration, GoalSource.Random, rewardCurrency, values);
        Goal newGoal = new Goal(newDataGoal);
        if(_isDebugVerbose) Debug.Log("[GoalSystem] NewGoal : " + newGoal.Data.Type + " - " + newGoal.Data.Values[0]);

        return newGoal;
    }

    public static void CompleteGoal(Goal completedGoal)
    {
        if (_isDebugVerbose) Debug.Log("[GoalSystem] Completed : " + completedGoal.Data.Type);
        GameManager.Instance.PlayerProfile.Inventory.Currency.Add(completedGoal.Data.RewardCurrency);
        if(completedGoal.Data.Duration == GoalDuration.Cumul)
            ResetCumulativeData(completedGoal.Data.Type);

        GameManager.Instance.PlayerProfile.Goals.Remove(completedGoal);
        UpdateNewGoal(completedGoal);
    }

    static void UpdateNewGoal(Goal lastCompletedGoal)
    {
        Goal newGoal;
        if (lastCompletedGoal.Data.Source == GoalSource.Main)
            newGoal = GetNextMainGoal();
        else
            newGoal = GetRandomGoal();

        if (newGoal == null)
        {
            Debug.LogWarning("Last Main Quest not properly handled yet");
            return;
        }

        GameManager.Instance.PlayerProfile.AddGoal(newGoal);
        OnNewGoal.SafeInvoke();
    }

    static void ResetCumulativeData(GoalType type)
    {
        TrackingData data = GameManager.Instance.PlayerProfile.CumulativeTrackingData;
        switch (type)
        {
            case GoalType.Height:
                data.MaxHeight = 0;
                break;
            case GoalType.Gold:
                data.CurGold = 0;
                break;
            case GoalType.Speed:
                data.MaxSpeed = 0;
                break;
            case GoalType.Time:
                data.TimeLength = 0;
                break;
            case GoalType.Score:
                data.Score = 0;
                break;
            default:
                Debug.LogWarning("[GoalSystem] Unknown goal type when reinitializing cumulative value");
                break;
        }
    }

    static void InitializeGoals()
    {
        foreach (Goal goal in GameManager.Instance.PlayerProfile.Goals)
        {
            goal.Initialize();
        }
    }

}
