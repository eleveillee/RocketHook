﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Lean;

public class GadgetKnocker : Gadget
{
    private DataGadgetKnocker _data;

    //private bool _isUsingKnocker;
    //public Action OnUseKnocker;
    
    protected override void InitializeFromBase(DataGadget data)
    {
        _data = (DataGadgetKnocker)data;
    }

    public override bool IsCurrentlyInUse
    {
        get { return false; }
    }

    void Start()
    {
        LeanTouch.OnFingerSwipe += OnFingerSwipe;
    }

    void OnDestroy()
    {
        LeanTouch.OnFingerSwipe -= OnFingerSwipe;
    }

    void OnFingerSwipe(LeanFinger finger)
    {
        if (_player.Ressources.Get(RessourceType.Fuel) > 0 && _isActiveAndInput)
            UseKnocker(finger.SwipeDelta);
    }

    void UseKnocker(Vector2 delta)
    {
        float force = _data.UpgradeKnockerPower.GetValue(_instance) + Math.Abs(delta.x) / 1000.0f; //TODO Fix force distance ratio
        Vector2 directionForce = delta.x > 0.0f ? force*Vector2.right : force*Vector2.left;
        _player.Controller.Rigidbody2D.AddForce(directionForce, ForceMode2D.Impulse);
        _player.Ressources.Add(RessourceType.Fuel, - _data.UpgradeKnockerFuelCost.GetValue(_instance) * force / 900.0f);
        //OnUseKnocker.SafeInvoke();
    }
}
