﻿using UnityEngine;
using System.Collections;
using System;

public class Hook : MonoBehaviour {

    DataGadgetHook _dataGadgetHook;
    GadgetInstance _gadgetInstance;

    public Action OnCollision;
    public Action OnTrigger;
    public Action OnHide;

    private float _distanceTraveled;
    private Vector2 _lastPosition;
    private bool _isMoving;
    
    void Start()
    {
        _lastPosition = transform.position;
    }

	public void Launch(DataGadgetHook dataGadgetHook, GadgetInstance gadgetInstance)
	{
        _dataGadgetHook = dataGadgetHook;
        _gadgetInstance = gadgetInstance;

        _isMoving = true;
	    _lastPosition = transform.position;
	    _distanceTraveled = 0.0f;
		GetComponent<Rigidbody2D>().isKinematic = false;
		GetComponent<Rigidbody2D>().AddRelativeForce (new Vector2(0.0f,100.0f*GetComponent<Rigidbody2D>().mass), ForceMode2D.Impulse);
	}

    void FixedUpdate()
    {
        CheckDistance();
    }

    void CheckDistance()
    {
        if (!_isMoving)
            return;

        _distanceTraveled += Vector2.Distance(transform.position, _lastPosition);

        _lastPosition = transform.position;
        if (_distanceTraveled > _dataGadgetHook.UpgradeHookRange.GetValue(_gadgetInstance))
            HideAndStop();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<AggroRangeTrigger>())
            return;

        Monster monster = other.GetComponentInParent<Monster>();
        if (monster == null && other.GetComponentInParent<Goo>() == null)
            return;

        if(monster != null)
            monster.AddHp(_dataGadgetHook.UpgradeHookDamage.GetValue(_gadgetInstance));

        GetComponent<Rigidbody2D>().isKinematic = true;
        OnCollision.SafeInvoke();
        _isMoving = false;

    }

    void OnCollisionEnter2D(Collision2D collision2D)
    {
        _isMoving = false;
        GetComponent<Rigidbody2D>().isKinematic = true;
	    OnCollision.SafeInvoke();
	}

    public void HideAndStop()
    {
        _isMoving = false;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        gameObject.SetActive(false);
        OnHide.SafeInvoke();
    }

    public bool IsMoving
    {
        get {  return _isMoving; }
        set { _isMoving = value; }
    }
}
