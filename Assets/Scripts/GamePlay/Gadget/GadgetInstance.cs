using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GadgetInstance
{
    [SerializeField]
    private string _id;

    [SerializeField]
    private List<UpgradeData> _upgrades = new List<UpgradeData>();
    
    [SerializeField]
    private bool _isEquipped;

    [SerializeField]
    private GadgetSlot _equippedSlot;

    private DataGadget _data;

    public void Equip(GadgetSlot slot, bool isToggle = false)
    {
        //if(slot != null)
        //    GameManager.Instance.PlayerProfile.Inventory.GetGadgetSlot()
        
        if (slot == _equippedSlot)
        {
            if(isToggle)
                _equippedSlot = GadgetSlot.Null;

            return;
        }

        GadgetInstance gadgetInstance = GameManager.Instance.PlayerProfile.Inventory.GetGadgetSlot(slot); 
        if(gadgetInstance != null)
            gadgetInstance.Equip(GadgetSlot.Null);

        _equippedSlot = slot;

    }

    public GadgetInstance(string id)
    {
        _id = id;
        List<DataGadgetUpgrade> dataGadgetUpgrades = GameManager.Instance.GameDefinition.DataShop.DataGadgetUpgrades[Data];
        foreach (DataGadgetUpgrade dataGadgetUpgrade in dataGadgetUpgrades)
        {
            _upgrades.Add(new UpgradeData(dataGadgetUpgrade.Id));
        }
    }

    public bool IsUpgradable
    {
        get
        {
            List<DataGadgetUpgrade> dataUpgrades = GameManager.Instance.GameDefinition.DataShop.DataGadgetUpgrades[Data];
            foreach (DataGadgetUpgrade dataGadgetUpgrade in dataUpgrades)
            {
                if (dataGadgetUpgrade.IsUpgradable(this))
                    return true;
            }
            return false;
        }
    }

    #region Properties
    public DataGadget Data
    {
        get
        {
            if (_data == null)
                _data = GameManager.Instance.GameDefinition.Gadgets.Find(x => x.Id == _id);

            return _data;
        }
    }

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    public List<UpgradeData> Upgrades
    {
        get { return _upgrades; }
        set { _upgrades = value; }
    }

    public bool IsEquipped
    {
        get { return _equippedSlot != GadgetSlot.Null; }
    }

    public GadgetSlot EquippedSlot
    {
        get { return _equippedSlot; }
    }

    public bool IsEquippable
    {
        get { return true; }
    }
    #endregion
}

[Serializable]
public class UpgradeData
{
    public UpgradeData(string id)
    {
        _id = id;
    }

    [SerializeField]
    private string _id;
    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private int _level;
    public int Level
    {
        get { return _level; }
        set { _level = value; }
    }
}