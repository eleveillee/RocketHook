using System;
using UnityEngine;
using System.Collections.Generic;
using Lean;

public class GadgetLauncherBooster : Gadget
{
    private DataGadgetLauncherBooster _data;
    private bool _isRunning;
    private float _timeStarted;

    public Action OnStart;
    public Action OnStop;
    protected override void InitializeFromBase(DataGadget data)
    {
        _data = (DataGadgetLauncherBooster)data;
    }

    public override bool IsCurrentlyInUse
    {
        get { return _isRunning; }
    }

    void Start()
    {
        LeanTouch.OnFingerDown += OnFingerDown;
        //LeanTouch.OnFingerHeldDown += OnFingerHeldDown;
        //LeanTouch.OnFingerHeldUp += OnFingerHoldUp;
    }

    void FixedUpdate()
    {
        if (!_isRunning)
            return;

        if (Time.time - _timeStarted < _data.UpgradeLauncherBooster_Duration.GetValue(_instance))
            UseBooster();
        else
            Stop();
    }

    void OnFingerDown(LeanFinger finger)
    {
        if (_isRunning || !_isActiveAndInput)
            return;
        
        _isRunning = true;
        _timeStarted = Time.time;
        OnStart.SafeInvoke();
    }

    void Stop()
    {
        _isRunning = false;
        OnStop.SafeInvoke();
        _player.Controller.ActivateNormalGadgets();
    }

    void UseBooster()
    {
        _player.Controller.Rigidbody2D.AddForce(new Vector2(0f, _data.UpgradeLauncherBooster_Power.GetValue(_instance)));

        if (LeanTouch.Fingers.Count > 0)
        {
            float offset = LeanTouch.Fingers[0].ScreenPosition.x - Camera.main.pixelWidth/2;
            _player.Controller.Rigidbody2D.AddForce(new Vector2(_data.UpgradeLauncherBooster_Control.GetValue(_instance) * offset/15f, 0f));
        }
        
    }
}
