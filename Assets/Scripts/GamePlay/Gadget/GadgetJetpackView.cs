using UnityEngine;
using System.Collections;
using Lean;

public class GadgetJetpackView : MonoBehaviour
{

    GadgetJetpack _gadgetJetpack;
    
    [SerializeField]
    ParticleSystem _firePs;

    void Awake()
    {
        _gadgetJetpack = GetComponent<GadgetJetpack>();
        _gadgetJetpack.OnStartJetpack += OnStartJetpack;
        _gadgetJetpack.OnStopJetpack += OnStopJetpack;
        OnStopJetpack();
    }

    void OnStartJetpack()
    {
        _firePs.gameObject.SetActive(true);
        _firePs.Play();
    }

    void OnStopJetpack()
    {
        _firePs.Stop();
        _firePs.gameObject.SetActive(false);   
    }

    void OnDestroy()
    {
        _gadgetJetpack.OnStartJetpack -= OnStartJetpack;
        _gadgetJetpack.OnStopJetpack -= OnStopJetpack;
    }

}
