using System;
using UnityEngine;
using System.Collections.Generic;
using Lean;

public class GadgetJetpack : Gadget
{
    private DataGadgetJetpack _data;
    private bool _isUsingJetpack;

    public Action OnStartJetpack;
    public Action OnStopJetpack;
    protected override void InitializeFromBase(DataGadget data)
    {
        _data = (DataGadgetJetpack)data;
    }

    public override bool IsCurrentlyInUse
    {
        get { return _isUsingJetpack; }
    }

    void Start()
    {
        LeanTouch.OnFingerHeldDown += OnFingerHoldDown;
        LeanTouch.OnFingerHeldUp += OnFingerHoldUp;
    }

    void FixedUpdate()
    {
        if (!_isUsingJetpack)
            return;

        if (_player.Ressources.Get(RessourceType.Fuel) > 0.0f && _isActiveAndInput)
            UseJetpack();
        else
            StopUsingJetPack();
    }

    void OnFingerHoldDown(LeanFinger finger)
    {
        if (_player.Ressources.Get(RessourceType.Fuel) > 0.0f && _isActiveAndInput)
        {
            _isUsingJetpack = true;
            OnStartJetpack.SafeInvoke();
        }

    }

    void OnFingerHoldUp(LeanFinger finger)
    {
        StopUsingJetPack();
    }

    void StopUsingJetPack()
    {
        _isUsingJetpack = false;
        OnStopJetpack.SafeInvoke();
    }

    void UseJetpack()
    {
        float fullCost = _data.UpgradeJetpackFuelCost.GetValue(_instance);
        float ratioPower = 1.0f;
        if (_player.Ressources.Get(RessourceType.Fuel) < fullCost)
            ratioPower = _player.Ressources.Get(RessourceType.Fuel) / fullCost;

        _player.Controller.Rigidbody2D.AddForce(new Vector2(0f, ratioPower*_data.UpgradeJetpackPower.GetValue(_instance)));
        _player.Ressources.Add(RessourceType.Fuel, -ratioPower* fullCost);
    }
}
