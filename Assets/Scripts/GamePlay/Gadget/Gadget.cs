using UnityEngine;
using System.Collections.Generic;

public abstract class Gadget : MonoBehaviour
{
    public abstract bool IsCurrentlyInUse { get; }
    protected GadgetInstance _instance;
    protected Player _player;
    protected bool _isActive;

    public void Initialize(DataGadget dataGadget, Player player)
    {
        _player = player;
        _instance = GameManager.Instance.PlayerProfile.Inventory.Gadgets.Find(x => x.Id == dataGadget.Id);
        InitializeFromBase(dataGadget);
    }

    protected abstract void InitializeFromBase(DataGadget dataGadget);
    
    public void Activate(bool isActive = true)
    {
        _isActive = isActive;
    }

    protected bool IsGrounded()
    {
        int groundMask = (1 << Constants.LAYER_GROUNDONEWAY) | (1 << Constants.LAYER_GROUND);
        return Physics2D.OverlapCircle(transform.position, 20f, groundMask) != null;
    }

    public GadgetInstance Instance { get { return _instance; } }
    protected bool _isActiveAndInput{ get { return _isActive && LevelManager.Instance.Player.Controller.IsInput; } }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, 20f);
    }
}
