using System;
using UnityEngine;
using System.Collections.Generic;
using Lean;

public class GadgetJump : Gadget
{
    private DataGadgetJump _data;

    //private bool _isUsingJump;
    //public Action OnUseJump;

    protected override void InitializeFromBase(DataGadget data)
    {
        _data = (DataGadgetJump)data;
    }

    public override bool IsCurrentlyInUse
    {
        get { return false; }
    }

    void Start()
    {
        LeanTouch.OnFingerSwipe += OnFingerSwipe;
    }

    void OnDestroy()
    {
        LeanTouch.OnFingerSwipe -= OnFingerSwipe;
    }

    void OnFingerSwipe(LeanFinger finger)
    {
        if (_player.Ressources.Get(RessourceType.Fuel) > 0 && _isActiveAndInput)
            UseJump(finger.SwipeDelta);
    }

    void UseJump(Vector2 delta)
    {
        float force = _data.UpgradeJumpPower.GetValue(_instance) + Math.Abs(delta.x) / 1000.0f; //TODO Fix force distance ratio
        //Vector2 directionForce = delta.y > 0.0f ? force * Vector2;
        if(delta.y > 0.5f)
            _player.Controller.Rigidbody2D.AddForce(Vector2.up * force, ForceMode2D.Impulse);

        _player.Ressources.Add(RessourceType.Fuel, -_data.UpgradeJumpFuelCost.GetValue(_instance) * force / 900.0f);
        //OnUseJump.SafeInvoke();
    }
}
