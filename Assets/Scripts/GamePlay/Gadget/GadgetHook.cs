using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Lean;

public class GadgetHook : Gadget {

    private DataGadgetHook _data;

    [SerializeField]
    GameObject _hookPrefab;

    private const float TAP_THRESHOLD = 100.0f;

    [SerializeField]
    private LineRenderer _lineRenderer;

    private Hook _hook;

    public override bool IsCurrentlyInUse
    {
        get { return _hook != null && _hook.IsMoving; }
    }

    protected override void InitializeFromBase(DataGadget data)
    {
        _data = (DataGadgetHook)data;
        LeanTouch.OnFingerTap += OnFingerTap;
    }
    
    void OnFingerTap(LeanFinger finger)
    {
        if (_player.Ressources.Get(RessourceType.Hook) > 0 && finger.SwipeDelta.magnitude < TAP_THRESHOLD && _isActiveAndInput)
            ThrowHook(finger.GetWorldPosition(0.0f));
    }

    void Update()
    {
        if(_lineRenderer.gameObject.activeSelf && _hook != null)
            _lineRenderer.SetPositions(new Vector3[] { transform.position, _hook.transform.position });
    }
    
    //TODO Consider player speed when throwing hook, at really fast speed, player goes past it
    void ThrowHook(Vector3 target)
    {
        _lineRenderer.gameObject.SetActive(true);
        _player.Ressources.Add(RessourceType.Hook, -1);
        float hookOffset = 5.0f;
        Vector3 direction = (target - transform.position).normalized;
        Vector3 hookPosition = transform.position + hookOffset * direction;
        hookPosition.z = 0.0f;


        float angle = Mathf.Atan2(target.y - transform.position.y, target.x - transform.position.x) * 180 / Mathf.PI - 90.0f;
        Quaternion hookRotation = Quaternion.Euler(0.0f, 0.0f, angle);

        // Create hook if null. Reuse spawned hook otherwise.
        if (_hook == null)
        {
            GameObject go = Instantiate(_hookPrefab) as GameObject;
            _hook = go.GetComponent<Hook>();
            _hook.OnCollision += OnPull;
            _hook.OnHide += OnHookHide;
        }

        _hook.gameObject.SetActive(true);
        _hook.gameObject.transform.position = hookPosition;
        _hook.transform.localEulerAngles = direction;
        _hook.transform.rotation = hookRotation;
        _hook.Launch(_data, _instance);
    }

    void OnHookHide()
    {
        _lineRenderer.gameObject.SetActive(false);
    }

    public void OnPull()
    {
        if (_hook == null)
            return;
        
        Vector3 direction = (_hook.transform.position - transform.position).normalized;
        float distance = Vector2.Distance(_hook.transform.position, transform.position);
        Vector3 force = _data.UpgradeHookPullStrength.GetValue(_instance) * Mathf.Pow(distance, 1.05f) * direction;
        _player.Controller.Rigidbody2D.AddForce(force, ForceMode2D.Impulse);
        StartCoroutine(WaitAndHideHook(0.2f)); // Might calculate a time in function of distance/speed, or evaluate distance with a threshold
    }

    IEnumerator WaitAndHideHook(float time)
    {
        yield return new WaitForSeconds(time);
        _hook.gameObject.SetActive(false);
        _lineRenderer.gameObject.SetActive(false);
    }

    void DestroyHook()
    {
        Destroy(_hook.gameObject);
        _hook.OnCollision -= OnPull;
        _hook.OnHide -= OnHookHide;
    }

    void OnDestroy()
    {
        if (_hook != null)
            DestroyHook();
 
        LeanTouch.OnFingerTap -= OnFingerTap;
    }
}
