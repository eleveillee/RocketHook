using UnityEngine;
using System.Collections.Generic;
using Lean;

public class GadgetLauncherBoosterView : MonoBehaviour
{

    GadgetLauncherBooster _gadget;
    
    [SerializeField]
    List<ParticleSystem> _firePs = new List<ParticleSystem>();

    void Awake()
    {
        _gadget = GetComponent<GadgetLauncherBooster>();
        _gadget.OnStart += OnStart;
        _gadget.OnStop += OnStop;
        OnStop();
    }

    void OnStart()
    {
        foreach(ParticleSystem ps in _firePs)
        {
            ps.gameObject.SetActive(true);
            ps.Play();
        }
    }

    void OnStop()
    {
        foreach (ParticleSystem ps in _firePs)
        {
            ps.Stop();
            ps.gameObject.SetActive(false);
        }
    }

    void OnDestroy()
    {
        _gadget.OnStart -= OnStart;
        _gadget.OnStop -= OnStop;
    }

}
