﻿using UnityEngine;
using System.Collections;

public class GadgetMagnet : Gadget
{
    private DataGadgetMagnet _data;

    private CircleCollider2D _collider;

    
    public override bool IsCurrentlyInUse
    {
        get { return true; }
    }

    void Awake()
    {
    }

    void Start()
    {
        InitializeFromBase(_data);
    }

    protected override void InitializeFromBase(DataGadget data)
    {
        _collider = GetComponent<CircleCollider2D>();
        _data = (DataGadgetMagnet) data;
        _collider.radius = _data.UpgradeMagnetRadius.GetValue(_instance);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (!other.GetComponent<PowerUp>() || !_isActiveAndInput)
            return;

        other.transform.position = Vector3.MoveTowards(other.transform.position, LevelTracker.Instance.Player.transform.position, _data.UpgradeMagnetPower.GetValue(_instance));
    }
}
