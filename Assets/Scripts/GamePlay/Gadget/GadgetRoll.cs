using System;
using UnityEngine;
using System.Collections.Generic;
using Lean;

public class GadgetRoll : Gadget
{
    private DataGadgetRoll _data;
    private bool _isUsingRoll;

    public Action OnStartRoll;
    public Action OnStopRoll;
    protected override void InitializeFromBase(DataGadget data)
    {
        _data = (DataGadgetRoll)data;
    }

    public override bool IsCurrentlyInUse
    {
        get { return _isUsingRoll; }
    }

    void Start()
    {
        LeanTouch.OnFingerHeldDown += OnFingerHoldDown;
        LeanTouch.OnFingerHeldUp += OnFingerHoldUp;
    }

    void FixedUpdate()
    {
        if (!_isUsingRoll)
            return;


        if (_player.Ressources.Get(RessourceType.Fuel) > 0.0f && _isActiveAndInput && IsGrounded())
            UseRoll();
        else
            StopUsingRoll();
    }

    void OnFingerHoldDown(LeanFinger finger)
    {
        if (_player.Ressources.Get(RessourceType.Fuel) > 0.0f && _isActiveAndInput && IsGrounded())
        {
            _isUsingRoll = true;
            OnStartRoll.SafeInvoke();
        }

    }

    void OnFingerHoldUp(LeanFinger finger)
    {
        StopUsingRoll();
    }

    void StopUsingRoll()
    {
        _isUsingRoll = false;
        OnStopRoll.SafeInvoke();
    }

    void UseRoll()
    {
        float fullCost = _data.UpgradeRollFuelCost.GetValue(_instance);
        float ratioPower = 1.0f;
        if (_player.Ressources.Get(RessourceType.Fuel) < fullCost)
            ratioPower = _player.Ressources.Get(RessourceType.Fuel) / fullCost;
        
        float direction = LeanTouch.Fingers[0].ScreenPosition.x > Camera.main.pixelWidth / 2 ? 1f : -1f;

        _player.Controller.Rigidbody2D.AddForce(new Vector2(direction*ratioPower * _data.UpgradeRollPower.GetValue(_instance), 0f));
        _player.Ressources.Add(RessourceType.Fuel, -ratioPower* fullCost);
        
    }
}
