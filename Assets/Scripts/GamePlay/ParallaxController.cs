﻿using UnityEngine;
using System.Collections;

public class ParallaxController : MonoBehaviour {

    [SerializeField]
    GameObject _backgroundRoot;

    [SerializeField]
    Vector3 _offset;

    const float PARALLAX_SPEED = 0.04f;

    Rigidbody2D _target;
    Vector3 _initialPosition;
    Vector3 _targetPreviousPosition;

    Rigidbody2D _rigidBody2D;
    void Start()
    {
        _backgroundRoot.SetActive(true);
        LevelManager.Instance.OnPlayerSpawned += OnPlayerSpawned;
        _initialPosition = transform.localPosition;
    }
    void Update()
	{
	    if (_target == null)
	        return;

        FollowTarget();
    }


    void OnPlayerSpawned(Player player)
    {
        transform.localPosition = _initialPosition;
        _target = player.GetComponent<Rigidbody2D>();
        _targetPreviousPosition = _target.transform.position;
    }

    void FollowTarget()
    {
        transform.localPosition -= (_target.transform.position - _targetPreviousPosition) * PARALLAX_SPEED;
        _targetPreviousPosition = _target.transform.position;
    }
}
