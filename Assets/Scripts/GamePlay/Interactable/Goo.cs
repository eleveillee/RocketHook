﻿using UnityEngine;
using System.Collections;
using Lean;

public class Goo : MonoBehaviour, ISpawnable
{

    private float _viscosity = 35f;
     
    public void Initialize(DataSpawnable data, DataLevelParameters dataLevelParamaters)
    {

    }

    public bool IsPositionGood()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 20f);
        return colliders.Length <= 1;
    }

    void OnTriggerStay2D (Collider2D other)
    {
        other.attachedRigidbody.AddForce(-_viscosity * other.attachedRigidbody.velocity);
    }
	
	
}
