﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectGroup : MonoBehaviour, ISpawnable
{
    DataObjectGroup _data;
    AssetGameDefinition _gameDefinition;
    int ownColliders;
    void Awake()
    {
        _gameDefinition = GameManager.Instance.GameDefinition;
    }

    public void Initialize(DataSpawnable data, DataLevelParameters dataLevelParamaters)
    {
        _data = (DataObjectGroup)data;
        foreach (ObjectPosition objectPosition in _data.ObjectPositions)
        {
            DataSpawnable dataObject = _gameDefinition.GetSpawnableById(objectPosition.Id);
            LevelManager.Instance.Generator.ChunkGenerator.SpawnObject(_gameDefinition.DatabaseSpawnablePrefabs.Get(dataObject.SpawnableType), dataObject, transform, objectPosition.Position, Quaternion.identity, dataObject.Name, false);
        }

        ownColliders = _data.ObjectPositions.Count;
    }

    public bool IsPositionGood()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 18f);
        return colliders.Length <= ownColliders;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, 20f);
    }
}
