﻿using UnityEngine;
using System.Collections;


public class Bumper : MonoBehaviour, ISpawnable
{
    public void Initialize(DataSpawnable data, DataLevelParameters dataLevelParamaters)
    {

    }
    public bool IsPositionGood()
    {
        return true;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        Rigidbody2D rigidbody2D = other.GetComponentInParent<Rigidbody2D>();
        if (rigidbody2D != null)
            rigidbody2D.AddForce(Vector3.up*100.0f, ForceMode2D.Impulse);
    }

}
