﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour, ISpawnable
{
    private float _roll;
    private float _rotateSpeed = 50f;

    DataPlatform _data;
    BoxCollider2D _boxCollider2D;

    void Awake()
    {
        _boxCollider2D = GetComponent<BoxCollider2D>();
    }

    public void Initialize(DataSpawnable data, DataLevelParameters dataLevelParamaters)
    {
        _data = (DataPlatform)data;
        //Vector2 localPosition = new Vector3(Random.Range(0, _dataLevelParameters.Size), Random.Range(0, _dataLevelParameters.Size));
        //Quaternion localRotation = Quaternion.identity;
        //GameObject platform = SpawnObject(_platformPrefab, localPosition, localRotation, chunk.gameObject, "Platform(" + k + ")");
        //platform.GetComponent<Platform>().Initialize; 
        transform.localScale = new Vector3(_data.Size.GetRandomValue(), 1f, 10f);
        if(dataLevelParamaters.IsPlatformRotating)
            transform.Rotate(new Vector3(0f, 0f, Random.Range(-45f, 45f)));
    }

    public bool IsPositionGood()
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(transform.position, 2*transform.localScale, transform.rotation.eulerAngles.z);
        /*Debug.Log("colliders : " + colliders.Length);
        if (colliders.Length >= 2)
            Debug.Log(transform.name + " collided with " + colliders[1].name);*/

        return colliders.Length <= 1;
    }

    void Update ()
    {
        //if (_roll > 90f)
        //    transform.Rotate(0f, 0f, _rotateSpeed * Time.deltaTime);

    }
}
