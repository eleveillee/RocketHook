﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Currency
{
    [SerializeField]
    private int _coin;

    [SerializeField]
    private int _gem;

    public Action<Currency> OnUpdate;

    public Currency(int coin = 0, int gem = 0)
    {
        _coin = coin;
        _gem = gem;
    }

    public bool Has(Currency cost)
    {
        if (_coin < cost.Coin)
            return false;
        else if (_gem < cost.Gem)
            return false;

        return true;
    }

    public void Add(Currency addCurrency, bool isNegative = false)
    {
        if (isNegative)
            Add(-addCurrency.Coin, -addCurrency.Gem);
        else
            Add(addCurrency.Coin, addCurrency.Gem);
    }

    public bool TrySpend(Currency currency)
    {
        if (Has(currency))
        {
            Add(currency, true);
            return true;
        }

        return false;
    }

    //Gold
    void AddCoin(int value)
    {
        SetCoin(_coin + value);
    }

    void SetCoin(int value)
    {
        _coin = Math.Max(value, 0);
    }

    //Gems
    void AddGem(int value)
    {
        SetGem(_gem + value);
    }

    void SetGem(int value)
    {
        _gem = Math.Max(value, 0);
    }

    public int Coin
    {
        get { return _coin; }
    }

    public int Gem
    {
        get { return _gem; }
    }

    public void Add(int coin, int gem)
    {
        AddCoin(coin);
        AddGem(gem);
        
        if(OnUpdate != null)
            OnUpdate(this);
            
    }
}
