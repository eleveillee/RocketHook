﻿using System;
using System.Collections.Generic;
using UnityEngine;

static class SaveSystem
{
    public const float CURRENT_VERSION = 0.15f;
    private static bool _isDebugVerbose = false;
    private static string _playerPrefName = "Save";
    public static bool IsSaveExist { get { return PlayerPrefs.HasKey(_playerPrefName); }}
    public static bool _isSaveAllowed = false;

    #region Save
    public static void Save()
    {
        if (!_isSaveAllowed)
            return;

        string saveJson = Serialize();
        PlayerPrefs.SetString(_playerPrefName, saveJson);
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Save to Json : " + saveJson);
    }
    
    public static string Serialize()
    {
        return JsonUtility.ToJson(GameManager.Instance.PlayerProfile);
    }

    #endregion

    #region Load
    public static PlayerProfile Load()
    {
        string jsonSave = PlayerPrefs.GetString(_playerPrefName);
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Load from Json : " + jsonSave);
        return Deserialize(jsonSave);
    }

    public static PlayerProfile Deserialize(string jsonSave)
    {
        return JsonUtility.FromJson<PlayerProfile>(jsonSave);
    }

    #endregion

    public static void DeleteSave()
    {
        Debug.Log("[SaveSystem] Save Deleted and profile restarted");
        PlayerPrefs.DeleteAll();
    }

    public static void AllowSave(bool isAllowed)
    {
        _isSaveAllowed = isAllowed;
    }
}