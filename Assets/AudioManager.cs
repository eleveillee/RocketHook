﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance = null;
           
    private Player _player;     
    private DatabaseAudio _data;

    [SerializeField] private AudioSource _sourceMusic;
    [SerializeField] private AudioSource _sourceSFX;

    private float _masterVolume = 1f;
    
    private float _musicVolume = 1f;
    
    private float _sfxVolume = 1f;
    


    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);        
    }


    public void Initialize(Player player)
    {        
        _player = player;
        _player.OnDamageTaken += PlayHurtAudio;

        _data = GameManager.Instance.GameDefinition.DatabaseAudio;

        PlayMusic();
    }

    void PlayMusic()
    {
        AudioClip music = _data.GameBeat;
        if (music != null)
        {
            _sourceMusic.clip = music;
            _sourceMusic.volume = _masterVolume * _musicVolume;
            _sourceMusic.Play();
        }       
    }

    public void OnMasterVolumeChanged(float value)
    {
        _masterVolume = value;
        _sourceMusic.volume = _musicVolume * _masterVolume;
        _sourceSFX.volume = _sfxVolume * _masterVolume;
    }

    public void OnMusicVolumeChanged(float value)
    {
        _musicVolume = value;
        _sourceMusic.volume = _musicVolume * _masterVolume;
    }
   
    public void OnSFXVolumeChanged(float value)
    {
        _sfxVolume = value;
        _sourceSFX.volume = _sfxVolume * _masterVolume;
    }

    void PlayHurtAudio(float damage)
    {
        AudioClip hurt = _data.HurtAudio[Random.Range(0, _data.HurtAudio.Count)];
        if (hurt != null)        
            _sourceSFX.PlayOneShot(hurt, _masterVolume * _sfxVolume);
    }

    public void PlaySlashAudio()
    {
        AudioClip slash = _data.SlashAudio[Random.Range(0, _data.SlashAudio.Count)];
        if (slash != null)
            _sourceSFX.PlayOneShot(slash, _masterVolume * _sfxVolume);
    }

    public void PlayPowerUpAudio()
    {
        AudioClip powerUpAudio = _data.PowerUpAudio;
        if (powerUpAudio != null)
            _sourceSFX.PlayOneShot(powerUpAudio, _masterVolume * _sfxVolume);
    } 

    void OnDestroy()
    {
        if (_player != null)
            _player.OnDamageTaken -= PlayHurtAudio;
    }
}