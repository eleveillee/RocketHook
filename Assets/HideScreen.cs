﻿using UnityEngine;
using System.Collections;

public class HideScreen : MonoBehaviour {

    public static HideScreen Instance;
    
    protected SpriteRenderer _spriteRenderer;

    void Awake()
    {
        gameObject.SetActive(false);

        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void ChangeOpacity(float opacity)
    {
        _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, opacity);
    }
}
