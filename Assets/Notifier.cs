﻿using UnityEngine;
using System.Collections;

public class Notifier : MonoBehaviour {

    [SerializeField]
    Transform _root;
    
    [SerializeField]
    GameObject _notifierPrefab;

    GameObject _notifier;

    void Awake()
    {
        _notifier = (GameObject)Instantiate(_notifierPrefab, _root.transform.position, Quaternion.identity, _root.transform);
        Toggle(false);
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	public void Toggle (bool isEnabled) {
        _notifier.SetActive(isEnabled);
    }
}
